<?php
App::uses('AppModel', 'Model');
/**
 * Adminuser Model
 *
 */
class Attribute extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'attribute_id';

}
