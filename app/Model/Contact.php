<?php
App::uses('AppModel', 'Model');
/**
 * Staticpage Model
 *
 */
class Contact extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'contact_id';

}
