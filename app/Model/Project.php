<?php
App::uses('AppModel', 'Model');
/**
 * Staticpage Model
 *
 */
class Project extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'project_id';

}
