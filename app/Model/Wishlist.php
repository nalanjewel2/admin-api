<?php

App::uses('AppModel', 'Model');

/**
 * Adminuser Model
 *
 */
class Wishlist extends AppModel {

    /**
     * Primary key field
     *
     * @var string
     */
    public $primaryKey = 'wish_id';

}
