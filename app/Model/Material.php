<?php
App::uses('AppModel', 'Model');
/**
 * Staticpage Model
 *
 */
class Material extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'material_id';

}
