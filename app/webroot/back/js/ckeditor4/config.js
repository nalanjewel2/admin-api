/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';

//    config.extraPlugins += (config.extraPlugins.length == 0 ? '' : ',') + 'ckeditor_wiris';
//    config.toolbar_Full.push({name: 'wiris', items: ['ckeditor_wiris_formulaEditor', 'ckeditor_wiris_formulaEditorChemistry']});
    config.allowedContent = true;
    config.filebrowserBrowseUrl = '/trade/back/js/ckfinder/ckfinder.html'; /*Change foldername*/

    config.filebrowserImageBrowseUrl = '/trade/back/js/ckfinder/ckfinder.html?Type=Images';

    config.filebrowserFlashBrowseUrl = '/trade/back/js/ckfinder/ckfinder.html?Type=Flash';

    config.filebrowserUploadUrl = '/trade/back/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';

    config.filebrowserImageUploadUrl = '/trade/back/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';

    config.filebrowserFlashUploadUrl = '/trade/back/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
};
