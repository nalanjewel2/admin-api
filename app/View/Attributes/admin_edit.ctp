<div class="br-mainpanel">
    <div class="br-pagetitle">   
        <div class="col-md-6">
            <h4>Edit Attribute</h4>
        </div>
        <div class="col-md-6">
            <div class="btn-group float-right">
                <a href="<?php echo BASE_URL; ?>admin/attributes/index" class="btn addbtn"><i class="fas fa-arrow-left"></i> Back to list</a>
            </div>
        </div>
    </div><!-- d-flex -->
    <div class="br-pagebody">
        <div class="br-section-wrapper">
            <form method="post" action="#" class="validation_form" enctype="multipart/form-data">
                <div class="form-layout form-layout-4"> 
                <div class="row mg-t-20">
                        <label class="col-sm-2 form-control-label">Attribute Name <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" class="form-control validate[required,custom[onlyLetterSp]]" name="data[Attribute][attribute_name]" value="<?php echo $result['Attribute']['attribute_name']; ?>"/>
                        </div>
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-2 form-control-label">Attribute Type <span class="tx-danger">*</span></label>
                        <div class="col-lg-9" style="display:flex;">
                            <div class="radio-inline">
                                <label>
                                    <input <?php echo ($result['Attribute']['type'] == "Select") ? "checked" : ""; ?> type="radio" value="Select"  class="control-info control-info1 validate[required]" name="data[Attribute][type]">
                                    Select
                                </label>
                            </div>
                            <div class="radio-inline">
                                <label>
                                    <input <?php echo ($result['Attribute']['type'] == "Text") ? "checked" : ""; ?> type="radio" value="Text"  class="control-info control-info1 validate[required]" name="data[Attribute][type]">
                                    Text
                                </label>
                            </div>
                        </div>
                    </div>                    
                    <div class="row mg-t-20 values" style="display: none;">
                        <label class="col-sm-2 form-control-label">Value <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <?php
                            $values = ClassRegistry::init('Attributevalue')->find('all', array('conditions' => array('attribute_id' => $this->params['pass']['0'])));
                            $vals = array_map(function ($ar) {
                                return $ar['Attributevalue']['value'];
                            }, $values);
                            ?>
                            <textarea class="form-control validate[required]" name="data[Attributevalue][value]"> <?php echo implode(',', $vals); ?></textarea>  
                            <span class="help-block">Each value should be seperated with comma(,)</span>
                        </div>
                    </div>
                   <div class="clearfix form-actions row submit-cancel"> 
                                    <div class="">
                                        <button class="btn btn-green" type="submit"> <i class="ace-icon fa fa-check bigger-110"></i> Submit </button>
                                    </div>
                                    <div class="">
                                        <a href="<?php echo BASE_URL; ?>admin/attributes/index" class="btn btn-cancel"/>Cancel</a>
                                    </div>
                                </div>
                </div>   
            </form>
        </div>
    </div>
</div>
<script>
    $('.control-info1').click(function () {
        var val = $(this).val();
        if (val == 'Single' || val == 'Select') {
            $('.values').show();
        } else {
            $('.values').hide();
        }
    });
    $(document).ready(function () {
        var val = $('.control-info1:checked').val();
        if (val == 'Single' || val == 'Select') {
            $('.values').show();
        } else {
            $('.values').hide();
        }
    });
</script>