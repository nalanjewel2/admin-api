<div class="br-mainpanel">
    <div class="br-pagetitle">
        <i class="icon icon ion-ios-book-outline"></i> 
        <div class="col-md-6">
            <h4>Product View</h4>
        </div>
        <div class="col-md-6">
            <div class="btn-group float-right">
                <a href="<?php echo BASE_URL; ?>admin/products" class="btn br-menu-link active addbtn"><i class="la la-arrow-left"></i> Back to List</a>
            </div>
        </div>
    </div><!-- d-flex -->
    <div class="br-pagebody">
        <div class="br-section-wrapper">
            <form action="" class="form-horizontal" method="post" enctype="multipart/form-data">
                <fieldset class="content-group">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-3">Product Code</label>
                        <div class="col-lg-9 col-md-9">
                            <?php echo $product['Product']['product_code']; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-3">Product Name</label>
                        <div class="col-lg-9 col-md-9">
                            <?php echo $product['Product']['name']; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-3">Category Name</label>
                        <div class="col-lg-9 col-md-9">
                            <?php
                            $categories = ClassRegistry::init('Category')->find('all', array('conditions' => array('category_id' => $product['Product']['category_id'])));
                            foreach ($categories as $category) {
                                ?>
                                <?php echo $category['Category']['category_name']; ?>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-3">Description</label>
                        <div class="col-lg-9 col-md-9">
                            <?php echo $product['Product']['discription']; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-3">Warranty</label>
                        <div class="col-lg-9 col-md-9">
                            <?php echo $product['Product']['warrenty']; ?>
                        </div>
                    </div>
                    <?php if (!empty($product['Product']['warrent_period'])) { ?>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-lg-3">Warranty Period</label>
                            <div class="col-lg-9 col-md-9">
                                <?php echo $product['Product']['warrent_period']; ?>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-3">Date</label>
                        <div class="col-lg-9 col-md-9">
                            <?php echo date('d-m-Y', strtotime($product['Product']['created_date'])); ?>
                        </div>
                    </div>
                    <?php
                    $images = ClassRegistry::init('Productimage')->find('all', array('conditions' => array('product_id' => $product['Product']['product_id'])));
                    if (!empty($images)) {
                        ?>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-lg-3">Images</label>
                            <?php
                            foreach ($images as $image) {
                                ?>
                                <div class="col-lg-9 col-md-9">
                                    <a href="<?php echo BASE_URL; ?>files/proimages/<?php echo $image['Productimage']['images']; ?>" data-lightbox="image-1" data-title="My caption">
                                        <img src="<?php echo BASE_URL; ?>files/proimages/<?php echo $image['Productimage']['images']; ?>" class="img-thumb-admin img-responsive"/>
                                    </a>
                                </div>
                                </a>
                            <?php }
                            ?>
                        </div>
                    <?php } ?>
                </fieldset>
            </form>
        </div>
    </div>
</div>
<style>
    .form-group{
        display: flex;
    }
    label.control-label {
        color: black;
        font-weight: 500;
    }
    img.img-thumb-admin.img-responsive {
        width: 150px !important;
    }
</style>