<div class="br-mainpanel">
    <div class="br-pagetitle">      
        <div class="col-md-6">
            <h4>Edit Product</h4>
        </div>
        <div class="col-md-6">
            <div class="btn-group float-right">
                <a href="<?php echo BASE_URL; ?>admin/products/index" class="btn  addbtn"><i class="fas fa-arrow-left"></i> Back to list</a>
            </div>
        </div>
    </div><!-- d-flex -->
    <div class="br-pagebody">
        <div class="br-section-wrapper addproducts">
            <form method="post" action="#" class="validation_form addform" enctype="multipart/form-data" id="product-submit">
                <div class="form-layout form-layout-4">  
                  <h5 class="addproduct_title first">Product Details</h5>
                    <div class="row">
                        <label class="col-sm-2 form-control-label">Product Name: <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" placeholder="Name" class="form-control validate[required,custom[onlyLetterSp]]" name="data[Product][product_name]" value="<?php echo $this->request->data['Product']['product_name'] ?>"/>
                        </div>
                    </div><!-- row -->
                   <div class="row mg-t-20">
                        <label class="col-sm-2 form-control-label">Description <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <textarea name="data[Product][description]" class="form-control validate[required]"  placeholder="Description" rows="8"><?php echo $this->request->data['Product']['description'] ?></textarea>    
                        </div>
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-2 form-control-label">Collection <span class="tx-danger"></span></label>
                        <?php
                        if($this->request->data['Product']['latest_collection']=="1"){
                            $lates_collection="checked";
                        }else{
                            $lates_collection="";
                        }
                        ?>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                          <input type="checkbox" <?php echo $lates_collection;?> name="data[Product][latest_collection]"/> Latest Collection
                        </div>
                    </div> 
                    <div class="row mg-t-20">
                        <label class="col-sm-2 form-control-label">Image <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="file" class="form-control" id="jewelphotos" multiple="" name="data[Product][image][]"/>    
                            <p><small>Recommended Size: 1600*750</small></p>         
                        </div>
                    </div>
                    <div class="form-group mg-t-20 sprofile">
                            <ul class="list-unstyled shop-photos added_img" style="display:flex">
                                <?php
                                $photos = ClassRegistry::init('Productimage')->find('all', array('conditions' => array('product_id' => $this->request->data['Product']['product_id'])));
                                foreach ($photos as $photo) {
                                ?>
                                <li>
                                    
                                        <img class="img-responsive img-rounded product-img" src="<?php echo BASE_URL.'files/products/'.$photo['Productimage']['image']?>"/>
                                        <a href="javascript:;" class='remove'><i class="fas fa-times"></i></a>
                                        <input type="hidden" class="old_photos" name='data[Product][old_photos][]' value="<?php echo $photo['Productimage']['product_imageid']; ?>"/>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>                    
                    <div class="row mg-t-20">
                        <label class="col-sm-2 form-control-label">Quantity<span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" placeholder="Quantity" class="form-control validate[required]" value="<?php echo $this->request->data['Product']['product_qty'];?>" name="data[Product][product_qty]"/>
                        </div>
                    </div>
                    <div class="row mg-t-20">
                        <?php
                        $materials = ClassRegistry::init('Material')->find('all', array('conditions' => array('status !=' => 'Trash'),'order'=>'material_name ASC'));
                        $projects = ClassRegistry::init('Project')->find('all', array('conditions' => array('status !=' => 'Trash'),'order'=>'project_name ASC'));
                        ?>
                        <div class="col-md-6 row">
                             <label class="col-sm-4 form-control-label">Material <span class="tx-danger">*</span></label>
                             <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                               <select class="form-control validate[required] select2" multiple="true"  name="data[Product][material_id][]">
                                  <option value="">---Select Material---</option>
                                  <?php 
                                  $material_ids=explode(',',$this->request->data['Product']['material_id']);
                                  foreach ($materials as $material) { ?>
                                    <option value="<?php echo $material['Material']['material_id'];?>" <?php echo in_array($material['Material']['material_id'],$material_ids) ? "selected" : ""?>><?php echo $material['Material']['material_name'];?></option>
                                  <?php } ?>                                
                               </select>  
                            </div>
                        </div>
                        <div class="col-md-6 row">
                            <label class="col-sm-4 form-control-label">Project <span class="tx-danger">*</span></label>
                             <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                               <select class="form-control validate[required]"  name="data[Product][project_id]">
                                  <option value="">---Select Project---</option>
                                  <?php foreach ($projects as $project) { ?>
                                    <option value="<?php echo $project['Project']['project_id'] ?>"  <?php echo ($this->request->data['Product']['project_id']==$project['Project']['project_id']) ? "selected" : ""?>><?php echo $project['Project']['project_name'] ?></option>
                                  <?php } ?>                                
                               </select>  
                            </div>
                        </div>
                    </div>
                    <div class="row mg-t-20">
                        <?php
                        $collections = ClassRegistry::init('Collection')->find('all', array('conditions' => array('status !=' => 'Trash','parent_collection'=>0),'order'=>'collection_name ASC'));
                        $subcollections = ClassRegistry::init('Collection')->find('all', array('conditions' => array('status !=' => 'Trash','parent_collection'=>$this->request->data['Product']['collection_id']),'order'=>'collection_name ASC'));
                        ?>
                        <div class="col-md-6 row">
                             <label class="col-sm-4 form-control-label">Collection <span class="tx-danger">*</span></label>
                             <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                               <select class="form-control validate[required] collection"  name="data[Product][collection_id]">
                                  <option value="">---Select Collection---</option>
                                  <?php foreach ($collections as $collection) { ?>
                                    <option value="<?php echo $collection['Collection']['collection_id'] ?>" <?php echo ($this->request->data['Product']['collection_id']==$collection['Collection']['collection_id']) ? "selected" : ""?>><?php echo $collection['Collection']['collection_name'] ?></option>
                                  <?php } ?>                                
                               </select>  
                            </div>
                        </div>
                        <div class="col-md-6 row">
                            <label class="col-sm-4 form-control-label">Subcollection <span class="tx-danger">*</span></label>
                             <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                 <select class="form-control subcollection validate[required] select2"  name="data[Product][subcollection_id][]" multiple="">
                                     <?php
                                     $sub_ids=explode(',',$this->request->data['Product']['subcollection_id']);
                                     foreach($subcollections as $subcollection) {
                                     ?>
                                     <option value="<?php echo $subcollection['Collection']['collection_id'];?>" <?php echo in_array($subcollection['Collection']['collection_id'],$sub_ids) ? "selected" : ""?>><?php echo $subcollection['Collection']['collection_name'];?></option>
                                     <?php } ?>
                                 </select> 
                            </div>
                        </div>
                    </div>
                    <h5 class="addproduct_title">Stone details</h5>
                    <div class="row mg-t-20">
                        <label class="col-sm-2 form-control-label">Attribute <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <?php
                                $attribute = ClassRegistry::init('Attribute')->find('all', array('conditions' => array('status' => "Active"),'order'=>'attribute_name ASC'));
                                $productattributes = ClassRegistry::init('Productattribute')->find('all', array('conditions' => array('product_id' => $this->request->data['Product']['product_id']), 'group' => array('Productattribute.attribute_id')));
                                $product_attri=array();
                                foreach($productattributes as $productattribute){
                                $attributess = ClassRegistry::init('Attribute')->find('first', array('conditions' => array('attribute_id' => $productattribute['Productattribute']['attribute_id'])));
                                $product_attri[]=$attributess['Attribute']['attribute_id'];                                
                                }   

                                                           
                            ?>
                            <select class="form-control" name="" id="attribute_id">
                                <option value="">[--Select Attribute--]</option>
                                <?php foreach ($attribute as $attribute) { 
                                if(in_array($attribute['Attribute']['attribute_id'],$product_attri)){
                                $disabled="disabled";
                                }else{
                                $disabled="";
                                }                               

                                ?>
                                    <option data-number="<?php echo $attribute['Attribute']['attribute_id'];?>"  value="<?php echo $attribute['Attribute']['attribute_id']; ?>" <?php echo $disabled;?>><?php echo $attribute['Attribute']['attribute_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-sm-2 mg-t-10 mg-sm-t-0">
                            <button class="btn btn-primary" type="button" id="attribute_id_add">Add</button>
                        </div>
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-2 form-control-label"> <span class="tx-danger"></span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <table class="table table-striped table-bordered attribute-table">
                                <?php
                                $tr = '';
                                $productattributes = ClassRegistry::init('Productattribute')->find('all', array('conditions' => array('product_id' => $this->request->data['Product']['product_id']), 'group' => array('Productattribute.attribute_id')));
                                foreach ($productattributes as $productattribute) {
                                    $proattributes = ClassRegistry::init('Productattribute')->find('all', array('fields' => array('Productattribute.value as value'), 'conditions' => array('product_id' => $this->request->data['Product']['product_id'])));
                                    $ids = array_map(function ($ar) {
                                        return $ar['Productattribute']['value'];
                                    }, $proattributes);
                                    $attribute = ClassRegistry::init('Attribute')->find('first', array('conditions' => array('attribute_id' => $productattribute['Productattribute']['attribute_id'])));
                                    $tr .= '<tr class="additional" id="'.$attribute['Attribute']['attribute_id'].'">';
                                    $tr .= '<td>' . $attribute['Attribute']['attribute_name'] . '</td>';
                                    if ($attribute['Attribute']['type'] == 'Select') {
                                        $attributevalues = ClassRegistry::init('Attributevalue')->find('all', array('conditions' => array('attribute_id' => $productattribute['Productattribute']['attribute_id'])));
                                        $options = '';
                                        foreach ($attributevalues as $attributevalue) {
                                            if (in_array($attributevalue['Attributevalue']['value'], $ids)) {
                                                $selected = "selected";
                                            } else {
                                                $selected = "";
                                            }
                                            $options .= "<option " . $selected . " value='" . $attributevalue['Attributevalue']['value'] . "'>" . $attributevalue['Attributevalue']['value'] . "</option>";
                                        }
                                        $tr .= '<td><select class="form-control chosen validate[required]" name="data[Productattribute][value][' . $attribute['Attribute']['attribute_id'] . '][]">' . $options;
                                        $tr .= '</select>';
                                        $tr .= '</td>';                                        
                                    } else {
                                        $tr .= '<td><textarea class="form-control validate[required]" name="data[Productattribute][value][' . $attribute['Attribute']['attribute_id'] . ']">' . $productattribute['Productattribute']['value'] . '</textarea></td>';
                                    }
                                    $tr .= '<td class="hide text-center">'
                                            . '<input type="hidden" name="data[Productattribute][attribute_id][]" value="' . $attribute['Attribute']['attribute_id'] . '"/>';
                                  
                                   $tr .= '<i class="fa fa-times remove_attribute" aria-hidden="true"></i>';            $tr .= '</td>';
                                    $tr .= '</tr>';
                                }
                                echo $tr;
                                ?>
                            </table>
                        </div>
                    </div>
                    <h5 class="addproduct_title">Price details</h5>
                      <div class="row mg-t-20">
                        <div class="col-md-6 row">
                            <label class="col-sm-5 form-control-label">Gross weight (g) <span class="tx-danger">*</span></label>
                            <div class="col-sm-7 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control validate[required,custom[positiveNumber]] gross_weight" name="data[Product][gross_weight]" value="<?php echo $this->request->data['Product']['gross_weight'];?>"/>
                            </div>
                        </div>
                        <div class="col-md-6 row">
                              <label class="col-sm-5 form-control-label">Stone price (SGD) <span class="tx-danger"></span></label>
                            <div class="col-sm-7 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control stone_price" name="data[Product][stone_price]" value="<?php echo $this->request->data['Product']['stone_price'];?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="row mg-t-20">
                        <div class="col-md-6 row">
                            <label class="col-sm-5 form-control-label">Stone weight (g) <span class="tx-danger"></span></label>
                            <div class="col-sm-7 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control stone_weight" name="data[Product][stone_weight]" value="<?php echo $this->request->data['Product']['stone_weight'];?>"/>
                            </div>
                        </div>
                        <div class="col-md-6 row">
                              <label class="col-sm-5 form-control-label">Diamond price (SGD) <span class="tx-danger"></span></label>
                            <div class="col-sm-7 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control diamond_price" name="data[Product][diamond_price]" value="<?php echo $this->request->data['Product']['diamond_price'];?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="row mg-t-20">
                        <div class="col-md-6 row">
                              <label class="col-sm-5 form-control-label">Diamond weight (g) <span class="tx-danger"></span></label>
                            <div class="col-sm-7 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control diamond_weight" name="data[Product][diamon_weight]" value="<?php echo $this->request->data['Product']['diamon_weight'];?>"/>
                            </div>
                        </div>
                        <div class="col-md-6 row">
                              <label class="col-sm-5 form-control-label">Other weight (g)<span class="tx-danger"></span></label>
                            <div class="col-sm-7 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control other_weight" name="data[Product][other_weight]" value="<?php echo $this->request->data['Product']['other_weight'];?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="row mg-t-20">
                         <div class="col-md-6 row">
                              <label class="col-sm-5 form-control-label">Project (%) <span class="tx-danger">*</span></label>
                            <div class="col-sm-7 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control validate[required,custom[positiveNumber]] project_percetange" name="data[Product][project_percetange]" value="<?php echo $this->request->data['Product']['project_percetange'];?>"/>
                            </div>
                        </div>
                        <div class="col-md-6 row">
                              <label class="col-sm-5 form-control-label">Gold purity (%)<span class="tx-danger">*</span></label>
                            <div class="col-sm-7 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control validate[required,custom[positiveNumber]]" name="data[Product][gold_purity]" value="<?php echo $this->request->data['Product']['gold_purity'];?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="row mg-t-20">
                        <div class="col-md-6 row">
                              <label class="col-sm-5 form-control-label">Net Weight (g)<span class="tx-danger">*</span></label>
                            <div class="col-sm-7 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control net_weight" name="data[Product][net_weight]" readonly="true" value="<?php echo $this->request->data['Product']['net_weight'];?>"/>
                            </div>
                        </div>
                        <div class="col-md-6 row">
                              <label class="col-sm-5 form-control-label">Making Charges (SGD)<span class="tx-danger">*</span></label>
                            <div class="col-sm-7 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control making_charges" name="data[Product][making_charges]" readonly="true" value="<?php echo $this->request->data['Product']['making_charges'];?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="row mg-t-20">
                        <div class="col-md-6 row">
                              <label class="col-sm-5 form-control-label">Gold Price (SGD)<span class="tx-danger">*</span></label>
                            <div class="col-sm-7 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control gold_price" name="data[Product][gold_price]" readonly="true" value="<?php echo $this->request->data['Product']['gold_price'];?>"/>
                            </div>
                        </div>
                        <div class="col-md-6 row">
                              <label class="col-sm-5 form-control-label">Other charges (SGD)<span class="tx-danger"></span></label>
                            <div class="col-sm-7 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control other_charges" name="data[Product][other_charges]" value="<?php echo $this->request->data['Product']['other_charges'];?>"/>
                            </div>
                        </div>
                    </div>
                    <h5 class="addproduct_title">Commission & Discount <span style="color: #807c7c;font-size: 14px; margin-left: 5px;">(From Making charges)</span></h5>
                    <div class="row mg-t-20">
                            <div class="col-md-6 row">
                                <label class="col-sm-4 form-control-label">Discount in<span class="tx-danger">*</span></label>
                                <div class="col-sm-2 mg-t-10 mg-sm-t-0">
                                     <input class="validate[required]" type="radio" name="data[Product][discount_type]" id="dis_percent" value="Percentage" <?php echo ($this->request->data['Product']['discount_type']=="Percentage") ? "checked" : ""?>/> %
                                </div>
                                <div class="col-sm-2 mg-t-10 mg-sm-t-0">
                                    <input type="radio" class="validate[required]" name="data[Product][discount_type]" id="dis_amnt" value="Fixed"  <?php echo ($this->request->data['Product']['discount_type']=="Fixed") ? "checked" : ""?>/> SGD
                                </div>
                            </div>
                            <div class="col-md-6 row">
                                <label class="col-sm-4 form-control-label">Commission in<span class="tx-danger">*</span></label>
                                <div class="col-sm-2 mg-t-10 mg-sm-t-0">
                                     <input type="radio" name="data[Product][commission_type]" id="com_percent" value="Percentage" <?php echo ($this->request->data['Product']['commission_type']=="Percentage") ? "checked" : ""?>/> %
                                </div>
                                <div class="col-sm-2 mg-t-10 mg-sm-t-0">
                                    <input type="radio" name="data[Product][commission_type]" id="com_amnt" value="Fixed" <?php echo ($this->request->data['Product']['commission_type']=="Fixed") ? "checked" : ""?>/> SGD
                                </div>
                            </div>
                    </div>
                    <div class="row mg-t-20">
                        <div class="col-md-6 row">
                            <label class="col-sm-4 form-control-label">Discount <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control discount validate[required]" name="data[Product][discount]" value="<?php echo $this->request->data['Product']['discount'];?>"/>
                            </div>
                        </div>
                        <div class="col-md-6 row">
                             <label class="col-sm-4 form-control-label">Commission <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control validate[required,custom[positiveNumber]] commission" name="data[Product][commission]" value="<?php echo $this->request->data['Product']['commission'];?>"/>
                            </div>
                        </div>
                    </div>
                     <?php
                        $gold_Rate = ClassRegistry::init('Sitesetting')->find('first', array('conditions' => array('id'=>1)));
                        ?>
                        <input type="hidden" class="gold_rate" name="gold_rate" value="<?php echo $gold_Rate['Sitesetting']['gold_rate']?>"/>
                        <input type="hidden" class="gst" name="gst" value="<?php echo $gold_Rate['Sitesetting']['gst']?>"/>
                        <input type="hidden" class="product_price" name="data[Product][product_price]" value="<?php echo $this->request->data['Product']['product_price']; ?>"/>
                        <input type="hidden" class="discount_value" name="data[Product][discount_value]" value="<?php echo $this->request->data['Product']['discount_value']; ?>"/>
                        <input type="hidden" class="net_price" name="data[Product][net_price]" value="<?php echo $this->request->data['Product']['net_price']; ?>"/>
                        <input type="hidden" class="total_price" name="data[Product][total_price]" value="<?php echo $this->request->data['Product']['total_price']; ?>"/>
                        <input type="hidden" class="regular_price" name="data[Product][regular_price]" value="<?php echo $this->request->data['Product']['regular_price']; ?>"/>
                        <input type="hidden" class="sales_price" name="data[Product][sales_price]" value="<?php echo $this->request->data['Product']['sales_price']; ?>"/>
                        <h5 class="addproduct_title">Out of Stock Remarks</h5>
                        <div class="row mg-t-20">
                            <label class="col-sm-2 form-control-label">Remarks<span class="tx-danger">*</span></label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                   <textarea class="form-control validate[required]" name="data[Product][outofstock_remark]"><?php echo $this->request->data['Product']['outofstock_remark']; ?></textarea>
                                </div>
                        </div>
                        <h5 class="addproduct_last">Product Price  <span style="font-size: 13px;">(SGD)</span> :  <span class="finalprice"><?php echo $this->App->number_format($this->request->data['Product']['total_price']); ?></span></h5>
                    <div class="clearfix form-actions row submit-cancel">
                                    <div class="">
                                        <button class="btn btn-green" type="submit"> <i class="ace-icon fa fa-check bigger-110"></i> Submit </button>
                                    </div>
                                    <div class="">
                                        <a href="<?php echo BASE_URL; ?>admin/products/index" class="btn btn-cancel"/>Cancel</a>
                                    </div>
                                </div>
                </div>   


            </form>
        </div>
    </div>
</div>
<script>
jQuery("#product-submit").submit(function( event ) {
  var files = $('.productimages').length;
  var img = $('.productimages').val();
  var oldphots = $('.old_photos').val(); 
  if((files <= 0) && (img === undefined) && (oldphots === undefined)){ 
  event.preventDefault();
    swal("Error!", "Please upload atleast one product image.", "warning");       
  } 
});
    
    jQuery(document).on('click', '.remove', function () {
        jQuery(this).parent('li').remove();
    }); 
    jQuery(document).on('click', '.remove-file', function () {   
        jQuery(this).closest('li').remove();
    });
</script>
<script>
    jQuery(document).ready(function () {
        jQuery(document).on('change', '.collection', function () {
            var category_id = $(this).val();
            jQuery.ajax({
                type: "POST",
                url: "<?php echo BASE_URL; ?>products/get_subcollections",
                data: 'category_id=' + category_id,
                dataType: 'html',
                success: function (data) {
                    if (data != 'No') {
                        jQuery('.subcollection').html(data);
                        jQuery('.subcollection').select2('destroy');
                        jQuery('.subcollection').select2();
                    }
                }
            });
        });
    });
    jQuery(document).ready(function () {
        jQuery(document).on('change', '.gross_weight', function () {
            var gross_weight = $(this).val();
            var stone_weight = $('.stone_weight').val();
            var diamond_weight = $('.diamond_weight').val();
            var other_weight = $('.other_weight').val();
            var net_weight = gross_weight - stone_weight - diamond_weight - other_weight;
            $('.net_weight').val(net_weight.toFixed(4));
            var gold_rate = $('.gold_rate').val();
            var gold_price = gold_rate * net_weight
            $('.gold_price').val(gold_price.toFixed(4));

            if($('.project_percetange').val() !=""){
            var proj_percentage = $('.project_percetange').val();
            }else{
            var proj_percentage = 0;
            }
            
            var making_charge = gold_price * proj_percentage / 100;
            $('.making_charges').val(making_charge.toFixed(2));
            if($('.stone_price').val() !=""){
                var stone_price=$('.stone_price').val();
            }else{
                var stone_price = 0;
            }
            if($('.diamond_price').val() !=""){
            var diamond_price=$('.diamond_price').val();
            }else{
            var diamond_price=0;
            }
            
            if($('.other_charges').val() !=""){
            var other_charges=$('.other_charges').val();
            }else{
            var other_charges=0;
            }
            
            var product_price = parseFloat(gold_price) + parseFloat(making_charge) + parseFloat(stone_price) + parseFloat(diamond_price) + parseFloat(other_charges);
            $('.product_price').val(product_price);
            $('.regular_price').val(Math.round(product_price));
            if($('.making_charges').val() !=""){
            var making_charges = $('.making_charges').val();
            }else{
            var making_charges = 0;
            }
            if($('.discount').val() !=""){
            var discount = $('.discount').val();
            }else{
            var discount = 0;
            }   

            var gst = $('.gst').val();
            var product_price = $('.product_price').val();
            if($('#dis_percent').is(':checked')) {
               var discount_type = "Percentage";
            }else if($('#dis_amnt').is(':checked')) {
                var discount_type = "Amount";
            }
            if(discount_type=="Percentage"){
                var discount_value = making_charges * discount / 100;
            }else if(discount_type=="Amount"){
                var discount_value = discount;
            }
            if(discount_value !=""){
            $('.discount_value').val(discount_value);
            }else{
            $('.discount_value').val("0");
            }            
            var net_price= product_price - discount_value;
            $('.net_price').val(net_price);
            $('.sales_price').val(Math.round(net_price));
            var gst_price = net_price * gst / 100;
            var total_price = net_price + gst_price;
            $('.total_price').val(parseFloat(total_price).toFixed(2));
             if(total_price > 0){
            $('.finalprice').text(parseFloat(total_price).toFixed(2));
            }
        });
        
        jQuery(document).on('change', '.stone_weight', function () {
            var gross_weight = $('.gross_weight').val();
            var stone_weight = $('.stone_weight').val();
            var diamond_weight = $('.diamond_weight').val();
            var other_weight = $('.other_weight').val();
            var net_weight = gross_weight - stone_weight - diamond_weight - other_weight;
            $('.net_weight').val(net_weight.toFixed(4));
            var gold_rate = $('.gold_rate').val();
            var gold_price = gold_rate * net_weight
            $('.gold_price').val(gold_price.toFixed(4));

            if($('.project_percetange').val() !=""){
            var proj_percentage = $('.project_percetange').val();
            }else{
            var proj_percentage = 0;
            }
            
            var making_charge = gold_price * proj_percentage / 100;
            $('.making_charges').val(making_charge.toFixed(2));
            if($('.stone_price').val() !=""){
                var stone_price=$('.stone_price').val();
            }else{
                var stone_price = 0;
            }
            if($('.diamond_price').val() !=""){
            var diamond_price=$('.diamond_price').val();
            }else{
            var diamond_price=0;
            }
            
            if($('.other_charges').val() !=""){
            var other_charges=$('.other_charges').val();
            }else{
            var other_charges=0;
            }
            
            var product_price = parseFloat(gold_price) + parseFloat(making_charge) + parseFloat(stone_price) + parseFloat(diamond_price) + parseFloat(other_charges);
            $('.product_price').val(product_price);
            $('.regular_price').val(Math.round(product_price));
            if($('.making_charges').val() !=""){
            var making_charges = $('.making_charges').val();
            }else{
            var making_charges = 0;
            }
            if($('.discount').val() !=""){
            var discount = $('.discount').val();
            }else{
            var discount = 0;
            }   

            var gst = $('.gst').val();
            var product_price = $('.product_price').val();
            if($('#dis_percent').is(':checked')) {
               var discount_type = "Percentage";
            }else if($('#dis_amnt').is(':checked')) {
                var discount_type = "Amount";
            }
            if(discount_type=="Percentage"){
                var discount_value = making_charges * discount / 100;
            }else if(discount_type=="Amount"){
                var discount_value = discount;
            }
            if(discount_value !=""){
            $('.discount_value').val(discount_value);
            }else{
            $('.discount_value').val("0");
            }            
            var net_price= product_price - discount_value;
            $('.net_price').val(net_price);
            $('.sales_price').val(Math.round(net_price));
            var gst_price = net_price * gst / 100;
            var total_price = net_price + gst_price;
            $('.total_price').val(parseFloat(total_price).toFixed(2));
             if(total_price > 0){
            $('.finalprice').text(parseFloat(total_price).toFixed(2));
            }
        });
        
        jQuery(document).on('change', '.diamond_weight', function () {
            var gross_weight = $('.gross_weight').val();
            var stone_weight = $('.stone_weight').val();
            var diamond_weight = $('.diamond_weight').val();
            var other_weight = $('.other_weight').val();
            var net_weight = gross_weight - stone_weight - diamond_weight - other_weight;
            $('.net_weight').val(net_weight.toFixed(2));
            var gold_rate = $('.gold_rate').val();
            var gold_price = gold_rate * net_weight
            $('.gold_price').val(gold_price.toFixed(4));
            if($('.project_percetange').val() !=""){
            var proj_percentage = $('.project_percetange').val();
            }else{
            var proj_percentage = 0;
            }
            
            var making_charge = gold_price * proj_percentage / 100;
            $('.making_charges').val(making_charge.toFixed(2));
            if($('.stone_price').val() !=""){
                var stone_price=$('.stone_price').val();
            }else{
                var stone_price = 0;
            }
            if($('.diamond_price').val() !=""){
            var diamond_price=$('.diamond_price').val();
            }else{
            var diamond_price=0;
            }
            
            if($('.other_charges').val() !=""){
            var other_charges=$('.other_charges').val();
            }else{
            var other_charges=0;
            }
            
            var product_price = parseFloat(gold_price) + parseFloat(making_charge) + parseFloat(stone_price) + parseFloat(diamond_price) + parseFloat(other_charges);
            $('.product_price').val(product_price);
            $('.regular_price').val(Math.round(product_price));
            if($('.making_charges').val() !=""){
            var making_charges = $('.making_charges').val();
            }else{
            var making_charges = 0;
            }
            if($('.discount').val() !=""){
            var discount = $('.discount').val();
            }else{
            var discount = 0;
            }   

            var gst = $('.gst').val();
            var product_price = $('.product_price').val();
            if($('#dis_percent').is(':checked')) {
               var discount_type = "Percentage";
            }else if($('#dis_amnt').is(':checked')) {
                var discount_type = "Amount";
            }
            if(discount_type=="Percentage"){
                var discount_value = making_charges * discount / 100;
            }else if(discount_type=="Amount"){
                var discount_value = discount;
            }
            if(discount_value !=""){
            $('.discount_value').val(discount_value);
            }else{
            $('.discount_value').val("0");
            }            
            var net_price= product_price - discount_value;
            $('.net_price').val(net_price);
            $('.sales_price').val(Math.round(net_price));
            var gst_price = net_price * gst / 100;
            var total_price = net_price + gst_price;
            $('.total_price').val(parseFloat(total_price).toFixed(2));
             if(total_price > 0){
            $('.finalprice').text(parseFloat(total_price).toFixed(2));
            }
        });
        
        jQuery(document).on('change', '.other_weight', function () {
            var gross_weight = $('.gross_weight').val();
            var stone_weight = $('.stone_weight').val();
            var diamond_weight = $('.diamond_weight').val();
            var other_weight = $('.other_weight').val();
            var net_weight = gross_weight - stone_weight - diamond_weight - other_weight;
            $('.net_weight').val(net_weight.toFixed(2));
            var gold_rate = $('.gold_rate').val();
            var gold_price = gold_rate * net_weight
            $('.gold_price').val(gold_price.toFixed(2));
            if($('.project_percetange').val() !=""){
            var proj_percentage = $('.project_percetange').val();
            }else{
            var proj_percentage = 0;
            }
            
            var making_charge = gold_price * proj_percentage / 100;
            $('.making_charges').val(making_charge.toFixed(2));
            if($('.stone_price').val() !=""){
                var stone_price=$('.stone_price').val();
            }else{
                var stone_price = 0;
            }
            if($('.diamond_price').val() !=""){
            var diamond_price=$('.diamond_price').val();
            }else{
            var diamond_price=0;
            }
            
            if($('.other_charges').val() !=""){
            var other_charges=$('.other_charges').val();
            }else{
            var other_charges=0;
            }
            
            var product_price = parseFloat(gold_price) + parseFloat(making_charge) + parseFloat(stone_price) + parseFloat(diamond_price) + parseFloat(other_charges);
            $('.product_price').val(product_price);
            $('.regular_price').val(Math.round(product_price));
            if($('.making_charges').val() !=""){
            var making_charges = $('.making_charges').val();
            }else{
            var making_charges = 0;
            }
            if($('.discount').val() !=""){
            var discount = $('.discount').val();
            }else{
            var discount = 0;
            }   

            var gst = $('.gst').val();
            var product_price = $('.product_price').val();
            if($('#dis_percent').is(':checked')) {
               var discount_type = "Percentage";
            }else if($('#dis_amnt').is(':checked')) {
                var discount_type = "Amount";
            }
            if(discount_type=="Percentage"){
                var discount_value = making_charges * discount / 100;
            }else if(discount_type=="Amount"){
                var discount_value = discount;
            }
            if(discount_value !=""){
            $('.discount_value').val(discount_value);
            }else{
            $('.discount_value').val("0");
            }            
            var net_price= product_price - discount_value;
            $('.net_price').val(net_price);
            $('.sales_price').val(Math.round(net_price));
            var gst_price = net_price * gst / 100;
            var total_price = net_price + gst_price;
            $('.total_price').val(parseFloat(total_price).toFixed(2));
             if(total_price > 0){
            $('.finalprice').text(parseFloat(total_price).toFixed(2));
            }
        });
        
        jQuery(document).on('change', '.project_percetange', function () {
            var gold_price = $('.gold_price').val();
            var proj_percentage = $('.project_percetange').val();
            var making_charge = gold_price * proj_percentage / 100;
            $('.making_charges').val(making_charge.toFixed(2));
            if($('.stone_price').val() !=""){
                var stone_price=$('.stone_price').val();
            }else{
                var stone_price = 0;
            }
            
            if($('.diamond_price').val() !=""){
            var diamond_price=$('.diamond_price').val();
            }else{
            var diamond_price=0;
            }

            if($('.other_charges').val() !=""){
            var other_charges=$('.other_charges').val();
            }else{
            var other_charges=0;
            }
            
            var product_price = parseFloat(gold_price) + parseFloat(making_charge) + parseFloat(stone_price) + parseFloat(diamond_price) + parseFloat(other_charges);
            $('.product_price').val(product_price);
            $('.regular_price').val(Math.round(product_price));

            var gst = $('.gst').val();
            var product_price = $('.product_price').val();
            if($('#dis_percent').is(':checked')) {
               var discount_type = "Percentage";
            }else if($('#dis_amnt').is(':checked')) {
                var discount_type = "Amount";
            }
            if(discount_type=="Percentage"){
                var discount_value = making_charges * discount / 100;
            }else if(discount_type=="Amount"){
                var discount_value = discount;
            }
            if(discount_value !=""){
            $('.discount_value').val(discount_value);
            }else{
            $('.discount_value').val("0");
            }            
            var net_price= product_price - discount_value;
            $('.net_price').val(net_price);
            $('.sales_price').val(Math.round(net_price));
            var gst_price = net_price * gst / 100;
            var total_price = net_price + gst_price;
            $('.total_price').val(parseFloat(total_price).toFixed(2));
             if(total_price > 0){
            $('.finalprice').text(parseFloat(total_price).toFixed(2));
            }
        });
        
        jQuery(document).on('change', '.stone_price', function () {
            var gold_price = $('.gold_price').val();
            var proj_percentage = $('.project_percetange').val();
            var making_charge = gold_price * proj_percentage / 100;
            $('.making_charges').val(making_charge.toFixed(2));
            if($('.stone_price').val() !=""){
                var stone_price=$('.stone_price').val();
            }else{
                var stone_price = 0;
            }
             if($('.diamond_price').val() !=""){
            var diamond_price=$('.diamond_price').val();
            }else{
            var diamond_price=0;
            }
            
            if($('.other_charges').val() !=""){
            var other_charges=$('.other_charges').val();
            }else{
            var other_charges=0;
            }
            
            var product_price = parseFloat(gold_price) + parseFloat(making_charge) + parseFloat(stone_price) + parseFloat(diamond_price) + parseFloat(other_charges);
            $('.product_price').val(product_price);
            $('.regular_price').val(Math.round(product_price));

            var gst = $('.gst').val();
         var product_price = $('.product_price').val();
         if($('#dis_percent').is(':checked')) {
               var discount_type = "Percentage";
            }else if($('#dis_amnt').is(':checked')) {
                var discount_type = "Amount";
            }
            if(discount_type=="Percentage"){
                var discount_value = making_charges * discount / 100;
            }else if(discount_type=="Amount"){
                var discount_value = discount;
            }
            if(discount_value !=""){
            $('.discount_value').val(discount_value);
            }else{
            $('.discount_value').val("0");
            }            
            var net_price= product_price - discount_value;
            $('.net_price').val(net_price);
            $('.sales_price').val(Math.round(net_price));
            var gst_price = net_price * gst / 100;
            var total_price = net_price + gst_price;
            $('.total_price').val(parseFloat(total_price).toFixed(2));
             if(total_price > 0){
            $('.finalprice').text(parseFloat(total_price).toFixed(2));
            }
        });
        
        jQuery(document).on('change', '.diamond_price', function () {
            var gold_price = $('.gold_price').val();
            var proj_percentage = $('.project_percetange').val();
            var making_charge = gold_price * proj_percentage / 100;
            $('.making_charges').val(making_charge.toFixed(2));
            if($('.stone_price').val() !=""){
                var stone_price=$('.stone_price').val();
            }else{
                var stone_price = 0;
            }
            if($('.diamond_price').val() !=""){
            var diamond_price=$('.diamond_price').val();
            }else{
            var diamond_price=0;
            }
            
            if($('.other_charges').val() !=""){
            var other_charges=$('.other_charges').val();
            }else{
            var other_charges=0;
            }
            
            var product_price = parseFloat(gold_price) + parseFloat(making_charge) + parseFloat(stone_price) + parseFloat(diamond_price) + parseFloat(other_charges);
            $('.product_price').val(product_price);
            $('.regular_price').val(Math.round(product_price));
            var gst = $('.gst').val();
         var product_price = $('.product_price').val();
         if($('#dis_percent').is(':checked')) {
               var discount_type = "Percentage";
            }else if($('#dis_amnt').is(':checked')) {
                var discount_type = "Amount";
            }
            if(discount_type=="Percentage"){
                var discount_value = making_charges * discount / 100;
            }else if(discount_type=="Amount"){
                var discount_value = discount;
            }
            if(discount_value !=""){
            $('.discount_value').val(discount_value);
            }else{
            $('.discount_value').val("0");
            }            
            var net_price= product_price - discount_value;
            $('.net_price').val(net_price);
            $('.sales_price').val(Math.round(net_price));
            var gst_price = net_price * gst / 100;
            var total_price = net_price + gst_price;
            $('.total_price').val(parseFloat(total_price).toFixed(2));
             if(total_price > 0){
            $('.finalprice').text(parseFloat(total_price).toFixed(2));
            }
        });

        jQuery(document).on('change', '.other_charges', function () {
            var gold_price = $('.gold_price').val();
            var proj_percentage = $('.project_percetange').val();
            var making_charge = gold_price * proj_percentage / 100;
            $('.making_charges').val(making_charge.toFixed(2));
            if($('.stone_price').val() !=""){
                var stone_price=$('.stone_price').val();
            }else{
                var stone_price = 0;
            }
            if($('.diamond_price').val() !=""){
            var diamond_price=$('.diamond_price').val();
            }else{
            var diamond_price=0;
            }
            
            if($('.other_charges').val() !=""){
            var other_charges=$('.other_charges').val();
            }else{
            var other_charges=0;
            }
            
            var product_price = parseFloat(gold_price) + parseFloat(making_charge) + parseFloat(stone_price) + parseFloat(diamond_price) + parseFloat(other_charges);
            $('.product_price').val(product_price);
            $('.regular_price').val(Math.round(product_price));
            if($('.making_charges').val() !=""){
            var making_charges = $('.making_charges').val();
            }else{
            var making_charges = 0;
            }
            if($('.discount').val() !=""){
            var discount = $('.discount').val();
            }else{
            var discount = 0;
            }          
         
         var gst = $('.gst').val();
         var product_price = $('.product_price').val();
         if($('#dis_percent').is(':checked')) {
               var discount_type = "Percentage";
            }else if($('#dis_amnt').is(':checked')) {
                var discount_type = "Amount";
            }
            if(discount_type=="Percentage"){
                var discount_value = making_charges * discount / 100;
            }else if(discount_type=="Amount"){
                var discount_value = discount;
            }
            if(discount_value !=""){
            $('.discount_value').val(discount_value);
            }else{
            $('.discount_value').val("0");
            }            
            var net_price= product_price - discount_value;
            $('.net_price').val(net_price);
            $('.sales_price').val(Math.round(net_price));
            var gst_price = net_price * gst / 100;
            var total_price = net_price + gst_price;
            $('.total_price').val(parseFloat(total_price).toFixed(2));
             if(total_price > 0){
            $('.finalprice').text(parseFloat(total_price).toFixed(2));
            }
        });
        
        jQuery(document).on('change', '.discount', function () {
         var making_charges = $('.making_charges').val();
         var discount = $('.discount').val();
         var gst = $('.gst').val();
         var product_price = $('.product_price').val();
         if($('#dis_percent').is(':checked')) {
               var discount_type = "Percentage";
            }else if($('#dis_amnt').is(':checked')) {
                var discount_type = "Amount";
            }
            if(discount_type=="Percentage"){
                var discount_value = making_charges * discount / 100;
            }else if(discount_type=="Amount"){
                var discount_value = discount;
            }
            $('.discount_value').val(discount_value);
            var net_price= product_price - discount_value;
            $('.net_price').val(net_price);
            $('.sales_price').val(Math.round(net_price));
            var gst_price = net_price * gst / 100;
            var total_price = net_price + gst_price;
            $('.total_price').val(parseFloat(total_price).toFixed(2));
             if(total_price > 0){
            $('.finalprice').text(parseFloat(total_price).toFixed(2));
            }
        });
        jQuery(document).on('change', '#dis_percent', function () {
         var making_charges = $('.making_charges').val();
         var discount = $('.discount').val();
         if(discount !=""){
         var gst = $('.gst').val();
         var product_price = $('.product_price').val();
         var discount_value = making_charges * discount / 100;
         $('.discount_value').val(discount_value);
         var net_price= product_price - discount_value;
         $('.net_price').val(net_price);
         $('.sales_price').val(Math.round(net_price));
         var gst_price = net_price * gst / 100;
         var total_price = net_price + gst_price;
         $('.total_price').val(parseFloat(total_price).toFixed(2));
          if(total_price > 0){
            $('.finalprice').text(parseFloat(total_price).toFixed(2));
            }
         }
        });
        jQuery(document).on('change', '#dis_amnt', function () {
         var making_charges = $('.making_charges').val();
         var discount = $('.discount').val();
         if(discount !=""){
         var gst = $('.gst').val();
         var product_price = $('.product_price').val();
         var discount_value =  discount;
         $('.discount_value').val(discount_value);
         var net_price= product_price - discount_value;
         $('.net_price').val(net_price);
         $('.sales_price').val(Math.round(net_price));
         var gst_price = net_price * gst / 100;
         var total_price = net_price + gst_price;
         $('.total_price').val(parseFloat(total_price).toFixed(2));
          if(total_price > 0){
            $('.finalprice').text(parseFloat(total_price).toFixed(2));
            }
         }
        });
    });
    $('#attribute_id_add').click(function () {
        var id = $('#attribute_id').val();     
        $("#attribute_id option:selected").attr('disabled','disabled');
        $.ajax({
            url: '<?php echo BASE_URL ?>admin/products/getattributes',
            type: 'POST',
            data: 'attribute_id=' + id,
            dataType: 'html',
            success: function (data) {
                $('.attribute-table').append(data);
                //  alert(data);
                $(".chosen").chosen({no_results_text: "Oops, nothing found!"});
            }
        });

    });
    jQuery(document).on('click', '.remove_attribute', function () {
      var id = jQuery(this).closest('.additional').attr("id");         
        $("#attribute_id option[value='" + id + "']").attr("disabled", false);  
        jQuery(this).closest('.additional').remove();
    });  
    jQuery( document ).ready(function() {
    });
   jQuery(function () {
        jQuery('#jewelphotos').on('change', function () {
            var files = $(this)[0].files;

              var j=0;
             
              for (var i = 0; i < files.length; i++) {
                var file = files[i];
                var reader = new FileReader();
                // Note that parameters are reordered due to binding below
                reader.onload = (function(name, event) {
                    var img = event.target.result;
                    $('.sprofile ul').append(
                        "<li><input type='hidden' class='productimages' name='data[Product][productimages][]' value='"+ name + "'/><img class='thumbnail' src='" + img + "'/><a href='javascript:;'><small><i class='fa fa-times remove-file' aria-hidden='true'></i></small></a></li>");
                    }).bind(reader, file.name);
                    reader.readAsDataURL(file);
                }
                
            
            });
        });
</script>

