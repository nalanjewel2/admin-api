<div class="br-mainpanel">
    <div class="br-pagetitle"> 
        <div class="col-md-6">
            <h4><span>       <i class="icon icon ion-ios-book-outline"></i> </span>Product View</h4>
        </div>

    </div><!-- d-flex -->
    <div class="br-pagebody">
        <div class="br-section-wrapper">
            <form action="" class="form-horizontal" method="post" enctype="multipart/form-data">
                <fieldset class="content-group">
                    <div class="form-group">
                        <label class="control-label col-sm-12 col-md-3 col-lg-3">Product Name</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <?php echo $product['Product']['name']; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label  col-sm-12 col-md-3 col-lg-3">Category Name</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <?php
                            $categories = ClassRegistry::init('Category')->find('first', array('conditions' => array('category_id' => $product['Product']['category_id'])));
                            echo $categories['Category']['name'];
                            ?>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-12 col-md-3 col-lg-3">Brand</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <?php
                            $brands = ClassRegistry::init('Brand')->find('first', array('conditions' => array('brand_id' => $product['Product']['brand_id'])));
                            echo $brands['Brand']['name'];
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-12 col-md-3 col-lg-3">Description</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <?php echo $product['Product']['description']; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-12 col-md-3 col-lg-3">Regular Price</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <?php echo "Rs. " . $product['Product']['regular_price']; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-12 col-md-3 col-lg-3">Sales Price</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <?php echo "Rs. " . $product['Product']['sales_price']; ?>
                        </div>
                    </div>
                    <?php
                    $images = ClassRegistry::init('Productimage')->find('all', array('conditions' => array('product_id' => $product['Product']['product_id'])));
                    if (!empty($images)) {
                        ?>
                        <div class="form-group">
                            <label class="control-label  col-sm-12 col-md-3 col-lg-3">Images</label>
                            <?php
                            foreach ($images as $image) {
                                ?>

                                <img src="<?php echo BASE_URL . 'files/vendors/vendor_' . $product['Product']['vendor_id'] . '/' . $image['Productimage']['image']; ?>" class="img-thumb-admin img-responsive"/>


                                </a>
                            <?php }
                            ?>
                        </div>
                    <?php } ?>
                </fieldset>
            </form>
        </div>
    </div>
</div>
<style>
    .form-group{
        display: flex;
    }
    label.control-label {
        color: black;
        font-weight: 500;
    }
    img.img-thumb-admin.img-responsive {
        width: 150px !important;
        height: 100px;
        margin-right: 10px;
    }
    @media(max-width:500px){
        .form-group {
            display: block;
        }
    }
</style>