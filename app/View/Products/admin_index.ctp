<div class="br-mainpanel">
    <div class="br-pagetitle">  
        <div class="col-md-6">
            <h4>Product Management</h4>
        </div>
        <div class="col-md-6">
            <div class="btn-group float-right">
                <a href="<?php echo BASE_URL; ?>admin/products/add" class="btn addbtn"><i class="fas fa-plus-circle"></i>  Add Product</a>
            </div>
        </div>
    </div><!-- d-flex -->    
    <div class="br-pagebody">
        <div class="br-section-wrapper">
            <div class="search">
                <form method="get" action="<?php echo BASE_URL; ?>admin/products/index" class="form-inline validation-form">
                    <ul class="list-unstyled list-inline" style="display: inline-flex;">
                        <li>
                          <div class="input-group">
                                <input type="text" class="form-control validate[required]" value="<?php echo isset($_REQUEST['s']) ? $_REQUEST['s'] : "" ?>"  placeholder="Search product name" name="s">
                                <span class="input-group-btn">
                                <button class="btn bd bg-white tx-gray-600"  name="search" type="submit"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </li>                        
                        <li>
                            <?php if (isset($_REQUEST['search'])) { ?><a class="btn btn-danger" href="<?php echo BASE_URL; ?>admin/products/index">Cancel</a><?php } ?> 
                        </li>
                    </ul>
                </form> 
            </div>
            <?php if(!empty($products)) { ?>
            <div class="table-wrapper">
                <table class="table display responsive nowrap table-bordered  table-striped">
                    <thead class="thead-colored thead-dark">
                        <tr>
                            <th>#</th>     
                            <th>Product Name</th>   
                            
                            <th>Stock</th>    
                            <th>Commission</th> 
                            <th>Discount</th>    
                            <th>Price</th>    
                            <th>Action</th>                         
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = $this->Paginator->counter('{:start}');
                        foreach ($products as $product) {
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>                                  
                                <td><?php echo $product['Product']['product_name']; ?></td>   
                               
                                <td><?php echo $product['Product']['product_qty']; ?></td>
                                <?php
                                if($product['Product']['commission_type']=="Percentage"){
                                    $commission_amnt = $product['Product']['commission'] . " % ";
                                }else if($product['Product']['commission_type']=="Fixed"){
                                    $commission_amnt = $this->App->number_format($product['Product']['commission']) . " SGD " ;
                                }
                                ?>
                                <td><?php echo $commission_amnt;?></td>
                                <?php
                                if($product['Product']['discount_type']=="Percentage"){
                                    $discount_amnt=$product['Product']['making_charges'] * $product['Product']['discount'] / 100;
                                }else if($product['Product']['discount_type']=="Fixed"){
                                    $discount_amnt= $product['Product']['discount'];
                                }
                                ?>
                                <td><?php echo !empty($discount_amnt) ? $discount_amnt." SGD" : ""; ?></td>
                                <td><?php echo $this->App->number_format($product['Product']['total_price']) ." SGD"; ?></td>   
                                <td>
                                    <div class="actions-btns">
                                        <a href="<?php echo BASE_URL; ?>admin/products/edit/<?php echo $product['Product']['product_id']; ?>" class="green btn btn-outline-edit" title="Edit">
                                            <i class="fas fa-pencil-alt"></i>
                                        </a>
                                        <a href="<?php echo BASE_URL; ?>admin/products/delete/<?php echo $product['Product']['product_id']; ?>" class="green deleteconfirm btn btn-outline-delete" title="Delete">   
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </div>
                                </td>
                                                           
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-md-6">
                        <div class="dataTables_info" id="sample-table-2_info" role="status" aria-live="polite">
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page') . ' {:page} ' . __('of') . ' {:pages}, ' . __('showing') . ' {:current} ' . __('records out of') . ' {:count} ' . __('entries')
                            ));
                            ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="dataTables_paginate paging_simple_numbers pull-right" id="sample-table-2_paginate">
                            <ul class="pagination">
                                <?php
                                echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('tag' => 'li', 'class' => 'page-link', 'escape' => false), '<a><i class="fa fa-angle-left"></i></a>', array('class' => 'prev disabled page-link', 'tag' => 'li', 'escape' => false));
                                $numbers = $this->Paginator->numbers();
                                if (empty($numbers)) {
                                    echo '<li class="active page-link"><a>1</a></li>';
                                } else {
                                    echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'class' => 'page-link', 'first' => 'First page', 'currentClass' => 'active', 'currentTag' => 'a'));
                                }
                                echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('tag' => 'li', 'class' => 'page-link', 'escape' => false), '<a><i class="fa fa-angle-right"></i></a>', array('class' => 'next disabled page-link', 'tag' => 'li', 'escape' => false));
                                ?>
                            </ul>
                        </div> 
                    </div>
                </div>
            </div>
            <?php } else { ?>
            <div class="text-center">
                <img src="<?php echo BASE_URL;?>img/no-data.png"/>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<style>
.search select {
    padding: 7px;
}
</style>
<script>
    $(document).ready(function () {
        $('.table-wrapper').doubleScroll();
    });
</script>