<div class="br-mainpanel">
    <div class="br-pagetitle">       
        <div class="col-md-6">
            <h4>Sales person detail</h4>
        </div>
        <div class="col-md-6">
            <div class="btn-group float-right">
                <a href="<?php echo BASE_URL; ?>admin/salesmans/index" class="btn addbtn"><i class="fas fa-arrow-left"></i> Back to List</a>
            </div>
        </div>
    </div><!-- d-flex -->
    <div class="br-pagebody">
        <div class="br-section-wrapper">         
                    <div class="thumbnail">
                        <div class="thumb thumb-rounded thumb-slide">
                            <?php
                            if (!empty($user['User']['profile'])) {
                                ?>
                                <img style="width: 11%;margin: 0 auto;display: block;height: 115px;" src="<?php echo BASE_URL; ?>files/users/<?php echo $user['User']['profile'] ?>" alt="">
                            <?php } else { ?>
                                <img style="width: 11%;margin: 0 auto;display: block;height:115px;" src="<?php echo WEBROOT; ?>img/facebook.jpg" class="wd-32 rounded-circle" alt="">
                            <?php } ?>
                        </div>
                        <div class="caption">
                            <table class="table table-borderless">
                                <tbody><tr>
                                        <td class="text-left" style="color:#000;">User Name</td>
                                        <td class="text-right"><?php echo!empty($user['User']['full_name']) ? $user['User']['full_name'] : ""; ?></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left" style="color:#000;">Email</td>
                                        <td class="text-right"><?php echo $user['User']['email']; ?></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left" style="color:#000;">Mobile</td>
                                        <td class="text-right"><?php echo $user['User']['mobile']; ?></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left" style="color:#000;">Commission earned</td>
                                        <td class="text-right">
                                            <?php echo "0";?>                        
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
           
        </div>
    </div>
</div>
<style>
    @media(max-width:500px){
        .thumb.thumb-rounded.thumb-slide img {
            height: 80px !important;
            width:100px;
        }
    }
</style>

