<div class="br-mainpanel">
    <div class="br-pagetitle">
        <div class="col-md-6">
            <h4>Add Material</h4>
        </div>
        <div class="col-md-6">
            <div class="btn-group float-right">
                <a href="<?php echo BASE_URL; ?>admin/materials/index" class="btn addbtn"><i class="fas fa-arrow-left"></i> Back to list</a>
            </div>
        </div>
    </div>
    <div class="br-pagebody">
        <div class="br-section-wrapper">
            <form method="post" action="#" class="validation_form addform" enctype="multipart/form-data">
                <div class="form-group clearfix row">
                    <label class="col-sm-2 control-label no-padding-right"> Material Name <span class="tx-danger">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" placeholder="Material Name" class="form-control validate[required,custom[onlyLetterSp]]" name="data[Material][material_name]"/>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 control-label no-padding-right"> Image <span class="tx-danger">*</span></label>
                    <div class="col-sm-6">
                        <input type="file" class="form-control validate[required]" name="data[Material][material_image]"/>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 control-label no-padding-right"> </label>
                    <div class="col-sm-6">
                       <p><small>Recommended Size: 100*100</small></p>
                    </div>
                </div>

                <div class="clearfix form-actions row submit-cancel">
                                    <div class="">
                                        <button class="btn btn-green" type="submit"> <i class="ace-icon fa fa-check bigger-110"></i> Submit </button>
                                    </div>
                                    <div class="">
                                        <a href="<?php echo BASE_URL; ?>admin/materials/index" class="btn btn-cancel"/>Cancel</a>
                                    </div>
                                </div>

            </form>
        </div>
    </div>
</div>