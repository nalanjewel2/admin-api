<div class="br-mainpanel">
    <div class="br-pagetitle">   
        <div class="col-md-6">
            <h4>Add Slider</h4>
        </div>
        <div class="col-md-6">
            <div class="btn-group float-right">
                <a href="<?php echo BASE_URL; ?>admin/sliders/index" class="btn addbtn"><i class="fas fa-arrow-left"></i> Back to list</a>
            </div>
        </div>
    </div>
    <div class="br-pagebody">
        <div class="br-section-wrapper">
            <form method="post" action="#" class="validation_form" enctype="multipart/form-data">
                <div class="form-layout form-layout-4">                  
                    <div class="row mg-t-20">
                        <label class="col-sm-2 form-control-label">Image <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="file" name="data[Slider][slider_name]" class="form-control validimage validate[required]" id="fileUpload"/>
                        </div>
                    </div>   
                    <div class="row">
                        <label class="col-sm-2 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                           <p><small>Recommended Size: 1600*800</small></p>
                        </div>
                    </div> 
                    <div class="clearfix form-actions row submit-cancel">
                    <div class="">
                        <button class="btn btn-green" type="submit"> <i class="ace-icon fa fa-check bigger-110"></i> Submit </button>
                    </div>
                    <div class="">
                        <a href="<?php echo BASE_URL; ?>admin/sliders/index" class="btn btn-cancel"/>Cancel</a>
                    </div>
                </div>
                </div> 
            </form>
        </div>
    </div>
</div>

