<div class="br-mainpanel">
    <div class="br-pagetitle"> 
        <div class="col-md-6">
            <h4>Sliders Management</h4>
        </div>

        <div class="col-md-6">
            <div class="btn-group float-right">
                <a href="<?php echo BASE_URL; ?>admin/sliders/add" class="btn addbtn"><i class="fas fa-plus-circle"></i>  Add Slider</a>
            </div>
        </div>
    </div><!-- d-flex -->
    <div class="br-pagebody">
        <div class="br-section-wrapper">          
        <?php if(!empty($banners)) { ?>
            <div class="table-wrapper">
                <table class="table display responsive nowrap table-bordered  table-striped">
                    <thead class="thead-colored thead-dark"> 
                        <tr>
                            <th class="wd-5p">#</th>                       
                            <th class="wd-20p">Image</th>            
                            <th class="text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($banners as $banner) {
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><img src="<?php echo BASE_URL; ?>files/sliders/<?php echo $banner['Slider']['slider_name']; ?>" class="img-responsive slider_img"/></td>                                                
                                <td class="text-right">
                                    <div class="actions">                                       
                                        <a href="<?php echo BASE_URL; ?>admin/sliders/delete/<?php echo $banner['Slider']['slider_id']; ?>" class="green deleteconfirm btn btn-outline-delete" title="Delete">   
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-md-6">
                        <div class="dataTables_info" id="sample-table-2_info" role="status" aria-live="polite">
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page') . ' {:page} ' . __('of') . ' {:pages}, ' . __('showing') . ' {:current} ' . __('records out of') . ' {:count} ' . __('entries')
                            ));
                            ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="dataTables_paginate paging_simple_numbers pull-right" id="sample-table-2_paginate">
                            <ul class="pagination">
                                <?php
                                echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('tag' => 'li', 'class' => 'page-link', 'escape' => false), '<a><i class="fa fa-angle-left"></i></a>', array('class' => 'prev disabled page-link', 'tag' => 'li', 'escape' => false));
                                $numbers = $this->Paginator->numbers();
                                if (empty($numbers)) {
                                    echo '<li class="active page-link"><a>1</a></li>';
                                } else {
                                    echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'class' => 'page-link', 'first' => 'First page', 'currentClass' => 'active', 'currentTag' => 'a'));
                                }
                                echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('tag' => 'li', 'class' => 'page-link', 'escape' => false), '<a><i class="fa fa-angle-right"></i></a>', array('class' => 'next disabled page-link', 'tag' => 'li', 'escape' => false));
                                ?>
                            </ul>
                        </div> 
                    </div>
                </div>
            </div>
            <?php }else{ ?>
            <div class="text-center">
                <img src="<?php echo BASE_URL;?>img/no-data.png"/>
            </div>
            <?php } ?>
        </div>
    </div>
</div>

