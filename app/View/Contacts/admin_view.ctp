<div class="br-mainpanel">
    <div class="br-pagetitle">
        <div class="col-md-6">
            <h4>Enquiry Detail</h4>
        </div>
        <div class="col-md-6">
            <div class="btn-group float-right">
                <a href="<?php echo BASE_URL; ?>admin/contacts/index" class="btn addbtn"><i class="fas fa-arrow-left"></i> Back to list</a>
            </div>
        </div>
    </div><!-- d-flex -->
    <div class="br-pagebody">
        <div class="br-section-wrapper">
            <form action="" class="form-horizontal" method="post" enctype="multipart/form-data">
                <fieldset class="content-group contactus">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-2">Name   </label>
                        <div class="col-lg-9 col-md-9">
                            <?php echo $result['Contact']['name']; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-2">Email   </label>
                        <div class="col-lg-9 col-md-9">
                            <?php echo $result['Contact']['email']; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-2">Phone   </label>
                        <div class="col-lg-9 col-md-9">
                            <?php echo $result['Contact']['mobile']; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-2">Subject   </label>
                        <div class="col-lg-9 col-md-9">
                            <?php echo $result['Contact']['subject']; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-2">Posted Date   </label>
                        <div class="col-lg-9 col-md-9">
                            <?php echo date('d-m-y h:i a', strtotime($result['Contact']['created_date'])); ?>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>