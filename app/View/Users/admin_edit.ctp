<div class="br-mainpanel">
    <div class="br-pagetitle"> 
        <div class="col-md-6">
            <h4>Edit Customer</h4>
        </div>
        <!-- d-flex -->
        <div class="col-md-6">
            <div class="btn-group float-right">
                <a href="<?php echo BASE_URL; ?>admin/users/index" class="btn addbtn"><i class="fas fa-arrow-left"></i> Back to list</a>
            </div>
        </div>
    </div>
    <div class="br-pagebody">
        <div class="br-section-wrapper">
            <form method="post" action="#" class="validation_form addform" enctype="multipart/form-data">

                <div class="form-group clearfix row">
                    <label class="col-sm-2 control-label no-padding-right"> Name <span class="tx-danger">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" placeholder="Name" class="form-control validate[required]" name="data[User][full_name]" value="<?php echo $user['User']['full_name'] ?>"/>
                    </div>                
                </div>


                <div class="form-group clearfix row">
                    <label class="col-sm-2 control-label no-padding-right"> Email <span class="tx-danger">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" placeholder="Email" class="form-control validate[required,custom[email]]" name="data[User][email]" value="<?php echo $user['User']['email'] ?>"/>
                    </div>
                </div> 


                <div class="form-group clearfix row">
                    <label class="col-sm-2 control-label no-padding-right"> Phone Number <span class="tx-danger"></span></label>
                    <div class="col-sm-6">
                        <input type="text" placeholder="Mobile Number" class="form-control" name="data[User][mobile]" value="<?php echo $user['User']['mobile'] ?>"/>
                    </div>
                </div>
                <div class="form-group clearfix row">
                    <label class="col-sm-2 control-label no-padding-right"> Password <span class="tx-danger"></span></label>
                    <div class="col-sm-6">
                        <input type="password" class="form-control" name="data[User][password]" value="<?php echo $user['User']['passwrd_text'] ?>"/>
                    </div>
                </div>
                <div class="form-group clearfix row">
                    <label class="col-sm-2 control-label no-padding-right"> Status </label>
                    <div class="col-sm-6">
                      <select class="form-control" name="data[User][status]">
                      <option value="Active" <?php echo !empty($user['User']['status']=="Active") ? "selected" : ""?>>Active</option>
                      <option value="Inactive" <?php echo !empty($user['User']['status']=="Inactive") ? "selected" : ""?>>Inactive</option>
                      </select>
                    </div>

                </div>
                <div class="form-group clearfix row">
                    <label class="col-sm-2 control-label no-padding-right"> Profile </label>
                    <div class="col-sm-6">
                        <input type="file" class="form-control" name="data[User][profile]"/>
                        <p><small>Recommended Size: 150*150</small></p>
                    </div>

                </div>
                <?php if (!empty($user['User']['profile'])) { ?>
                <div class="form-group clearfix row">
                    <label class="col-sm-2 control-label no-padding-right"> </label>
                    <div class="col-sm-6">
                    <img src="<?php echo BASE_URL; ?>files/users/<?php echo $user['User']['profile']; ?>" class="form-control userdp"/>
                    </div>
                    </div>
                <?php } ?>
               <div class="clearfix form-actions row submit-cancel">
                                    <div class="">
                                        <button class="btn btn-green" type="submit"> <i class="ace-icon fa fa-check bigger-110"></i> Submit </button>
                                    </div>
                                    <div class="">
                                        <a href="<?php echo BASE_URL; ?>admin/users/index" class="btn btn-cancel"/>Cancel</a>
                                    </div>
                                </div>

            </form>
        </div>
    </div>
</div>

