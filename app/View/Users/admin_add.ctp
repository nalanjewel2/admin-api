<div class="br-mainpanel">
    <div class="br-pagetitle">
        <div class="col-md-6">
            <h4>Add Customer</h4>
        </div>
        <div class="col-md-6">
            <div class="btn-group float-right">
                <a href="<?php echo BASE_URL; ?>admin/users/index" class="btn addbtn"><i class="fas fa-arrow-left"></i> Back to list</a>
            </div>
        </div>
    </div>
    <div class="br-pagebody">
        <div class="br-section-wrapper">
            <form method="post" action="#" class="validation_form addform" enctype="multipart/form-data">

                <div class="form-group clearfix row">
                    <label class="col-sm-2 control-label no-padding-right"> Name <span class="tx-danger">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" placeholder="Name" class="form-control validate[required,custom[onlyLetterSp]]" name="data[User][full_name]"/>
                    </div>
                </div>


                <div class="form-group clearfix row">
                    <label class="col-sm-2 control-label no-padding-right"> Email <span class="tx-danger">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" placeholder="Email" class="form-control validate[required,custom[email]]" name="data[User][email]"/>
                    </div>
                </div> 


                <div class="form-group clearfix row">
                    <label class="col-sm-2 control-label no-padding-right"> Phone Number <span class="tx-danger"></span></label>
                    <div class="col-sm-6">
                        <input type="text" placeholder="Mobile Number" class="form-control" name="data[User][mobile]"/>
                    </div>
                </div>
                <div class="clearfix form-actions row submit-cancel">
                                    <div class="">
                                        <button class="btn btn-green" type="submit"> <i class="ace-icon fa fa-check bigger-110"></i> Submit </button>
                                    </div>
                                    <div class="">
                                        <a href="<?php echo BASE_URL; ?>admin/users/index" class="btn btn-cancel"/>Cancel</a>
                                    </div>
                                </div>

            </form>
        </div>
    </div>
</div>