<div class="br-mainpanel">
    <div class="br-pagetitle">       
        <div class="col-md-6">
            <h4>Customer Detail</h4>
        </div>
        <div class="col-md-6">
            <div class="btn-group float-right">
                <a href="<?php echo BASE_URL; ?>admin/users/index" class="btn addbtn"><i class="fas fa-arrow-left"></i> Back to list</a>
            </div>
        </div>
    </div><!-- d-flex -->
    <div class="br-pagebody">
        <div class="br-section-wrapper">        
                    <div class="views row">
                    <div class="col-md-5">                        
                        <div class="caption">
                            <table class="table table-borderless">
                                <tbody><tr>
                                        <td class="text-left" style="color:#000;"><b>Customer Name</b></td>
                                        <td class="text-right"><?php echo!empty($user['User']['full_name']) ? $user['User']['full_name'] : ""; ?></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left" style="color:#000;"><b>Email</b></td>
                                        <td class="text-right" style="word-break: break-all;"><?php echo $user['User']['email']; ?></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left" style="color:#000;"><b>Mobile</b></td>
                                        <td class="text-right"><?php echo $user['User']['mobile']; ?></td>
                                    </tr>                                 
                                    <tr>
                                        <td class="text-left" style="color:#000;"><b>Registered Date</b></td>
                                        <td class="text-right"><?php echo date('d-m-y h:i a',strtotime($user['User']['created_date'])); ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        </div>
                        <div class="ml-md-auto col-md-3">
                        <div class="thumb thumb-rounded thumb-slide text-center">

                            <?php
                            if (!empty($user['User']['profile'])) {
                                ?>
                                <img  src="<?php echo BASE_URL; ?>files/users/<?php echo $user['User']['profile'] ?>" alt="">
                            <?php } else { ?>
                                <img style="" src="<?php echo WEBROOT; ?>img/facebook.jpg" class="wd-32 rounded-circle" alt="">
                            <?php } ?>
                                <p>Customer Profile</p>
                        </div>
                        </div>
                        </div>                       
                    </div>
           
        </div>
    </div>
