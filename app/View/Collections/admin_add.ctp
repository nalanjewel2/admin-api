<div class="br-mainpanel">
    <div class="br-pagetitle">        
        <div class="col-md-7">
            <h4>Add Collection <span style="font-size: 13px;color: #616161;"> ( Collection and Sub collection will be added in the same page ) </span></h4>
        </div>
        <div class="col-md-5">
            <div class="btn-group float-right">
                <a href="<?php echo BASE_URL; ?>admin/collections/index" class="btn addbtn"><i class="fas fa-arrow-left"></i> Back to list</a>
            </div>
        </div>
    </div>
    <div class="br-pagebody">
        <div class="br-section-wrapper">
            <form method="post" action="#" class="validation_form" enctype="multipart/form-data">
                <div class="form-layout form-layout-4">
                    <div class="row">
                        <label class="col-sm-2 form-control-label">Collection Name <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" placeholder="Collection Name" class="form-control validate[required,custom[onlyLetterSp]]" name="data[Collection][collection_name]"/>
                        </div>
                    </div><!-- row -->  
                    <div class="row mg-t-20">
                        <label class="col-sm-2 form-control-label">Parent collection <span class="tx-danger"></span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select class="form-control" name="data[Collection][parent_collection]" style="padding: 7px;">
                                <option value="">[--- Select Parent collection ---]</option>
                                <?php
                                $categories = ClassRegistry::init('Collection')->find('all', array('conditions' => array('status' => 'Active','parent_collection'=>0)));
                                foreach ($categories as $category) {
                                    ?>
                                    <option value="<?php echo $category['Collection']['collection_id']; ?>"><?php echo $category['Collection']['collection_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div><!-- row -->   
                    <div class="clearfix form-actions row submit-cancel">
                                    <div class="">
                                        <button class="btn btn-green" type="submit"> <i class="ace-icon fa fa-check bigger-110"></i> Submit </button>
                                    </div>
                                    <div class="">
                                        <a href="<?php echo BASE_URL; ?>admin/collections/index" class="btn btn-cancel"/>Cancel</a>
                                    </div>
                                </div>
                </div> 
            </form>
        </div>
    </div>
</div>