<div class="br-mainpanel">
    <div class="br-pagetitle">
        
        <div class="col-md-6">
            <h4>Edit Collection</h4>
        </div>
        <div class="col-md-6">
            <div class="btn-group float-right">
                <a href="<?php echo BASE_URL; ?>admin/collections/index" class="btn addbtn"><i class="fas fa-arrow-left"></i> Back to list</a>
            </div>
        </div>
    </div>
    <div class="br-pagebody">
        <div class="br-section-wrapper">
            <form method="post" action="#" class="validation_form" enctype="multipart/form-data">
                <div class="form-layout form-layout-4"> 
                <div class="row mg-t-20">
                        <label class="col-sm-2 form-control-label">Collection Name <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" placeholder="Collection Name" class="form-control validate[required,custom[onlyLetterSp]]" name="data[Collection][collection_name]" value="<?php echo $this->request->data['Collection']['collection_name'] ?>"/>
                        </div>
                    </div><!-- row --> 
                <div class="row mg-t-20">
                        <label class="col-sm-2 form-control-label">Parent Collection <span class="tx-danger"></span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <?php
                            $categories = ClassRegistry::init('Collection')->find('all', array('conditions' => array('status' => 'Active','parent_collection'=>0)));
                            ?>
                            <select class="form-control" name="data[Collection][parent_collection]" style="padding: 7px;">
                                <option value="0">[--Select Parent Collection--]</option>
                                <?php foreach ($categories as $category) { ?>
                                    <option <?php echo (!empty($this->request->data['Collection']['parent_collection']) && $this->request->data['Collection']['parent_collection'] == $category['Collection']['collection_id']) ? "selected" : "" ?> value="<?php echo $category['Collection']['collection_id']; ?>"><?php echo $category['Collection']['collection_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div><!-- row --> 
                    <div class="clearfix form-actions row submit-cancel">
                                    <div class="">
                                        <button class="btn btn-green" type="submit"> <i class="ace-icon fa fa-check bigger-110"></i> Submit </button>
                                    </div>
                                    <div class="">
                                        <a href="<?php echo BASE_URL; ?>admin/collections/index" class="btn btn-cancel"/>Cancel</a>
                                    </div>
                                </div>>
                </div> 
            </form>
        </div>
    </div>
</div>