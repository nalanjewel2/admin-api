<div class="static-page mt-5 mb-5">
    <div class="container">
        <div class="card">
            <div class="card-header">
                <?php echo $result['Staticpage']['pagename']; ?>
            </div>
            <div class="card-body">
                <?php echo $result['Staticpage']['page_content']; ?>
            </div>
        </div>
    </div>
</div> 
<style>
    .static-page.mt-5.mb-5 {
        margin: 15px;
    }
    .card-header {
    font-size: 25px;
    color: #22A052;
}
.card-body {
    font-size: 16px;
}
.card {
    padding: 15px;
    box-shadow: 0 0 5px 0px #c7c7c7;
}
</style>