<div class="br-mainpanel">
    <div class="br-pagetitle">
        <div class="col-md-6">
            <h4>Edit - <?php echo $this->request->data['Staticpage']['page_title'];?></h4>
        </div>
        <div class="col-md-6">
            <div class="btn-group float-right">
                <a href="<?php echo BASE_URL; ?>admin/staticpages/index" class="btn addbtn"><i class="fas fa-arrow-left"></i> Back to list</a>
            </div>
        </div>
    </div><!-- d-flex -->
    <div class="br-pagebody">
        <div class="br-section-wrapper">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <form action="" class="form-horizontal validation_form" method="post" enctype="multipart/form-data">
                            <fieldset class="content-group">
                                <div class="form-group clearfix">
                                    <label class="control-label"> Page Name <span class="tx-danger">*</span></label>
                                    <div class="">
                                        <input type="text" readonly="true" class="form-control validate[required]" name="data[Staticpage][page_title]" value="<?php echo (!empty($this->request->data['Staticpage']['page_title'])) ? $this->request->data['Staticpage']['page_title'] : "" ?>" />
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <label class="control-label"> Page content <span class="tx-danger">*</span></label>
                                    <div class="">
                                        <textarea name="data[Staticpage][page_content]" class="form-control validate[required]" rows="12"><?php echo (!empty($this->request->data['Staticpage']['page_content'])) ? $this->request->data['Staticpage']['page_content'] : "" ?></textarea>
                                    </div>
                                </div>                          
                                <div class="clearfix form-actions row submit-cancel">
                                    <div class="">
                                        <button class="btn btn-green" type="submit"> <i class="ace-icon fa fa-check bigger-110"></i> Submit </button>
                                    </div>
                                    <div class="">
                                        <a href="<?php echo BASE_URL; ?>admin/staticpages/index" class="btn btn-cancel"/>Cancel</a>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>