<div class="br-mainpanel">
    <div class="br-pagetitle">
        <div class="col-md-6">
            <h4>Sales Person Management</h4>
        </div>
        <div class="col-md-6">
            <div class="btn-group float-right">
                <a href="<?php echo BASE_URL; ?>admin/salesmans/add" class="btn addbtn"><i class="fas fa-plus-circle"></i>  Add Sales person</a>
            </div>
        </div>
    </div><!-- d-flex -->
    <div class="br-pagebody">
        <div class="br-section-wrapper">
            <div class="search">
                <form method="get" action="<?php echo BASE_URL; ?>admin/salesmans/index" class="form-inline validation-form searchform">
                    <ul class="list-unstyled list-inline" style="display: inline-flex;">
                        <li>
                            <div class="input-group">
                                <input type="text" class="form-control validate[required]" value="<?php echo isset($_REQUEST['s']) ? $_REQUEST['s'] : "" ?>"  placeholder="Search name, email, mobile" name="s">
                                <span class="input-group-btn">
                                <button class="btn bd bg-white tx-gray-600"  name="search" type="submit"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </li>
                        <li>
                            <?php if (isset($_REQUEST['search'])) { ?><a class="btn btn-danger" href="<?php echo BASE_URL; ?>admin/salesmans/index">Cancel</a><?php } ?> 
                        </li>
                    </ul>
                </form> 
           
                <ul class="list-unstyled list-inline statusfilter">
                        <li>
                             <form method="get" action="<?php echo BASE_URL; ?>admin/salesmans/index" class="form-inline validation-form">
                            <label> Filter </label>
                            <select class="form-control" name="status" onchange="this.form.submit()">
                                <option value="All" <?php echo (!empty($_REQUEST['status'])) ? (($_REQUEST['status']=="All") ? "selected" : "") : ""?>>ALL</option>
                                <option value="Active" <?php echo (!empty($_REQUEST['status'])) ? (($_REQUEST['status']=="Active") ? "selected" : "") : ""?>>Active</option>
                                <option value="Inactive" <?php echo (!empty($_REQUEST['status'])) ? (($_REQUEST['status']=="Inactive") ? "selected" : "") : ""?>>Inactive</option>
                            </select>
                               </form>
                        </li>
                </ul>
         
            </div>
            <?php if(!empty($users)) {?>
            <div class="table-wrapper">
                <table class="table display responsive nowrap table-bordered  table-striped">
                    <thead class="thead-colored thead-dark">                        
                        <tr>
                            <th>#</th>
                            <th>Registered On</th>
                            <th>Name</th>                                    
                            <th>Email</th>
                            <th>Phone No</th>      
                            <th>Commissions Earned</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = $this->Paginator->counter('{:start}');
                        foreach ($users as $user) {
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td> 
                                <td><?php echo date('d-m-y h:i a',strtotime($user['User']['created_date'])); ?></td>      
                                <td><?php echo!empty($user['User']['full_name']) ? $user['User']['full_name'] : "-"; ?></td>                                                  
                                <td style="word-break: break-all;"><?php echo!empty($user['User']['email']) ? $user['User']['email'] : "-"; ?></td>
                                <td><?php echo $user['User']['mobile']; ?></td>      
                                <td>-</td>
                                <td>
                                    <a data-toggle="modal" title="Update Status"  class="label-<?php echo $user['User']['status']; ?>" data-status="<?php echo $user['User']['status']; ?>" data-id="<?php echo $user['User']['user_id']; ?>" href="javascript:;" data-toggle="modal" data-target="#updatestatus">
                                       <?php echo $user['User']['status']; ?>
                                    </a>

                                </td>

                                <td>
                                    <div class="actions-btns">               
                                        <a href="<?php echo BASE_URL; ?>admin/salesmans/view/<?php echo $user['User']['user_id']; ?>" class="green btn btn-outline-view"  title="View">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                        <a data-toggle="modal" title="Update Password "  class="green btn btn-outline-password" data-email="<?php echo $user['User']['email']; ?>" " data-name="<?php echo $user['User']['full_name']; ?>" data-pass="<?php echo $user['User']['passwrd_text']; ?>" data-id="<?php echo $user['User']['user_id']; ?>" href="javascript:;" data-toggle="modal" data-target="#updatepassword">
                                                <i class="fa fa-key" aria-hidden="true" ></i>
                                            </a>
                                        <a href="<?php echo BASE_URL; ?>admin/salesmans/edit/<?php echo $user['User']['user_id']; ?>" class="green btn btn-outline-edit"  title="Edit">
                                            <i class="fas fa-pencil-alt"></i>
                                        </a>
                                        <a href="<?php echo BASE_URL; ?>admin/salesmans/delete/<?php echo $user['User']['user_id']; ?>" class="green deleteconfirm btn btn-outline-delete"  title="Delete">   
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-md-6">
                        <div class="dataTables_info" id="sample-table-2_info" role="status" aria-live="polite">
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page') . ' {:page} ' . __('of') . ' {:pages}, ' . __('showing') . ' {:current} ' . __('records out of') . ' {:count} ' . __('entries')
                            ));
                            ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="dataTables_paginate paging_simple_numbers pull-right" id="sample-table-2_paginate">
                            <ul class="pagination">
                                <?php
                                echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('tag' => 'li', 'class' => 'page-link', 'escape' => false), '<a><i class="fa fa-angle-left"></i></a>', array('class' => 'prev disabled page-link', 'tag' => 'li', 'escape' => false));
                                $numbers = $this->Paginator->numbers();
                                if (empty($numbers)) {
                                    echo '<li class="active page-link"><a>1</a></li>';
                                } else {
                                    echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'class' => 'page-link', 'first' => 'First page', 'currentClass' => 'active', 'currentTag' => 'a'));
                                }
                                echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('tag' => 'li', 'class' => 'page-link', 'escape' => false), '<a><i class="fa fa-angle-right"></i></a>', array('class' => 'next disabled page-link', 'tag' => 'li', 'escape' => false));
                                ?>
                            </ul>
                        </div> 
                    </div>
                </div>
            </div>
            <?php }else { ?>
            <div class="text-center">
                <img src="<?php echo BASE_URL;?>img/no-data.png"/>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<div class="modal fade effect-flip-vertical" id="updatepassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="display: block;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Update Password</h4>
            </div>
            <form method="post" action="<?php echo BASE_URL; ?>admin/salesmans/updatepassword">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" readonly="true" value="" class="form-control" id="vendor-name"/>
                        <label>Email</label>
                        <input type="text" readonly="true" value="" class="form-control" id="vendor-email"/>
                        <label>New Password</label>
                        <input type="password" name="data[User][password]" id="vendor-pass" class="form-control"/>
                        <input type="hidden" name="data[User][user_id]" id="vendor-id"/>
                    </div>
                </div>
                <div class="modal-footer">                  
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form> 
        </div>
    </div>
</div>
<div class="modal fade effect-flip-vertical" id="updatestatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="display: block;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Update Status</h4>
            </div>
            <form method="post" action="<?php echo BASE_URL; ?>admin/salesmans/updatestatus">
                <div class="modal-body">
                    <div class="form-group">
                        <label>status</label>
                        <select class="form-control" name="data[User][status]" id="user-status">                      
                            <option value="Active">Active</option>
                            <option value="Inactive">Inactive</option>
                        </select>

                        <input type="hidden" name="data[User][user_id]" id="user-id"/>
                    </div>
                </div>
                <div class="modal-footer">                  
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form> 
        </div>
    </div>
</div>
<script>
    $('#updatestatus').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var modal = $(this)
        modal.find('#user-id').val(button.attr('data-id'));
        modal.find('#user-status').val(button.attr('data-status'));
    });
    $('#updatepassword').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var modal = $(this)
        modal.find('#vendor-name').val(button.attr('data-name'));
        modal.find('#vendor-email').val(button.attr('data-email'));
        modal.find('#vendor-id').val(button.attr('data-id'));
        modal.find('#vendor-pass').val(button.attr('data-pass'));
    });
     $(document).ready(function () {
        $('.table-wrapper').doubleScroll();
    });
</script> 