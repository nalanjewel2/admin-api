<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $setting = ClassRegistry::init('Sitesetting')->find('first'); ?>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo "Nalan Jewel"; ?></title>
        <!-- Global stylesheets -->      
        <link href="<?php echo WEBROOT; ?>back/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo WEBROOT; ?>back/lib/Ionicons/css/ionicons.css" rel="stylesheet">
        <link href="<?php echo WEBROOT; ?>back/lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
        <link href="<?php echo WEBROOT; ?>back/lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
        <link href="<?php echo WEBROOT; ?>back/lib/rickshaw/rickshaw.min.css" rel="stylesheet">
        <link href="<?php echo WEBROOT; ?>back/lib/select2/css/select2.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo WEBROOT; ?>back/css/chosen.min.css">
        <link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo WEBROOT; ?>back/css/validationEngine.jquery.css">
        <link rel="stylesheet" href="<?php echo WEBROOT; ?>back/css/bracket.css">
        <link rel="stylesheet" href="<?php echo WEBROOT; ?>back/css/custom.css">
        <link rel="stylesheet" href="<?php echo WEBROOT; ?>back/css/line-awesome.min.css"> 
        <link rel="shortcut icon" href="<?php echo WEBROOT; ?>img/<?php echo $setting['Sitesetting']['fav_icon']; ?>" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous"/>        <script src="<?php echo WEBROOT; ?>back/lib/jquery/jquery.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" rel="stylesheet"/>
        <script src="<?php echo WEBROOT; ?>back/lib/popper.js/popper.js"></script>
        <script src="<?php echo WEBROOT; ?>back/lib/bootstrap/js/bootstrap.js"></script>
        <script src="<?php echo WEBROOT; ?>back/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
        <script src="<?php echo WEBROOT; ?>back/lib/moment/moment.js"></script>
        <script src="<?php echo WEBROOT; ?>back/lib/jquery-ui/jquery-ui.js"></script>
        <script src="<?php echo WEBROOT; ?>back/lib/jquery-switchbutton/jquery.switchButton.js"></script>
        <script src="<?php echo WEBROOT; ?>back/js/chosen.jquery.min.js"></script>
        <script src="<?php echo WEBROOT; ?>back/lib/select2/js/select2.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>
        <script src="<?php echo WEBROOT; ?>back/lib/peity/jquery.peity.js"></script>
        <script src="<?php echo WEBROOT; ?>back/js/bracket.js"></script>
        <script src="<?php echo WEBROOT; ?>back/js/jquery.validationEngine.js"></script>
        <script src="<?php echo WEBROOT; ?>back/js/jquery.validationEngine-en.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo WEBROOT; ?>back/js/doubleScroll.js"></script>
    </head>
    <body>
        <!-- ########## START: LEFT PANEL ########## -->
        <div class="br-logo text-center"><a href="#"><span></span><span style="color:#000;font-weight: 500;"><img src="<?php echo BASE_URL;?>img/logo.png"/></span></a></div>
        <?php echo $this->Element('admin_sidebar'); ?>
        <!-- ########## END: LEFT PANEL ########## -->
        <!-- ########## START: HEAD PANEL ########## -->
        <?php echo $this->Element('admin_header'); ?>
        <!-- ########## END: HEAD PANEL ########## -->
        <?php
        $success = $this->Session->flash('success');
        if (!empty($success)) {
            ?>
            <script>
                swal("Success!", "<?php echo $success; ?>", "success")
            </script>
        <?php } ?>
        <?php
        $danger = $this->Session->flash('danger');
        if (!empty($danger)) {
            ?>
            <script>
                swal("Error!", "<?php echo $danger; ?>", "error")
            </script>
        <?php } ?>
        <?php echo $this->fetch('content'); ?>      
        <style>
            .withoutchoosen {
                display: none !important;
            }.chosen-container{
                width: 100% !important;
            }.chosen-container-single .chosen-single{
                height: 40px;
                padding: 0 0 0 33px !important;
            }.datepicker {
                padding: 13px !important;
            }
        </style>
        <script>
            $(document).on('click', '.deleteconfirm', function (e) {
                var form = $(this);
                e.preventDefault();
                swal({
                    title: "",
                    text: "Are you sure want to delete?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: true,
                },
                        function (isConfirm) {
                            if (isConfirm) {
                                window.location.href = form.attr('href');
                            }
                        });
            });
            $('.validation_form').validationEngine({scroll: false});

            $(document).ready(function () {
                $(".chosen-select").chosen({});
                $('.chosen-select').css('display', 'block');
            });
            jQuery('.date').datepicker({
                multidate: true,
                format: 'dd-mm-yyyy'
            });
            $(function () {
                'use strict'
                // FOR DEMO ONLY
                // menu collapsed by default during first page load or refresh with screen
                // having a size between 992px and 1299px. This is intended on this page only
                // for better viewing of widgets demo.
                $(window).resize(function () {
                    minimizeMenu();
                });
                minimizeMenu();
                function minimizeMenu() {
                    if (window.matchMedia('(min-width: 992px)').matches && window.matchMedia('(max-width: 1299px)').matches) {
                        // show only the icons and hide left menu label by default
                        $('.menu-item-label,.menu-item-arrow').addClass('op-lg-0-force d-lg-none');
                        $('body').addClass('collapsed-menu');
                        $('.show-sub + .br-menu-sub').slideUp();
                    } else if (window.matchMedia('(min-width: 1300px)').matches && !$('body').hasClass('collapsed-menu')) {
                        $('.menu-item-label,.menu-item-arrow').removeClass('op-lg-0-force d-lg-none');
                        $('body').removeClass('collapsed-menu');
                        $('.show-sub + .br-menu-sub').slideDown();
                    }
                }
            });
        </script>
        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-36251023-1']);
            _gaq.push(['_setDomainName', 'jqueryscript.net']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();
        </script>
    </body>
</html>
