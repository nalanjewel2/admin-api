<?php $settings = ClassRegistry::init('Sitesetting')->find('first'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><?php echo $settings['Sitesetting']['site_title']; ?></title>
        <meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type" />
    </head>
    <body>

        <div class="wrapper" style="border:3px solid #454EC3; padding:23px;">
            <img src="<?php echo BASE_URL; ?>img/<?php echo $settings['Sitesetting']['logo']; ?>" alt="logo"  border="0" style="max-width: 150px;"/>
            <hr style="border-bottom: 3px solid #454EC3;border-top: none;"/>
            <?php echo $this->fetch('content'); ?>
            <?php /* ?><p>Regards,</p>
              <p><?php echo $settings['Sitesetting']['site_title']; ?></p><?php */ ?>
        </div>
    </body>
</html>
<style>
    .mailtable {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    .mailtable td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

</style>

