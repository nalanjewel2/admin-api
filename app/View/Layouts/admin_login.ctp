<!DOCTYPE html>
<html lang="en">
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <?php $setting = ClassRegistry::init('Sitesetting')->find('first'); ?>

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo "Nalan Jewel"; ?></title>
        <link rel="shortcut icon" href="<?php echo WEBROOT; ?>img/<?php echo $setting['Sitesetting']['fav_icon']; ?>" />
        <!-- vendor css -->
        <link href="<?php echo WEBROOT; ?>back/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo WEBROOT; ?>back/lib/Ionicons/css/ionicons.css" rel="stylesheet">
        <link href="<?php echo WEBROOT; ?>back/lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
        <link href="<?php echo WEBROOT; ?>back/lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
        <link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo WEBROOT; ?>back/css/validationEngine.jquery.css">
        <link rel="stylesheet" href="<?php echo WEBROOT; ?>back/css/bracket.css">
        <link rel="stylesheet" href="<?php echo WEBROOT; ?>back/css/custom.css">
        <!-- Core JS files -->
        <script src="<?php echo WEBROOT; ?>back/lib/jquery/jquery.js"></script>
        <script src="<?php echo WEBROOT; ?>back/lib/popper.js/popper.js"></script>
        <script src="<?php echo WEBROOT; ?>back/lib/bootstrap/js/bootstrap.js"></script>
        <script src="<?php echo WEBROOT; ?>back/js/jquery.validationEngine.js"></script>
        <script src="<?php echo WEBROOT; ?>back/js/jquery.validationEngine-en.js"></script>
        <script src="<?php echo WEBROOT; ?>back/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
        <script src="<?php echo WEBROOT; ?>back/lib/moment/moment.js"></script>
        <script src="<?php echo WEBROOT; ?>back/lib/jquery-ui/jquery-ui.js"></script>
        <script src="<?php echo WEBROOT; ?>back/lib/jquery-switchbutton/jquery.switchButton.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>
        <script src="<?php echo WEBROOT; ?>back/lib/peity/jquery.peity.js"></script>
        <script src="<?php echo WEBROOT; ?>back/js/bracket.js"></script>

    </head>
    <body>
        <?php
        $success = $this->Session->flash('success');
        if (!empty($success)) {
            ?>
            <script>
                swal("Success!", "<?php echo $success; ?>", "success")
            </script>
        <?php } ?>
        <?php
        $danger = $this->Session->flash('danger');
        if (!empty($danger)) {
            ?>
            <script>
                swal("Error!", "<?php echo $danger; ?>", "error")
            </script>
        <?php } ?>
        <?php echo $this->fetch('content'); ?>

        <script>
            $('.validation_form').validationEngine({scroll: false});
            $("#flashMessage").click(function () {
                $("#flashMessage").fadeOut(1000);
            });
            setTimeout(function () {
                $('#flashMessage').fadeOut(1000);
            }, 5000);
        </script>
        <style>
            .tx-info{
                color:#000 !important;
            }
        </style>
    </body>
</html>
