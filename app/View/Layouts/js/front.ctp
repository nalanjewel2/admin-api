<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Nalan Jewel</title>
        <meta name="description" content="">
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>front/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>front/css/style.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>front/css/validationEngine.jquery.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>front/css/owl.carousel.min.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>front/css/owl.theme.default.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>front/css/addtohomescreen.css">
        <link href="<?php echo WEBROOT ?>front/css/sweetalert.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>front/css/lightbox.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>front/css/lightgallery.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="<?php echo BASE_URL ?>front/js/bootstrap.min.js"></script>
        <script src="http://maps.google.com/maps/api/js?key=AIzaSyCR-KEWAVCn52mSdeVeTqZjtqbmVJyfSus&amp;language=en"></script>
        <script src="<?php echo BASE_URL ?>front/js/jquery.validationEngine.js"></script>
        <script src="<?php echo BASE_URL ?>front/js/jquery.validationEngine-en.js"></script>
        <script src="<?php echo BASE_URL ?>front/js/addtohomescreen.js"></script>
        <script src="<?php echo WEBROOT ?>front/js/sweetalert.min.js"></script>
        <script src="<?php echo WEBROOT ?>front/js/jquery.matchHeight.js"></script>
        <script src="<?php echo WEBROOT ?>front/js/owl.carousel.min.js"></script>
        <script src="<?php echo WEBROOT ?>front/js/zoomsl.min.js"></script>
        <script src="<?php echo WEBROOT ?>front/js/lightbox.js"></script>
        <script src="<?php echo WEBROOT ?>front/js/bootstrap-datepicker.min.js"></script>
        <script src="<?php echo WEBROOT ?>front/js/lightgallery.min.js"></script>
    </head>
    <body class="tg-home tg-homeone">
        <div id="tg-wrapper" class="tg-wrapper tg-haslayout">
           
            <?php
            $success = $this->Session->flash('success');
            if (!empty($success)) {
                ?>
                <script>
                    swal("Success!", "<?php echo $success; ?>!", "success")
                </script>
            <?php } ?>
            <?php
            $danger = $this->Session->flash('danger');
            if (!empty($danger)) {
                ?>
                <script>
                    swal("Error!", "<?php echo $danger; ?>!", "error")
                </script>
            <?php } ?>
            <?php echo $this->fetch('content'); ?>
           
        </div>
        
    </body>
    <script>
        $('.validation_form').validationEngine();
        $(".delconfirm").on('click', function (e) {
            e.preventDefault();
            var href = $(this).attr('href');
            var title = "Delete Confirmation";
            $('.dlt_href').attr('href', href);
            $('.delete_modal').modal('show');
        });
    </script>
</html>
