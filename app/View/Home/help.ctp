<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
<div class="contents"> 
    <!--Who we are-->
    <div class="who_we_are">
        <div class="bann_waves"> </div>
        <div class="container">
            <div class="row details_cms">
                <div class="col-sm-12 col-xs-12">
                    <div class="m40 clearfix">
                        <h4><?php echo $result['Staticpage']['page_title']; ?></h4>
                        <div class="text-content">
                            <?php echo $result['Staticpage']['page_content']; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .m40 h4 {
        font-size: 18px;
        text-align: center;
        font-family: 'Montserrat', sans-serif;
        font-weight: 700;
        margin: 25px 0;
        text-transform: uppercase;
    }
    .m40 h5{
        font-size: 17px;
        text-align: left;
        font-family: 'Montserrat', sans-serif;
        margin-bottom: 10px;
        color: black;
        font-weight: 600;
        margin: 15px 0;
    }
    .m40 .text-content {
        text-align: left;
        font-family: 'Montserrat', sans-serif;
        font-size: 13px;
        color: gray;
        line-height: 22px;
        font-weight: 500;
        margin-bottom: 25px;
    }
    .about_details img {
        width: 18px;
        margin-right: 10px;
    }
    .about_details h5 {
        font-size: 14px;
        text-align: left;
        font-family: 'Montserrat', sans-serif;
        margin-bottom: 10px;
        color: black;
        font-weight: 700;
        margin: 15px 0;
    }
    .about_details h6 {
        font-size: 15px;
        margin: 15px 0px;
        padding: 0 30px;
        font-family: 'Montserrat', sans-serif;
        font-weight: 500;
        color: #5c5b5b;
        line-height: 25px;
    }
    hr {
        border-top: 5px solid #f2f2f2;
        margin: 10px 0;
    }
    .line {
        border-bottom: 0.5px solid #f2f2f2;

    }
    @media(min-width:767px){
        .about_details h6{
            padding: 0px 0px;
        }
        .line {
            border-bottom:unset;
            margin: unset;
        }
    }
</style>
