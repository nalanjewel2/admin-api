<div class="br-mainpanel">
    <div class="br-pagetitle">    
        <div class="col-md-6">
            <h4>Orders List</h4>
        </div>
    </div>
    <div class="br-pagebody">
        <div class="br-section-wrapper">
            <div class="search">
                <form method="get" action="<?php echo BASE_URL; ?>admin/orders/index" class="form-inline validation-form" style="margin-bottom: 20px;">
                    <ul class="list-unstyled list-inline" style="display: inline-flex;">
                        <li>
                           <div class="input-group">
                                <input type="text" class="form-control validate[required]" value="<?php echo isset($_REQUEST['s']) ? $_REQUEST['s'] : "" ?>"  placeholder="Search order ID" name="s">
                                <span class="input-group-btn">
                                <button class="btn bd bg-white tx-gray-600"  name="search" type="submit"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </li>
                        <li>
                            <?php if (isset($_REQUEST['search'])) { ?><a class="btn btn-danger" href="<?php echo BASE_URL; ?>admin/orders/index">Cancel</a><?php } ?> 
                        </li>
                    </ul>
                </form> 
                 <ul class="list-unstyled list-inline statusfilter" style="display:flex">
                        <li>
                             <form method="get" action="<?php echo BASE_URL; ?>admin/orders/index" class="form-inline validation-form">
                            <label> Date</label>
                           <input type="text" class="form-control picker" placeholder="Select date" name="datetime" onchange="this.form.submit()" value="<?php echo isset($_REQUEST['datetime']) ? date('d-m-y',strtotime($_REQUEST['datetime'])) : "" ?>"/>
                               </form>
                        </li>
                         <li>
                             <form method="get" action="<?php echo BASE_URL; ?>admin/orders/index" class="form-inline validation-form">
                            <label> Status</label>
                            <select class="form-control" name="status">
                            <?php
                             $orderstatuses = ClassRegistry::init('Orderstatus')->find('all');
                             foreach($orderstatuses as $orderstatus){ ?>
                             <option  value="<?php echo $orderstatus['Orderstatus']['order_statusid']?>" <?php ($_REQUEST['status']==$orderstatus['Orderstatus']['order_statusid']) ? "selected" : ""?>><?php echo $orderstatus['Orderstatus']['status']?></option>
                             <?php
                             }
                            ?>
                            </select>                        
                               </form>
                        </li>
                        <li>
                            <?php if ((isset($_REQUEST['datetime'])) || (isset($_REQUEST['datetime']))) { ?><a class="btn btn-danger" href="<?php echo BASE_URL; ?>admin/contacts/index">Cancel</a><?php } ?> 
                        </li>
                </ul>
            </div>
            <?php if (!empty($orders)) { ?>
                <div class="table-wrapper">
                    <table class="table display responsive nowrap table-bordered  table-striped">
                        <thead class="thead-colored thead-dark">
                            <tr>
                                <th>#</th>
                                <th>Order Id</th>
                                <th style="white-space: nowrap;">Total Products</th>
                                <th style="white-space: nowrap;">Customer Name</th>     
                                <th style="white-space: nowrap;">Customer ph number</th> 
                                <th>Category Name</th>
                                <th>Total Amount</th>    
                                <th>Cancelled By</th>
                                <th>Order Status</th>   
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = $this->Paginator->counter('{:start}');
                            foreach ($orders as $order) {
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td style="width: 16%;"><?php echo "LEQ " . $order['Order']['order_id']; ?></td>
                                    <td>
                                        <?php
                                        echo $order['Order']['total_products'];
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $user = ClassRegistry::init('User')->find('first', array('conditions' => array('user_id' => $order['Order']['user_id'])));
                                        $category = ClassRegistry::init('Category')->find('first', array('conditions' => array('category_id' => $order['Order']['category_id'])));
                                        echo $user['User']['full_name'];
                                        ?>
                                    </td>
                                    <td style="white-space: nowrap;"><?php echo!empty($user['User']['mobile']) ? $user['User']['mobile'] : "-"; ?></td>
                                    <td style="white-space: nowrap;"><?php echo $category['Category']['name']; ?></td>
                                    <td style="white-space: nowrap;"><?php echo " Rs. " . number_format((float) $order['Order']['total_amount'], 2, '.', ''); ?></td>
                                    <td><?php echo $order['Order']['cancelled_by']; ?></td>
                                    <td style="white-space: nowrap;"><p class="label-<?php echo $order['Order']['order_status']; ?>"><?php echo $order['Order']['order_status']; ?></p></td>
                                    <td>
                                        <div class="btn-group hidden-xs-down">                                        
                                            <a href="<?php echo BASE_URL; ?>admin/orders/orderdetail/<?php echo $order['Order']['order_id']; ?>" class="green btn btn-outline-info" title="Edit">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a> 

                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="dataTables_info" id="sample-table-2_info" role="status" aria-live="polite">
                                <?php
                                echo $this->Paginator->counter(array(
                                    'format' => __('Page') . ' {:page} ' . __('of') . ' {:pages}, ' . __('showing') . ' {:current} ' . __('records out of') . ' {:count} ' . __('entries')
                                ));
                                ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="dataTables_paginate paging_simple_numbers pull-right" id="sample-table-2_paginate">
                                <ul class="pagination">
                                    <?php
                                    echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('tag' => 'li', 'class' => 'page-link', 'escape' => false), '<a><i class="fa fa-angle-left"></i></a>', array('class' => 'prev disabled page-link', 'tag' => 'li', 'escape' => false));
                                    $numbers = $this->Paginator->numbers();
                                    if (empty($numbers)) {
                                        echo '<li class="active page-link"><a>1</a></li>';
                                    } else {
                                        echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'class' => 'page-link', 'first' => 'First page', 'currentClass' => 'active', 'currentTag' => 'a'));
                                    }
                                    echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('tag' => 'li', 'class' => 'page-link', 'escape' => false), '<a><i class="fa fa-angle-right"></i></a>', array('class' => 'next disabled page-link', 'tag' => 'li', 'escape' => false));
                                    ?>
                                </ul>
                            </div> 
                        </div>
                    </div>
                 </div>
                    <?php
                } else { ?>
                <div class="text-center"><img src="<?php echo BASE_URL;?>img/no-data.png"/></div>
                <?php                 
                }
                ?>
            </div>
        </div>
    </div>
</div>
<style>
    .label-Processing {
        margin: 0px;
        background: blue;
        color: #fff;
        text-align: center;
    }.label-Shipping {
        background:#ff3559;
        color:#fff;
        margin: 0px;
        text-align: center;
    }.label-Confirmed {
        background:#12d202;
        color:#fff;
        margin: 0px;
        text-align: center;
    }.label-Delivered {
        background:yellow;
        color:#000;
        margin: 0px;
        text-align: center;
    }.label-Cancelled {
        background:red;
        color:#fff;
        margin: 0px;
        text-align: center;
    }
</style>
<script>
    $(document).ready(function () {
        $('.table-wrapper').doubleScroll();
    });
</script>