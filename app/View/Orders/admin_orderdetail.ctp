<div class="br-mainpanel">
    <div class="br-pagetitle">     
        <div class="col-md-6">
            <h4><span>   <i class="icon icon ion-ios-book-outline"></i> </span>Orderdetail</h4>
        </div>
    </div>
    <div class="br-pagebody">
        <div class="br-section-wrapper">
            <div class="col-md-12">
                <div class="card card-body account-right orderdetail">
                    <div class="widget">
                        <div class="order-detail-main">
                            <div class="card table-wrapper">
                                <table class="table table-borderless ">
                                    <tr>
                                        <th>Order ID</th>
                                        <th>Order Date</th>
                                        <th>Order Total</th>
                                        <th>Order Status</th> 
                                    </tr>
                                    <tr>
                                        <td>#<?php echo $this->App->lequillaOrderId($order['order_id']); ?></td>
                                        <td><?php echo date('d M Y', strtotime($order['created_date'])); ?></td>
                                        <td><b style="color:#000;"><?php echo "Rs. " . $order['total_amount']; ?></b></td>
                                        <td>
                                            <label class="label-<?php echo $order['order_status']; ?>"><?php echo $order['order_status']; ?></label>
                                        </td>
                                    </tr>
                                </table>   
                            </div>
                            <div class="my-product-det">

                                <table class="table table-borderless">
                                    <?php
                                    if (!empty($products)) {
                                        $i = 1;
                                        foreach ($products as $product) {
                                            ?>
                                            <?php
                                            $procategory = ClassRegistry::init('Product')->find('first', array('conditions' => array('product_id' => $product['product_id'], 'status' => 'Active')));
                                            $categoryname = ClassRegistry::init('Category')->find('first', array('conditions' => array('category_id' => $procategory['Product']['category_id'], 'status' => 'Active')));
                                            ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>                                           
                                                <td style="width:100px">

                                                    <img src="<?php echo $product['thumb']; ?>" onerror="src='<?php echo BASE_URL; ?>img/product-image.png'" class="img-responsive order-product-image" style="">

                                                </td>                                           
                                                <td>
                                                    <h5 style="font-weight: 600;">  <?php echo $product['product']['name']; ?></h5>
                                                </td>
                                                <td></td>
                                                <td class="text-right">
                                                    <h5 style="font-weight: 600;">  <?php echo $product['qty']; ?> x <i class="fa fa-inr"></i> <?php echo $product['price']; ?></h5>
                                                </td>
                                            </tr>
                                            <?php
                                            $i++;
                                        }
                                    } else {
                                        ?>
                                        <tr>
                                            <td>No Product Found</td>
                                        </tr>
                                    <?php }
                                    ?>
                                </table>

                            </div>
                            <hr/>
                            <table class="table table-borderless text-right over-all-price">
                                <tbody>
                                    <tr>
                                        <th>Price (<?php echo $order['total_products']; ?> items )</th>
                                        <td>
                                            <i class="fa fa-inr"></i> <?php echo "Rs. " . $order['total_product_price']; ?>
                                        </td>
                                    </tr>
                                    <?php
                                    if ($order['coupon_id'] != "") {
                                        if (!empty($order['discount_amount'])) {
                                            ?>
                                            <tr>
                                                <th>Coupon Detail (<?php echo $order['coupon_code']; ?> )</th>
                                                <td>
                                                    <?php echo "Discount Amount : Rs. " . $order['discount_amount']; ?>
                                                </td>
                                            </tr>
                                        <?php } else { ?>
                                            <tr>
                                                <th>Coupon Detail ( User Applied Cashback )</th>
                                                <td>
                                                    <?php echo "Cashback Amount : Rs. " . $order['cashback_amount']; ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                    <tr>
                                        <th>Delivery Charge</th>
                                        <td><?php echo "Rs. " . $order['delivery_charge']; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Total Payable</th>
                                        <td><?php echo "Rs. " . $order['total_amount']; ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card card-body account-right">                       
                        <form method="post" action="#">                         
                            <div class="row">                                
                                <div class="col-md-2">
                                    <h6 style="font-size: 16px;color: #000;">Payment Method</h6>
                                </div>
                                <div class="col-md-10">
                                    <p style="color:#000;font-weight: 600;"><?php echo $order['payment_method']; ?></p>
                                </div>
                            </div>
                            <div class="row">                                
                                <div class="col-md-2">
                                    <h6 style="font-size: 16px;color: #000;">Order Status</h6>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">                                
                                        <select class="form-control" name="data[Order][order_status]">                    
                                            <option <?php echo ($order['order_status'] == 'Processing') ? "selected" : "" ?> value="Processing">Processing</option>
                                            <option <?php echo ($order['order_status'] == 'Shipping') ? "selected" : "" ?> value="Shipping">Shipping</option>
                                            <option <?php echo ($order['order_status'] == 'Confirmed') ? "selected" : "" ?> value="Confirmed">Confirmed</option>
                                            <option <?php echo ($order['order_status'] == 'Cancelled') ? "selected" : "" ?> value="Cancelled">Cancelled</option>
                                            <option <?php echo ($order['order_status'] == 'Delivered') ? "selected" : "" ?> value="Delivered">Delivered</option>
                                        </select>
                                    </div>
                                </div>
                            </div>                            
                            <input type="hidden" name="data[Order][order_id]" value="<?php echo $order['order_id']; ?>"/>
                            <button type="submit" class="btn btn-success btn-block mb-4">Submit</button>
                        </form>

                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="delivery-address">
                                    <h5 class="heading-design-h5">
                                        Billing Address
                                    </h5>                                  
                                    <?php
                                    $billingaddress = ClassRegistry::init('Billingaddress')->find('first', array('conditions' => array('billing_address_id' => $order['billing_address_id'])));
                                    ?>                                 
                                    <p><?php echo "<b>Location : </b>" . $billingaddress['Billingaddress']['location']; ?></p>
                                    <p><?php echo "<b>Flat No : </b>" . $billingaddress['Billingaddress']['flat_no']; ?></p>
                                    <p><?php echo "<b>Building : </b>" . $billingaddress['Billingaddress']['building']; ?></p>
                                    <p><?php echo "<b>Landmark : </b>" . $billingaddress['Billingaddress']['landmark']; ?></p>
                                    <p><?php echo "<b>Instruction : </b>" . $billingaddress['Billingaddress']['instruction']; ?></p>
                                    <p><?php echo "<b>Pincode : </b>" . $billingaddress['Billingaddress']['pincode']; ?></p>
                                    <p><?php echo "<b>Contact Person : </b>" . $billingaddress['Billingaddress']['contact_person']; ?></p>
                                    <p><?php echo "<b>Save Us : </b>" . $billingaddress['Billingaddress']['save']; ?></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="delivery-address">
                                    <h5 class="heading-design-h5"> Shipping Address</h5>
                                    <?php
                                    $shippingaddress = ClassRegistry::init('Shippingaddress')->find('first', array('conditions' => array('shippingaddress_id' => $order['shipping_address_id'])));
                                    ?>
                                    <p><?php echo "<b>Location : </b>" . $shippingaddress['Shippingaddress']['location']; ?></p>
                                    <p><?php echo "<b>Flat No : </b>" . $shippingaddress['Shippingaddress']['flat_no']; ?></p>
                                    <p><?php echo "<b>Building : </b>" . $shippingaddress['Shippingaddress']['building']; ?></p>
                                    <p><?php echo "<b>Landmark : </b>" . $shippingaddress['Shippingaddress']['landmark']; ?></p>
                                    <p><?php echo "<b>Instruction : </b>" . $shippingaddress['Shippingaddress']['instruction']; ?></p>
                                    <p><?php echo "<b>Pincode : </b>" . $shippingaddress['Shippingaddress']['pincode']; ?></p>
                                    <p><?php echo "<b>Contact Person : </b>" . $shippingaddress['Shippingaddress']['contact_person']; ?></p>
                                    <p><?php echo "<b>Save Us : </b>" . $shippingaddress['Shippingaddress']['save']; ?></p>
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .user-address p{
        margin-bottom: 0;
    }.user-address {
        margin-top: 20px;
    }.table th, .table td{
        border-top:unset;
    } .label-Processing {
        margin: 0px;
        background: blue;
        color: #fff;
        text-align: center;
        padding:4px 14px;
    }.label-Shipping {
        background:#ff3559;
        color:#fff;
        margin: 0px;
        text-align: center;
        padding:4px 14px;
    }.label-Confirmed {
        background:#12d202;
        color:#fff;
        margin: 0px;
        text-align: center;
        padding:4px 14px;
    }.label-Delivered {
        background:yellow;
        color:#000;
        margin: 0px;
        text-align: center;
        padding:4px 14px;
    }.label-Cancelled {
        background:red;
        color:#fff;
        margin: 0px;
        text-align: center;
        padding:4px 14px;
    }.br-section-wrapper {

        padding: 30px 0px !important;

    }

</style>