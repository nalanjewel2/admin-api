<?php
$adminuser = ClassRegistry::init('User')->find('first', array('conditions' => array('user_id' => $this->Session->read('User.user_id'),'role_id'=>1)));
?>
<div class="br-header">
    <div class="br-header-left">
        <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href="#"><i class="fa fa-bars" aria-hidden="true"></i></a></div>
        <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href="#"><i class="fa fa-bars" aria-hidden="true"></i></a></div>
    </div><!-- br-header-left -->
    <div class="br-header-right">
        <nav class="nav">
            <!-- <div class="dropdown notificationddown">               
                <a href="#" class="nav-link nav-link-profile" data-toggle="dropdown">
                    <span class="logged-name hidden-md-down"> <i class="la la-bell"></i></span>                    
                    <span class="square-10 bg-danger">
                    </span>
                </a>
            </div> -->
            <div class="dropdown">
                <a href="#" class="nav-link nav-link-profile" data-toggle="dropdown">
                    <span class="logged-name hidden-md-down"><?php echo $adminuser['User']['full_name']; ?>  <i class="fas fa-chevron-down"></i></span>
                    <!-- <span class="square-10 bg-success"></span> -->
                </a>
                <div class="dropdown-menu dropdown-menu-header wd-250">
                    <div class="tx-center">
                        <h6 class="logged-fullname"><?php echo $adminuser['User']['full_name']; ?></h6>
                        <p><?php echo $adminuser['User']['email']; ?></p>
                    </div>
                    <hr>
                    <ul class="list-unstyled user-profile-nav">
                        <li><a href="<?php echo BASE_URL; ?>admin/adminusers/profile"><i class="la la-user"></i> <span style="margin-left:2px;">Edit Profile</span></a></li>      
                        <li><a href="<?php echo BASE_URL; ?>admin/adminusers/changepassword"><i class="la la-unlock-alt"></i><span>Change Password</span></a></li>   
                        <li><a href="<?php echo BASE_URL; ?>admin/app/logout"><i class="la la-sign-out"></i><span>LogOut</span></a></li>
                    </ul>
                </div><!-- dropdown-menu -->
            </div><!-- dropdown -->
        </nav>
        <div class="navicon-right">
            <a  href="<?php echo BASE_URL; ?>admin/app/logout" class="pos-relative adminlgout" title="Logout">
             <img src="<?php echo BASE_URL;?>img/logout.png"/>
            </a>
        </div><!-- navicon-right -->
    </div><!-- br-header-right -->
</div><!-- br-header --> 