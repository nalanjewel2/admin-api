<div class="br-sideleft overflow-y-auto">
    <label class="sidebar-label pd-x-10 mg-t-20 op-3"></label>
    <?php
    $adminusers = ClassRegistry::init('User')->find('first', array('conditions' => array('user_id' => $this->Session->read('User.user_id'),'role_id'=>1)));
    ?>
    <ul class="br-sideleft-menu">
        <li class="br-menu-item">
            <?php
            $cls = '';
            if ($this->params['controller'] == 'dashboard') {
                $cls = 'active';
            }
            ?>
            <a href="<?php echo BASE_URL; ?>admin/dashboard/index" class="br-menu-link <?php echo $cls; ?>">
                <i class="menu-item-icon icon la la-dashboard tx-24"></i>
                <span class="menu-item-label">Dashboard</span>
            </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
            <?php
            $cls = '';
            if (($this->params['action'] == 'admin_profile') || ($this->params['action'] == 'admin_changepassword')) {
                $cls = 'active';
            }
            ?>
            <a href="#" class="br-menu-link with-sub <?php echo $cls; ?>">
                <i class="far fa-user-circle"></i>
                <span class="menu-item-label">My Profile</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
                <li class="sub-item">
                    <?php
                    $cls = '';
                    if ($this->params['action'] == 'admin_profile') {
                        $cls = 'active';
                    }
                    ?>
                    <a href="<?php echo BASE_URL; ?>admin/adminusers/profile" class="sub-link <?php echo $cls; ?>">                 
                        <span class="menu-item-label">My Profile</span>  
                    </a>
                </li>
                <li class="sub-item">
                    <?php
                    $cls = '';
                    if ($this->params['action'] == 'admin_changepassword') {
                        $cls = 'active';
                    }
                    ?>
                    <a href="<?php echo BASE_URL; ?>admin/adminusers/changepassword" class="sub-link <?php echo $cls; ?>">                      
                        <span class="menu-item-label">Change Password</span>  
                    </a>
                </li>  
            </ul>
        </li>
        <li class="br-menu-item">
            <?php
            $cls = '';
            if ($this->params['controller'] == 'Sitesettings') {
                $cls = 'active';
            }
            ?>
            <a href="<?php echo BASE_URL; ?>admin/Sitesettings/index" class="br-menu-link <?php echo $cls; ?>">
                <i class="menu-item-icon icon la la-cog tx-24"></i>
                <span class="menu-item-label">Site Settings</span>
            </a><!-- br-menu-link -->

        </li>
        <li class="br-menu-item">
            <?php
            $cls = '';
            if (($this->params['controller'] == 'users') || ($this->params['controller'] == 'salesmans')) {
                $cls = 'active';
            }
            ?>
            <a href="#" class="br-menu-link with-sub <?php echo $cls; ?>">
               <i class="fas fa-users"></i>
                <span class="menu-item-label">User List</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
                <li class="sub-item">
                    <?php
                    $cls = '';
                    if ($this->params['controller'] == 'users') {
                        $cls = 'active';
                    }
                    ?>
                    <a href="<?php echo BASE_URL; ?>admin/users/index" class="sub-link <?php echo $cls; ?>">                 
                        <span class="menu-item-label">Customer Mgnt</span>  
                    </a>
                </li>
                 <li class="sub-item">
                    <?php
                    $cls = '';
                    if ($this->params['controller'] == 'salesmans') {
                        $cls = 'active';
                    }
                    ?>
                    <a href="<?php echo BASE_URL; ?>admin/salesmans/index" class="sub-link <?php echo $cls; ?>">                 
                        <span class="menu-item-label">Sales Person Mgnt</span>  
                    </a>
                </li>
            </ul>
        </li>       
        <li class="br-menu-item">
            <?php
            $cls = '';
            if (($this->params['controller'] == 'attributes') || ($this->params['controller'] == 'products') || ($this->params['controller'] == 'materials') || ($this->params['controller'] == 'projects') || ($this->params['controller'] == 'collections')) {
                $cls = 'active';
            }
            ?>
            <a href="#" class="br-menu-link with-sub <?php echo $cls; ?>">
                <i class="fas fa-box-open"></i>
                <span class="menu-item-label">Products Mgnt</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
                <li class="sub-item">
                    <?php
                    $cls = '';
                    if ($this->params['controller'] == 'materials') {
                        $cls = 'active';
                    }
                    ?>
                    <a href="<?php echo BASE_URL; ?>admin/materials/index" class="sub-link <?php echo $cls; ?>">                 
                        <span class="menu-item-label">Material Mgnt</span>  
                    </a>
                </li>
                 <li class="sub-item">
                    <?php
                    $cls = '';
                    if ($this->params['controller'] == 'projects') {
                        $cls = 'active';
                    }
                    ?>
                    <a href="<?php echo BASE_URL; ?>admin/projects/index" class="sub-link <?php echo $cls; ?>">                 
                        <span class="menu-item-label">Project Mgnt</span>  
                    </a>
                </li>
                 <li class="sub-item">
                    <?php
                    $cls = '';
                    if ($this->params['controller'] == 'collections') {
                        $cls = 'active';
                    }
                    ?>
                    <a href="<?php echo BASE_URL; ?>admin/collections/index" class="sub-link <?php echo $cls; ?>">                 
                        <span class="menu-item-label">Collection Mgnt</span>  
                    </a>
                </li>
                <li class="sub-item">
                    <?php
                    $cls = '';
                    if ($this->params['controller'] == 'attributes') {
                        $cls = 'active';
                    }
                    ?>
                    <a href="<?php echo BASE_URL; ?>admin/attributes/index" class="sub-link <?php echo $cls; ?>">                 
                        <span class="menu-item-label">Attribute Mgnt</span>  
                    </a>
                </li>
                <li class="sub-item">
                    <?php
                    $cls = '';
                    if ($this->params['controller'] == 'products') {
                        $cls = 'active';
                    }
                    ?>
                    <a href="<?php echo BASE_URL; ?>admin/products/index" class="sub-link <?php echo $cls; ?>">                 
                        <span class="menu-item-label">Product Mgnt</span>  
                    </a>
                </li>
            </ul>
        </li>
         <li class="br-menu-item">
            <?php
            $cls = '';
            if ($this->params['controller'] == 'sliders') {
                $cls = 'active';
            }
            ?>
            <a href="<?php echo BASE_URL; ?>admin/sliders/index" class="br-menu-link <?php echo $cls; ?>">
                <i class="fas fa-sliders-h"></i>
                <span class="menu-item-label">Sliders Mgnt</span>
            </a><!-- br-menu-link -->

        </li>
         <li class="br-menu-item">
            <?php
            $cls = '';
            if ($this->params['controller'] == 'contacts') {
                $cls = 'active';
            }
            ?>
            <a href="<?php echo BASE_URL; ?>admin/contacts/index" class="br-menu-link <?php echo $cls; ?>">
                <i class="far fa-comment-alt"></i>
                <span class="menu-item-label">Enquiry List</span>
            </a><!-- br-menu-link -->
        </li>
       
        <li class="br-menu-item">
            <?php
            $cls = '';
            if ($this->params['controller'] == 'staticpages') {
                $cls = 'active';
            }
            ?>
            <a href="<?php echo BASE_URL; ?>admin/staticpages/index" class="br-menu-link <?php echo $cls; ?>">
                <i class="far fa-file-alt"></i>
                <span class="menu-item-label">CMS Pages</span>
            </a><!-- br-menu-link -->

        </li>
        <li class="br-menu-item menuhide">
            <a href="<?php echo BASE_URL; ?>adminusers/database_mysql_dump" class="br-menu-link">
                <i class="menu-item-icon icon la la-comments-o tx-24"></i>
                <span class="menu-item-label">DB BACKUP</span>
            </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->

    </ul><!-- br-sideleft-menu --> 
</div><!-- br-sideleft -->
<style>
    i.la {
        font-size: 20px;
    }
</style>