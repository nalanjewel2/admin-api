<?php $settings = ClassRegistry::init('Sitesetting')->find('first'); ?>
<div class="row no-gutters flex-row-reverse ht-100v adminlogin">
    <div class="col-md-6 bg-gray-200 d-flex align-items-center justify-content-center">
        <div class="login-wrapper wd-250 wd-xl-350 mg-y-30">
            <form method="post" action="#" class="validation_form"> 
                <h4 class="tx-inverse tx-center">Sign In</h4>
                <p class="tx-center mg-b-60">Welcome back! Please sign in.</p>               
                <!-- form-group -->
                <div class="form-group">
                    <input type="text" name="data[User][username]" class="form-control validate[required]" placeholder="Enter your Email">
                </div>
                <!-- form-group -->
                <div class="form-group">
                    <input type="password" name="data[User][password]" class="form-control validate[required]" placeholder="Enter your password">
                    <a href="<?php echo BASE_URL; ?>admin/adminusers/forgot" class="tx-info tx-12 d-block mg-t-10">Forgot password?</a>
                </div>
                <!-- form-group -->
                <button type="submit" class="btn btn-info btn-block">Sign In</button>
            </form>
        </div><!-- login-wrapper -->
    </div><!-- col -->
    <div class="col-md-6 bg-br-primary d-flex align-items-center justify-content-center">
        <div class="wd-250 wd-xl-450 mg-y-30">
        </div><!-- wd-500 -->
    </div>
</div><!-- row -->
