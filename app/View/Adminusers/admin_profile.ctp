<div class="br-mainpanel">
    <div class="br-pagetitle">        
        <div>
            <h4>Update Profile</h4>
        </div>
    </div><!-- d-flex -->
    <div class="br-pagebody">
        <div class="br-section-wrapper"> 
            <form class="form-horizontal validation_form" role="form" id="myForm" name="profile" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-sm-3 control-label"> Name</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control  validate[required,custom[onlyLetterSp]]" name="data[User][full_name]" value="<?php echo $this->request->data['User']['full_name']; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"> Email <span class="tx-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" id="email"   class="form-control validate[required,custom[email]]" name="data[User][email]" value="<?php echo $this->request->data['User']['email']; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"> Mobile <span class="tx-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" id="email"   class="form-control validate[required,custom[onlyNumberSp],minSize[10],maxSize[10]]" name="data[User][mobile]" value="<?php echo $this->request->data['User']['mobile']; ?>"/>
                    </div>
                </div>             
              
                <div class="clearfix form-actions row submit-cancel">
                    <div class="">
                        <button class="btn btn-green" type="submit"> <i class="ace-icon fa fa-check bigger-110"></i> Submit </button>
                    </div>
                    <div class="">
                        <a href="<?php echo BASE_URL; ?>admin/dashboard/index" class="btn btn-cancel"/>Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

