<div class="br-mainpanel">
    <div class="br-pagetitle"> 
        <div>
            <h4>Change Password</h4>
        </div>
    </div><!-- d-flex -->
    <div class="br-pagebody">
        <div class="br-section-wrapper"> 

            <form class="form-horizontal validation_form" role="form" id="myForm" name="profile" method="post" enctype="multipart/form-data">            
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Current password <span class="tx-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="password" id="oldpassword" placeholder="Current Password"  class="form-control validate[required]" name="data[User][oldpassword]" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> New Password <span class="tx-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="password" id="password" placeholder="New Password"  class="form-control validate[required,minSize[6]]" name="data[User][password]" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Confirm Password <span class="tx-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="password" id="conpass" placeholder="Confirm Password"  class="form-control validate[required,minSize[6],equals[password]]" name="data[User][cpasswords]"/>
                    </div>
                </div>
                <div class="clearfix form-actions row submit-cancel">
                    <div class="">
                        <button class="btn btn-green" type="submit"> <i class="ace-icon fa fa-check bigger-110"></i> Submit </button>
                    </div>
                    <div class="">
                        <a href="<?php echo BASE_URL; ?>admin/dashboard/index" class="btn btn-cancel"/>Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
