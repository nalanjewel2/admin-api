<?php $settings = ClassRegistry::init('Sitesetting')->find('first'); ?>
<div class="row no-gutters flex-row-reverse ht-100v adminlogin">
    <div class="col-md-6 bg-gray-200 d-flex align-items-center justify-content-center">
        <div class="login-wrapper wd-250 wd-xl-350 mg-y-30">
            <div class="">
            <form action="" method="post" class="validation_form">
                <div class="panel panel-body login-form">
                    <div class="text-center">                       
                        <h5>Forgot Password? </h5> 
                        <p><small class="display-block">Enter your credentials below</small></p>
                    </div>                   
                    <div class="form-group has-feedback has-feedback-left">
                        <input type="email" name="data[User][email]" id="email" class="form-control validate[required,custom[email]]" placeholder="Email" />
                        <div class="form-control-feedback">
                            <i class="icon-envelop text-muted"></i>
                        </div>
                    </div>
                    <div class="clearfix form-group">
                        <button type="submit" class="width-35 btn-block btn btn-sm btn-danger"> <i class="la la-lightbulb-o"></i>  <span class="bigger-110">Send Me!</span> </button>
                    </div>
                    <div class="text-center">
                        <input type="button"  onclick="goBack()" value="Back to Login" style="border: unset;background: transparent;color: blue;margin-top: 14px;cursor: pointer;"/>
                    </div>
                </div>
            </form>
        </div>
        </div><!-- login-wrapper -->
    </div><!-- col -->
    <div class="col-md-6 bg-br-primary d-flex align-items-center justify-content-center">
        <div class="wd-250 wd-xl-450 mg-y-30">
        </div><!-- wd-500 -->
    </div>
</div><!-- row -->

<script>
    function goBack() {
        window.history.back();
    }
</script>
