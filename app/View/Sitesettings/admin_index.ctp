<div class="br-mainpanel">
    <div class="br-pagetitle">
        <div>
            <h4>Site Settings</h4>
        </div>
    </div>
    <div class="br-pagebody">
        <div class="br-section-wrapper">
            <div class="form-layout form-layout-4 sitesettings">
                <form method="post" action="" class="validation_form" enctype="multipart/form-data">
                    <div class="row">
                        <label class="col-sm-3 form-control-label">Logo<span class="tx-danger">*</span></label>
                        <div class="col-sm-4 mg-t-10 mg-sm-t-0">
                            <label class="custom-file">
                                <input type="file" class="custom-file-input file-styled validate[custom[image]]" name="data[Sitesetting][logo]">
                                <span class="custom-file-control custom-file-control-primary"></span>
                            </label>
                            <p><small>Recommended Size: 100*100</small></p>
                        </div>
                        <div class="col-sm-3">
                            <img src="<?php echo BASE_URL; ?>img/<?php echo $result['Sitesetting']['logo']; ?>" class="img-circle img-responsive" style="width: 100%;height: 49px;"/>

                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-3 form-control-label">Fav<span class="tx-danger">*</span></label>
                        <div class="col-sm-4 mg-t-10 mg-sm-t-0">
                            <label class="custom-file">
                                <input type="file" class="custom-file-input file-styled validate[custom[image]]" name="data[Sitesetting][fav_icon]">
                                <span class="custom-file-control custom-file-control-primary"></span>
                            </label>
                            <p><small>Recommended Size: 16*16 </small></p>   
                        </div>
                        <div class="col-sm-2">
                            <img src="<?php echo BASE_URL; ?>img/<?php echo $result['Sitesetting']['fav_icon']; ?>" class="img-circle img-responsive" style="object-fit: cover; height: 50px; width: 50px;"/>

                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-3 form-control-label">Gold Rate<span class="tx-danger">*</span></label>
                        <div class="col-sm-4 mg-t-10 mg-sm-t-0">
                            <div class="input-group">
                                  <span class="input-group-addon">SGD</span>
                                  <input type="text" class="form-control validate[required]" placeholder="Gold rate" name="data[Sitesetting][gold_rate]" value="<?php echo (isset($result['Sitesetting']['gold_rate'])) ? $result['Sitesetting']['gold_rate'] : ""; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-3 form-control-label">GST <span class="tx-danger">*</span></label>
                        <div class="col-sm-4 mg-t-10 mg-sm-t-0">
                            <div class="input-group">
                                  <span class="input-group-addon" style="padding-left: 19px;">%</span>
                                  <input type="text" class="form-control validate[required]" placeholder="GST" name="data[Sitesetting][gst]" value="<?php echo (isset($result['Sitesetting']['gst'])) ? $result['Sitesetting']['gst'] : ""; ?>">
                            </div>
                        </div>
                    </div>
                     <div class="row">
                        <label class="col-sm-3 form-control-label">Delivery Cost <span class="tx-danger">*</span></label>
                        <div class="col-sm-4 mg-t-10 mg-sm-t-0">
                            <div class="input-group">
                                  <span class="input-group-addon">SGD</span>
                                  <input type="text" class="form-control validate[required]" placeholder="Delivery Cost" name="data[Sitesetting][delivery_fee]" value="<?php echo (isset($result['Sitesetting']['delivery_fee'])) ? $result['Sitesetting']['delivery_fee'] : ""; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <div class="">
                            <button class="btn btn-green" type="submit"> <i class="ace-icon fa fa-check bigger-110"></i> Submit </button>
                        </div>
                    </div>
                </form>              
            </div>
        </div>
    </div>
</div>         

