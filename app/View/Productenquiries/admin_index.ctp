
<div class="br-mainpanel">
    <div class="br-pagetitle">
        <div class="col-md-6">
            <h4>Request to view list</h4>
        </div>
    </div><!-- d-flex -->
    <div class="br-pagebody">
        <div class="br-section-wrapper">
            <div class="search">
                <form method="get" action="<?php echo BASE_URL; ?>admin/productenquiries/index" class="form-inline validation-form" style="float: left;">
                    <ul class="list-unstyled list-inline" style="display: inline-flex;">   
                         <li>
                            <div class="input-group">
                                <input type="text" class="form-control validate[required]" value="<?php echo isset($_REQUEST['s']) ? $_REQUEST['s'] : "" ?>"  placeholder="Search name, email, mobile" name="s">
                                <span class="input-group-btn">
                                <button class="btn bd bg-white tx-gray-600"  name="search" type="submit"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </li>
                        <li>
                            <?php if (isset($_REQUEST['search'])) { ?><a class="btn btn-danger" href="<?php echo BASE_URL; ?>admin/productenquiries/index">Cancel</a><?php } ?> 
                        </li>
                    </ul>
                </form> 
                 <ul class="list-unstyled list-inline statusfilter" style="display:flex">
                        <li>
                             <form method="get" action="<?php echo BASE_URL; ?>admin/productenquiries/index" class="form-inline validation-form">
                            <label> Date : </label>
                           <input type="text" class="form-control picker" placeholder="Select date" name="datetime" onchange="this.form.submit()" value="<?php echo isset($_REQUEST['datetime']) ? date('d-m-y',strtotime($_REQUEST['datetime'])) : "" ?>"/>
                               </form>
                        </li>
                        <li>
                            <?php if (isset($_REQUEST['datetime'])) { ?><a class="btn btn-danger" href="<?php echo BASE_URL; ?>admin/productenquiries/index">Cancel</a><?php } ?> 
                        </li>
                </ul>
            </div>
            <?php if(!empty($results)) { ?>
            <div class="table-wrapper">
                <table class="table display responsive nowrap table-bordered  table-striped">
                    <thead class="thead-colored thead-dark"> 
                        <tr>
                            <th>#</th>
                            <th>Posted date</th>
                            <th>Product name</th>
                            <th>Customer name</th>
                            <th>Customer email</th>
                            <th>Customer mobile</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = $this->Paginator->counter('{:start}');
                        foreach ($results as $enquiry) {
                        $product = ClassRegistry::init('Product')->find('first', array('conditions' => array('product_id' => $enquiry['Productenquiry']['product_id'])));
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo date('d-m-y',strtotime($enquiry['Productenquiry']['created_date']));?></td>
                                <td><?php echo $product['Product']['product_name'];?></td>
                                <td><?php echo $enquiry['Productenquiry']['name']; ?></td>                            
                                <td><?php echo $enquiry['Productenquiry']['email']; ?></td>  
                                <td><?php echo $enquiry['Productenquiry']['mobile']; ?></td> 
                                <td>
                                     <div class="actions-btns">
                                        <a href="<?php echo BASE_URL; ?>admin/productenquiries/view/<?php echo $enquiry['Productenquiry']['productenqury_id']; ?>" class="green btn btn-outline-view" title="View">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                        <a href="<?php echo BASE_URL; ?>admin/productenquiries/delete/<?php echo $enquiry['Productenquiry']['productenqury_id']; ?>" class="green deleteconfirm btn btn-outline-delete" title="Delete">   
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-md-6">
                        <div class="dataTables_info" id="sample-table-2_info" role="status" aria-live="polite">
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page') . ' {:page} ' . __('of') . ' {:pages}, ' . __('showing') . ' {:current} ' . __('records out of') . ' {:count} ' . __('entries')
                            ));
                            ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="dataTables_paginate paging_simple_numbers pull-right" id="sample-table-2_paginate">
                            <ul class="pagination">
                                <?php
                                echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('tag' => 'li', 'class' => 'page-link', 'escape' => false), '<a><i class="fa fa-angle-left"></i></a>', array('class' => 'prev disabled page-link', 'tag' => 'li', 'escape' => false));
                                $numbers = $this->Paginator->numbers();
                                if (empty($numbers)) {
                                    echo '<li class="active page-link"><a>1</a></li>';
                                } else {
                                    echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'class' => 'page-link', 'first' => 'First page', 'currentClass' => 'active', 'currentTag' => 'a'));
                                }
                                echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('tag' => 'li', 'class' => 'page-link', 'escape' => false), '<a><i class="fa fa-angle-right"></i></a>', array('class' => 'next disabled page-link', 'tag' => 'li', 'escape' => false));
                                ?>
                            </ul>
                        </div> 
                    </div>
                </div>
            </div>
            <?php }else { ?>
            <div class="text-center">
                <img src="<?php echo BASE_URL;?>img/no-data.png"/>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<script>
 $(document).ready(function () {
        $('.table-wrapper').doubleScroll();
    });
    $(document).ready(function(){      //Add this line (and it's closing line)
    var currentDate = new Date();
    $(".picker").datepicker({
         dateFormat: 'd-m-y',
         changeMonth: true,
         changeYear: true,
         inline: true,
    });
});
</script> 