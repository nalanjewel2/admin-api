<div class="br-mainpanel">
    <div class="br-pagetitle">
        <div class="col-md-6">
            <h4>View request detail</h4>
        </div>
        <div class="col-md-6">
            <div class="btn-group float-right">
                <a href="<?php echo BASE_URL; ?>admin/productenquiries/index" class="btn addbtn"><i class="fas fa-arrow-left"></i> Back to list</a>
            </div>
        </div>
    </div><!-- d-flex -->
    <div class="br-pagebody">
        <div class="br-section-wrapper request-details">
            <form action="" class="form-horizontal" method="post" enctype="multipart/form-data">
                <fieldset class="content-group">                               
                    <h5>Customer detail</h5>
                    <div class="form-group row">
                        <label class="control-label col-md-2 col-lg-2">Name   </label>
                         <div class="col-md-1 colon-style">
                        :
                        </div>
                        <div class="col-lg-8 col-md-8">
                            <?php echo $result['Productenquiry']['name'];?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-2 col-lg-2">Email   </label>
                         <div class="col-md-1 colon-style">
                        :
                        </div>
                        <div class="col-lg-8 col-md-8">
                            <?php echo $result['Productenquiry']['email'];?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-2 col-lg-2">Mobile   </label>
                         <div class="col-md-1 colon-style">
                        :
                        </div>
                        <div class="col-lg-8 col-md-8">
                            <?php echo $result['Productenquiry']['mobile'];?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-2 col-lg-2">Request reason   </label>
                         <div class="col-md-1 colon-style">
                        :
                        </div>
                        <div class="col-lg-8 col-md-8">
                            <?php echo $result['Productenquiry']['reason'];?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-2 col-lg-2">Poested date   </label>
                         <div class="col-md-1 colon-style">
                        :
                        </div>
                        <div class="col-lg-8 col-md-8">
                            <?php echo date('d-m-y',strtotime($result['Productenquiry']['created_date']));?>
                        </div>
                    </div>
                    <?php 
                $product = ClassRegistry::init('Product')->find('first', array('conditions' => array('product_id' => $result['Productenquiry']['product_id'])));
                ?>
                  <h5>Product detail</h5>
                  <div class="form-group row">
                        <label class="control-label col-md-2 col-lg-2">Product Id   </label>
                        <div class="col-md-1 colon-style">
                        :
                        </div>
                        <div class="col-lg-8 col-md-8">
                            <?php echo "NALAN". $product['Product']['product_id']; ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-2 col-lg-2">Product name   </label>
                         <div class="col-md-1 colon-style">
                        :
                        </div>
                        <div class="col-lg-8 col-md-8">
                            <?php echo $product['Product']['product_name']; ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-2 col-lg-2">Price   </label>
                         <div class="col-md-1 colon-style">
                        :
                        </div>
                        <div class="col-lg-8 col-md-8">
                            <?php echo $product['Product']['total_price'] . " SGD"; ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-2 col-lg-2">Description   </label>
                         <div class="col-md-1 colon-style">
                        :
                        </div>
                        <div class="col-lg-8 col-md-8">
                            <?php echo $product['Product']['description']; ?>
                        </div>
                    </div>    
                    <?php 
                $user = ClassRegistry::init('User')->find('first', array('conditions' => array('user_id' => $result['Productenquiry']['user_id'])));
                if(!empty($user['User']['sales_person_id'])) {
                ?>
                <h5>Sales person detail</h5>
                    <div class="form-group row">
                        <label class="control-label col-md-2 col-lg-2">Name   </label>
                         <div class="col-md-1 colon-style">
                        :
                        </div>
                        <div class="col-lg-8 col-md-8">
                            <?php echo $user['User']['full_name'];?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-2 col-lg-2">Email   </label>
                         <div class="col-md-1 colon-style">
                        :
                        </div>
                        <div class="col-lg-8 col-md-8">
                            <?php echo $user['User']['email'];?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-2 col-lg-2">Mobile   </label>
                         <div class="col-md-1 colon-style">
                        :
                        </div>
                        <div class="col-lg-8 col-md-8">
                            <?php echo $user['User']['mobile'];?>
                        </div>
                    </div>
                    <?php
                }
                ?>                    
                </fieldset>
            </form>
        </div>
    </div>
</div>