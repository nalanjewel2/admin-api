<?php

App::uses('AppController', 'Controller');

/**
 * Contacts Controller
 *
 * @property Contact $Contact
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ProductenquiriesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session', 'Image');
    public $uses = array('Product', 'Productenquiry','Emailcontent','Adminuser');
    public $layout = 'admin';

    /**
     * AdminIndex
     *
     * @return void
     */
    public function admin_index() {
        $this->checkadmin();
        $conditions=array('status'=>'Active');
        if (isset($_REQUEST['search'])) {
            $conditions['OR'] = array('Productenquiry.name LIKE' => '%' . trim($_REQUEST['s']) . '%', 'Productenquiry.email LIKE' => '%' . trim($_REQUEST['s']) . '%', 'Productenquiry.mobile LIKE' => '%' . trim($_REQUEST['s']) . '%'); 
        }
        if(!empty($_REQUEST['datetime'])){
            $conditions=array('DATE(created_date)'=>date('Y-m-d',strtotime($_REQUEST['datetime'])),'status'=>'Active');
        }
       
        $this->paginate = array('conditions'=>$conditions,'order' => 'productenqury_id DESC', 'limit' => '30');
        $this->set('results', $this->Paginator->paginate('Productenquiry'));
        }        
    

    public function admin_view($id = null) {
        $this->checkadmin();
        if (!$this->Productenquiry->exists($id)) {
            throw new NotFoundException(__('Productenquiry Not Found'));
        }
        $result = $this->Productenquiry->find('first', array('conditions' => array('productenqury_id' => $id)));
        $this->set('result', $result);
    }
    
    public function admin_delete($id = null) {
        $this->autorender = false;
        if (!$this->Productenquiry->exists($id)) {
            throw new NotFoundException(__('Productenquiry Not Found'));
        }
        $this->request->data['Productenquiry']['productenqury_id'] = $id;
        $this->request->data['Productenquiry']['status'] = 'Trash';
        $this->Productenquiry->save($this->request->data['Productenquiry']);
        $this->Session->setFlash('Product enquiry  deleted successfully!', '', array(''), 'success');
        $this->redirect(array('action' => 'index'));
    }

}
