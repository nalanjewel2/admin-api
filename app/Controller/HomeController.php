<?php

App::uses('AppController', 'Controller');

/**
 * Adminusers Controller
 *
 * @property Adminuser $Adminuser
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class HomeController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('Adminuser', 'Sitesetting', 'User', 'Emailcontent', 'Staticpage', 'Slider', 'Favourite', 'Category', 'Location', 'Faq', 'Staticpage', 'Job', 'Jobapplication', 'Adfield', 'Customfield', 'Spam');
    public $layout = 'front';

    public function about() {
        $this->layout = 'front';
        $result = $this->Staticpage->find('first', array('conditions' => array('page_id' => 1)));
        if (!empty($result)) {
            $this->set('result', $result);
        } else {
            throw new NotFoundException(__('Page Not Found'));
        }
    }

    public function policy() {
        $this->layout = 'front';
        $result = $this->Staticpage->find('first', array('conditions' => array('page_id' => 2)));
        if (!empty($result)) {
            $this->set('result', $result);
        } else {
            throw new NotFoundException(__('Page Not Found'));
        }
    }

    public function help() {
        $this->layout = 'front';
        $result = $this->Staticpage->find('first', array('conditions' => array('page_id' => 3)));
        if (!empty($result)) {
            $this->set('result', $result);
        } else {
            throw new NotFoundException(__('Page Not Found'));
        }
    }

}
