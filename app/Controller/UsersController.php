<?php

App::uses('AppController', 'Controller');

/**
 * Categorys Controller
 *
 * @property Category $Category
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class UsersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('Emailcontent', 'Sitesetting', 'User');
    public $layout = 'admin';

    /**
     * AdminIndex
     *
     * @return void
     */
    public function admin_index() {
        $this->User->recursive = 0;
        $this->checkadmin();
        $conditions = array('role_id'=>2,'status !='=>"Trash");
        if (isset($_REQUEST['search'])) {
            $conditions['OR'] = array('full_name LIKE' => '%' . trim($_REQUEST['s']) . '%', 'email LIKE' => '%' . trim($_REQUEST['s']) . '%', 'mobile LIKE' => '%' . trim($_REQUEST['s']) . '%',);
        }
        if(isset($_REQUEST['status'])){
            if($_REQUEST['status'] !="All"){
            $conditions = array('status'=>$_REQUEST['status'],'role_id'=>2);
            }else{
                 $conditions = array('status !='=>"Trash",'role_id'=>2);
            }
        }
        $this->paginate = array('conditions' => $conditions, 'order' => 'user_id DESC', 'limit' => '20');
        $this->set('users', $this->Paginator->paginate('User'));
    }

    public function admin_view($id = NULL) {
        $this->checkadmin();
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid User'));
        }
        $options = array('conditions' => array('user_id' => $id));
        $this->set('user', $this->User->find('first', $options));
    }
    
    public function admin_add() {
        $this->checkadmin();
        if ($this->request->is('post')) {
            $checkemail = $this->User->find('first', array('conditions' => array('email' => $this->request->data['User']['email'], 'status !=' => 'Trash')));
            if (empty($checkemail)) {               
                    $password = $this->str_rand(6, 'alphanum');
                    $this->request->data['User']['full_name']=trim(preg_replace('/\s+/', ' ', $this->request->data['User']['full_name']));
                    $this->request->data['User']['passwrd_text'] = $password;
                    $this->request->data['User']['password'] = md5($password);
                    $token = $this->str_rand(15, 'alphanum');
                    $this->request->data['User']['access_token'] = $token;
                    $this->request->data['User']['role_id'] = 2;
                    $this->request->data['User']['created_date'] = date('Y-m-d H:i:s');
                    $this->request->data['User']['modified_date'] = date('Y-m-d H:i:s');
                    if ($this->User->save($this->request->data)) {
                        $lid = $this->User->getLastInsertID();
                        $email = $this->request->data['User']['email'];
                        $name = trim(preg_replace('/\s+/', ' ', $this->request->data['User']['full_name']));
                        $emaillist1 = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '1')));
                        $message1 = str_replace(array('{name}','{email}','{password}'), array($name,$email, $password), $emaillist1['Emailcontent']['emailcontent']);
                        $this->mailsend($emaillist1['Emailcontent']['fromname'], $emaillist1['Emailcontent']['fromemail'], $email, $emaillist1['Emailcontent']['subject'], $message1);
                        $this->Session->setFlash('New customer created successfully!', '', array(''), 'success');
                        return $this->redirect(array('action' => 'index'));
                    } else {
                        $this->Session->setFlash('The customer could not be saved. Please, try again.', '', array(''), 'danger');
                    }              
            } else {
                $this->Session->setFlash('The email already exist. Please, try again.', '', array(''), 'danger');
            }
        }
    }
    

    public function admin_edit($id = null) {
        $this->checkadmin();
        $checkuser = ClassRegistry::init('User')->find('first', array('conditions' => array('user_id' => $id)));
        if ($this->request->is('post')) {           
            $checkemail = ClassRegistry::init('User')->find('first', array('conditions' => array('email' => $this->request->data['User']['email'], 'user_id !=' => $id)));
            if (empty($checkmobile)) {
                $this->request->data['User']['user_id'] = $id;
                $this->request->data['User']['full_name'] = trim(preg_replace('/\s+/', ' ', $this->request->data['User']['full_name']));;
                $this->request->data['User']['email'] = $this->request->data['User']['email'];
                $this->request->data['User']['mobile'] = $this->request->data['User']['mobile'];
                $this->request->data['User']['passwrd_text'] = $this->request->data['User']['password'];
                $this->request->data['User']['password'] = md5($this->request->data['User']['password']);
                
                if (!empty($this->request->data['User']['profile']['name'])) {
                    $banner = rand(0, 99999) . $this->request->data['User']['profile']['name'];
                    move_uploaded_file($this->request->data['User']['profile']['tmp_name'], 'files/users/' . $banner);
                    $this->request->data['User']['profile'] = $banner;
                } else {
                    $this->request->data['User']['profile'] = $checkuser['User']['profile'];
                }
                $this->User->save($this->request->data['User']);

                if(!empty($this->request->data['User']['email'])){
                $email = $this->request->data['User']['email'];
                $name = trim(preg_replace('/\s+/', ' ', $this->request->data['User']['full_name']));;
                $password=!empty($this->request->data['User']['passwrd_text']) ? $this->request->data['User']['passwrd_text'] : "";
                $mobile = !empty($this->request->data['User']['mobile']) ? $this->request->data['User']['mobile'] : "";
                $emaillist1 = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '8')));
                $message1 = str_replace(array('{name}','{email}','{password}','{mobile}'), array($name,$email, $password,$mobile), $emaillist1['Emailcontent']['emailcontent']);
                $this->mailsend($emaillist1['Emailcontent']['fromname'], $emaillist1['Emailcontent']['fromemail'], $email, $emaillist1['Emailcontent']['subject'], $message1);
                }
                $this->Session->setFlash('Customer profile updated successfully!', '', array(''), 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Email/Mobile Already exist!', '', array(''), 'danger');
                $this->redirect($this->referer());
            }
        } 
        $options = array('conditions' => array('user_id' => $id));
        $this->set('user', $this->User->find('first', $options));
    }

    public function admin_updatepassword($id = null) {
        $this->layout = 'admin';
        $this->checkadmin();
        $User = $this->User->find('first', array('conditions' => array('user_id' => $this->request->data['User']['user_id'])));
        $this->request->data['User']['user_id'] = $this->request->data['User']['user_id'];
        $this->request->data['User']['passwrd_text'] = $this->request->data['User']['password'];
        $this->request->data['User']['password'] = md5($this->request->data['User']['password']);
        $this->User->save($this->request->data['User']);
        if(!empty($User['User']['email'])){
                $email = $User['User']['email'];
                $name = $User['User']['full_name'];
                $password=$User['User']['passwrd_text'];
                $mobile = !empty($User['User']['mobile']) ? $User['User']['mobile'] : "";
                $emaillist1 = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '8')));
                $message1 = str_replace(array('{name}','{email}','{password}','{mobile}'), array($name,$email, $password,$mobile), $emaillist1['Emailcontent']['emailcontent']);
                $this->mailsend($emaillist1['Emailcontent']['fromname'], $emaillist1['Emailcontent']['fromemail'], $email, $emaillist1['Emailcontent']['subject'], $message1);
                }
        $this->Session->setFlash('Password updated successfully', '', array(''), 'success');
        $this->redirect(array('action' => 'index'));
    }
    public function admin_delete($id = null) {
        $this->autorender = false;
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('User Not Found'));
        }
        $this->request->data['User']['user_id'] = $id;
        $this->request->data['User']['status'] = 'Trash';
        $this->User->save($this->request->data['User']);
        $this->Session->setFlash('Customer deleted successfully!', '', array(''), 'success');
        $this->redirect(array('action' => 'index'));
    }
    
    public function admin_updatestatus($id = null) {
        $this->layout = 'admin';
        $this->checkadmin();
        $this->request->data['User']['user_id'] = $this->request->data['User']['user_id'];
        $this->request->data['User']['status'] = $this->request->data['User']['status'];
        $this->User->save($this->request->data['User']);
        $this->Session->setFlash('Customer status updated successfully', '', array(''), 'success');
        $this->redirect($this->referer());
    }
    

    function database_mysql_dump($tables = '*') {

        $return = '';

        $modelName = $this->User;
        $dataSource = $this->User->getDataSource();
        $databaseName = $dataSource->getSchemaName();

        // Do a short header
        $return .= '-- Database: `' . $databaseName . '`' . "\n";
        $return .= '-- Generation time: ' . date('D jS M Y H:i:s') . "\n\n\n";


        if ($tables == '*') {
            $tables = array();
            $result = $this->User->query('SHOW TABLES');
            foreach ($result as $resultKey => $resultValue) {
                $tables[] = current($resultValue['TABLE_NAMES']);
            }
        } else {
            $tables = is_array($tables) ? $tables : explode(',', $tables);
        }

        // Run through all the tables
        foreach ($tables as $table) {
            $tableData = $this->User->query('SELECT * FROM ' . $table);

            $return .= 'DROP TABLE IF EXISTS ' . $table . ';';
            $createTableResult = $this->User->query('SHOW CREATE TABLE ' . $table);
            $createTableEntry = current(current($createTableResult));
            $return .= "\n\n" . $createTableEntry['Create Table'] . ";\n\n";

            // Output the table data
            foreach ($tableData as $tableDataIndex => $tableDataDetails) {

                $return .= 'INSERT INTO ' . $table . ' VALUES(';

                foreach ($tableDataDetails[$table] as $dataKey => $dataValue) {

                    if (is_null($dataValue)) {
                        $escapedDataValue = 'NULL';
                    } else {
                        // Convert the encoding
                        $escapedDataValue = mb_convert_encoding($dataValue, "UTF-8", "ISO-8859-1");

                        // Escape any apostrophes using the datasource of the model.
                        $escapedDataValue = $this->User->getDataSource()->value($escapedDataValue);
                    }

                    $tableDataDetails[$table][$dataKey] = $escapedDataValue;
                }
                $return .= implode(',', $tableDataDetails[$table]);

                $return .= ");\n";
            }

            $return .= "\n\n\n";
        }

        // Set the default file name
        $fileName = $databaseName . '-backup-' . date('Y-m-d_H-i-s') . '.sql';


        // Serve the file as a download
        $this->layout = "";
        $this->autoRender = false;
        $this->response->type('Content-Type: text/x-sql');
        // move_uploaded_file($fileName, 'files/companyimages/' . $banner);
        $this->response->download($fileName);
        $this->response->body($return);
    }


}
