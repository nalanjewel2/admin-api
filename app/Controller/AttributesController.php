<?php
App::uses('AppController', 'Controller');
/**
 * Attributes Controller
 *
 * @property Attribute $Attribute
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class AttributesController extends AppController {
    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('Attribute', 'Product', 'Attributevalue','User','Cart','Productimage','Sitesetting');
    /**
     * AdminIndex
     *
     * @return void
     */
    public function admin_index() {
        $this->layout = 'admin';
        $this->Attribute->recursive = 0;
        $conditions = array('status !=' => 'Trash');
        if (isset($_REQUEST['s'])) {
            $s = $_REQUEST['s'];
            $conditions['OR'] = array('attribute_name LIKE' => '%' . $s . '%');
        }
        $this->paginate = array('conditions' => $conditions, 'order' => 'attribute_id DESC', 'limit' => '20');
        $this->set('attributes', $this->Paginator->paginate('Attribute'));
    }
    public function admin_add() {
        $this->layout = 'admin';
        $this->checkadmin();
        try {
            if ($this->request->is('post')) {
            	 $checkattribute = ClassRegistry::init('Attribute')->find('first', array('conditions' => array('attribute_name' => trim(preg_replace('/\s+/', ' ', $this->request->data['Attribute']['attribute_name'])),'status'=>"Active")));
            	 	if(empty($checkattribute)){

            	 		$this->request->data['Attribute']['created_date'] = date('Y-m-d H:i:s');
                $this->request->data['Attribute']['modified_date'] = date('Y-m-d H:i:s');
                $slug=preg_replace("/\s+/", "", $this->request->data['Attribute']['attribute_name']);
                $slug_name=strtolower($slug);
                $this->request->data['Attribute']['slug'] = $slug_name;           
                $this->request->data['Attribute']['attribute_name']=trim(preg_replace('/\s+/', ' ', $this->request->data['Attribute']['attribute_name']));
                $values = explode(',', $this->request->data['Attributevalue']['value']);                
                if ($this->Attribute->save($this->request->data['Attribute'])) {
                    $id = $this->Attribute->getLastInsertID();
                    foreach ($values as $value) {
                        $this->request->data['Attributevalue']['value'] = trim($value);
                        $this->request->data['Attributevalue']['attribute_id'] = $id;
                        $this->Attributevalue->saveAll($this->request->data['Attributevalue']);
                    }                   
            	 	}
            	 	 $this->Session->setFlash('Attribute has been saved successfully!', '', array(''), 'success');
                       $this->redirect(array('action' => 'index'));

                
                } else {
                   $this->Session->setFlash('Attribute name already exist!', '', array(''), 'danger');
                }             
            }
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }
    public function admin_edit($id = null) {
        $this->layout = 'admin';
        $this->checkadmin();
        if (!$this->Attribute->exists($id)) {
            throw new NotFoundException(__('Attribute Not Found'));
        }
        if ($this->request->is('post')) {
        	 $checkattribute = ClassRegistry::init('Attribute')->find('first', array('conditions' => array('attribute_name' => trim(preg_replace('/\s+/', ' ', $this->request->data['Attribute']['attribute_name'])),'attribute_id !='=>$id,'status !='=>'Trash')));

        	 	if(empty($checkattribute)){

        	 		$this->request->data['Attribute']['attribute_id'] = $id;
            $this->request->data['Attribute']['modified_date'] = date('Y-m-d H:i:s');
            $this->request->data['Attribute']['attribute_name']=trim(preg_replace('/\s+/', ' ', $this->request->data['Attribute']['attribute_name']));
            if($this->request->data['Attribute']['type']=="Select"){
                $values = explode(',', $this->request->data['Attributevalue']['value']);   
            }
  
                     
            if ($this->Attribute->save($this->request->data['Attribute'])) {

                $res=$this->Attributevalue->deleteAll(array('Attributevalue.attribute_id' => $id), false);
                if(!empty($values)){
                    foreach ($values as $value) {
                    $this->request->data['Attributevalue']['value'] = trim($value);
                    $this->request->data['Attributevalue']['attribute_id'] = $id;
                    $this->Attributevalue->saveAll($this->request->data['Attributevalue']);
                }
            }             
                


        	 	} 
        	 	$this->Session->setFlash('Updated successfully', '', array(''), 'success');
                  $this->redirect(array('action' => 'index'));           
            } else {
                $this->Session->setFlash('Attribute name already exist!', '', array(''), 'danger');
                
            }
        }
        $result = $this->Attribute->find('first', array('conditions' => array('attribute_id' => $id)));
        $this->set('result', $result);
    }
    public function admin_delete($id = null) {
        $this->autorender = false;
        $this->checkadmin();
        if (!$this->Attribute->exists($id)) {
            throw new NotFoundException(__('Attribute Not Found'));
        }
        $this->request->data['Attribute']['attribute_id'] = $id;
        $this->request->data['Attribute']['status'] = "Trash";
        $this->request->data['Attribute']['modified_date'] = date('Y-m-d H:i:s');
        if ($this->Attribute->save($this->request->data['Attribute'])) {
            $this->Session->setFlash('Attribute deleted successfully!', '', array(''), 'success');
        } else {
            $this->Session->setFlash('Attribute could not be deleted! Please try again later!', '', array(''), 'danger');
        }
        $this->redirect(array('action' => 'index'));
    }
    
    public function user_addtocart() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'user-id');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        $token = $this->request->header('user-id');
        try {
            $user = $this->checkusertoken($token);
            $product = $this->Product->find('first', array('conditions' => array('product_id' => $params['product_id'])));
            $price = $product['Product']['regular_price'];
            $check = $this->Cart->find('first', array('conditions' => array('product_id' => $params['product_id'], 'user_id' => $user['User']['user_id'])));
            $qty=1;
            $this->request->data['Cart']['user_id'] = $user['User']['user_id'];
            $this->request->data['Cart']['product_id'] = $params['product_id'];
            $this->request->data['Cart']['qty'] = (!empty($check)) ? $check['Cart']['qty'] + $qty : $qty;
            $this->request->data['Cart']['price']=$price;
            $this->request->data['Cart']['total_amount'] = $price * $this->request->data['Cart']['qty'];
            if (!empty($check)) {
                $this->request->data['Cart']['cart_id'] = $check['Cart']['cart_id'];
            }
            $this->Cart->save($this->request->data['Cart']);
            $result = array("message" => "Added Successfully!", "code" => 200);
            return json_encode($result);
            exit;
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }
    
    public function cartlist($id = null) {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'user-id');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        $token = $this->request->header('user-id');
        try {
            $user = $this->checkusertoken($token);
            $vendors = $this->Cart->find('all', array('conditions' => array('user_id' => $user['User']['user_id'])));
            $qty = $totalamount = $total_products = 0;
            $i = 0;
            $carts = $this->Cart->find('all', array('conditions' => array('user_id' => $user['User']['user_id'])));
            $site_setting = $this->Sitesetting->find('first', array('conditions' => array('id' => 1)));
            if($site_setting['Sitesetting']['delivery_fee'] !=0){
                $delivery_charge = $site_setting['Sitesetting']['delivery_fee'];
            }else{
                $delivery_charge = "free";
            }
            $gst=$site_setting['Sitesetting']['gst'];
            $gst_amnt=0;
            $product=array();
            $discount_amnt=0;
            foreach ($carts as $cart) {
                $pods = $this->Product->find('first', array('conditions' => array('product_id' => $cart['Cart']['product_id'])));
                $pods_image = $this->Productimage->find('first', array('conditions' => array('product_id' => $cart['Cart']['product_id'])));
                $total=$cart["Cart"]['qty']*$pods['Product']['regular_price'];
                if($pods['Product']['discount_type']=="Percentage"){
                    $discount=$pods['Product']['discount'] . "%";
                }else{
                    $discount=$pods['Product']['discount'] . "SGD";
                }
                $product=array(
                "product_id"=>$pods['Product']['product_id'],
                "product_name"=>$pods['Product']['product_name'],
                "thumb"=>BASE_URL."files/products/".$pods_image['Productimage']['image'],
                "price"=>(float)$pods['Product']['regular_price'],
                'cart_id' => $cart["Cart"]['cart_id'],
                'qty' => $cart["Cart"]['qty'],
                "total"=>(float)$total,
                "discount"=>$discount
                );
                $gst_price=($pods['Product']['net_price'] * $gst / 100);
                $gst_amnt=$gst_amnt + $gst_price;
                $discountamnt=$cart["Cart"]['qty']*$pods['Product']['discount_value'];
                $discount_amnt=$discount_amnt + $discountamnt;
                $data['products'][] = $product;
                $totalamount += $total;
                $total_products++;
            }
            $data['total_products'] = $total_products;
            $data['total'] = $totalamount;
            $data['delivery_charge'] = $delivery_charge;
            $data['discount_amount'] = $discount_amnt;
            $data['gst_amount']=$gst_amnt;
            $data['payable'] = $data['total'] + $delivery_charge - $discount_amnt + $gst_amnt;
            return json_encode(array("data" => $data, "code" => 200));
            exit;
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }
    
}
