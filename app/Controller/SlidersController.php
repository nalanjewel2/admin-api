<?php

App::uses('AppController', 'Controller');

/**
 * Shoppingsliders Controller
 *
 * @property Shoppingslider $Shoppingslider
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SlidersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('User', 'Sitesetting', 'Slider');
    public $layout = 'admin';

    /**
     * AdminIndex
     *
     * @return void
     */
    public function admin_index() {
        $this->layout = 'admin';
        $this->Slider->recursive = 0;
        $this->checkadmin();
        $conditions = array('status !=' => 'Trash');
        $this->paginate = array('conditions' => $conditions, 'order' => 'slider_id DESC', 'limit' => '20');
        $this->set('banners', $this->Paginator->paginate('Slider'));
    }

    public function admin_add() {
        $this->layout = 'admin';
        $this->checkadmin();
        try {
            if ($this->request->is('post')) {
                if (!empty($this->request->data['Slider']['slider_name']['name'])) {
                    $banner = rand(0, 99999) . $this->request->data['Slider']['slider_name']['name'];
                    move_uploaded_file($this->request->data['Slider']['slider_name']['tmp_name'], 'files/sliders/' . $banner);
                    $this->request->data['Slider']['slider_name'] = $banner;
                }
                $this->request->data['Slider']['created_date'] = date('Y-m-d H:i:s');
                $this->request->data['Slider']['modified_date'] = date('Y-m-d H:i:s');
                $this->Slider->save($this->request->data['Slider']);
                $this->Session->setFlash('Slider Created successfully!', '', array(''), 'success');
                $this->redirect(array('action' => 'index'));
            }
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function admin_delete($id = null) {
        $this->autorender = false;
        $this->checkadmin();
        if (!$this->Slider->exists($id)) {
            throw new NotFoundException(__('Slider Not Found'));
        }
        $this->request->data['Slider']['slider_id'] = $id;
        $this->request->data['Slider']['status'] = 'Trash';
        $this->request->data['Slider']['modified_date'] = date('Y-m-d H:i:s');
        if ($this->Slider->save($this->request->data['Slider'])) {
            $this->Session->setFlash('Slider deleted successfully!', '', array(''), 'success');
        } else {
            $this->Session->setFlash('Slider could not be deleted! Please try again later!', '', array(''), 'danger');
        }
        $this->redirect(array('action' => 'index'));
    }

}
