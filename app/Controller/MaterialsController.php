<?php

App::uses('AppController', 'Controller');

/**
 * Materials Controller
 *
 * @property Category $Category
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class MaterialsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('Emailcontent', 'Sitesetting', 'Material');
    public $layout = 'admin';

    /**
     * AdminIndex
     *
     * @return void
     */
    public function admin_index() {
        $this->Material->recursive = 0;
        $this->checkadmin();
         $conditions = array('status !='=>"Trash"); 
        if (isset($_REQUEST['s'])) {
            $s = $_REQUEST['s'];
            $conditions = array('material_name LIKE' => '%' . trim($s) . '%');
        }
        $this->paginate = array('conditions' => $conditions, 'order' => 'material_id DESC', 'limit' => '20');
        $this->set('materials', $this->Paginator->paginate('Material'));
    }
  
    
    public function admin_add() {
        $this->checkadmin();
        if ($this->request->is('post')) {            
            $checkemail = $this->Material->find('first', array('conditions' => array('material_name' => trim(preg_replace('/\s+/', ' ', $this->request->data['Material']['material_name'])), 'status !=' => 'Trash')));
            if (empty($checkemail)) {
                    $this->request->data['Material']['material_name'] = trim(preg_replace('/\s+/', ' ', $this->request->data['Material']['material_name']));
                    $this->request->data['Material']['created_date'] = date('Y-m-d H:i:s');
                    $this->request->data['Material']['modified_date'] = date('Y-m-d H:i:s');
                    $banner = rand(0, 99999) . $this->request->data['Material']['material_image']['name'];
                    move_uploaded_file($this->request->data['Material']['material_image']['tmp_name'], 'files/material/' . $banner);
                    $this->request->data['Material']['material_image'] = $banner;
                    $this->Material->save($this->request->data['Material']);
                    $this->Session->setFlash('Material Saved successfully!.', '', array(''), 'success');
                    $this->redirect(array('action' => 'index'));
                    
            } else {
                    $this->Session->setFlash('The Material name already exist. Please, try again.', '', array(''), 'danger');
            }
        }
    }
    

    public function admin_edit($id = null) {
        $this->checkadmin();
        $checkuser = ClassRegistry::init('Material')->find('first', array('conditions' => array('material_id' => $id)));
        if ($this->request->is('post')) {
                  $checkmaterial = ClassRegistry::init('Material')->find('first', array('conditions' => array('material_name' => trim(preg_replace('/\s+/', ' ', $this->request->data['Material']['material_name'])),'material_id !='=>$id,'status'=>'Active')));                 
               if(empty($checkmaterial)) {
                $this->request->data['Material']['material_id'] = $id;
                $this->request->data['Material']['material_name']=trim(preg_replace('/\s+/', ' ', $this->request->data['Material']['material_name']));
                if (!empty($this->request->data['Material']['material_image']['name'])) {
                    $banner = rand(0, 99999) . $this->request->data['Material']['material_image']['name'];
                    move_uploaded_file($this->request->data['Material']['material_image']['tmp_name'], 'files/material/' . $banner);
                    $this->request->data['Material']['material_image'] = $banner;
                } else {
                    $this->request->data['Material']['material_image'] = $checkuser['Material']['material_image'];
                }
                $this->Material->save($this->request->data['Material']);
                $this->Session->setFlash('Material Updated successfully!', '', array(''), 'success');
                $this->redirect(array('action' => 'index'));
               }else{
                  $this->Session->setFlash('Material name already exist!', '', array(''), 'danger'); 
               }
           
        }
        $options = array('conditions' => array('material_id' => $id));
        $this->set('material', $this->Material->find('first', $options));
    }

    public function admin_delete($id = null) {
        $this->autorender = false; 
        if (!$this->Material->exists($id)) {
            throw new NotFoundException(__('Material Not Found'));
        }
        $this->request->data['Material']['material_id'] = $id;
        $this->request->data['Material']['status'] = 'Trash';
        $this->Material->save($this->request->data['Material']);
        $this->Session->setFlash('Material deleted successfully!', '', array(''), 'success');
        $this->redirect(array('action' => 'index'));
    }

}
