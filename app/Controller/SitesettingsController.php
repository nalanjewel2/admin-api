<?php

App::uses('AppController', 'Controller');

/**
 * Adminusers Controller
 *
 * @property Adminuser $Adminuser
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SitesettingsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('User', 'Sitesetting');
    public $layout = 'admin';
    /**
     * AdminIndex
     *
     * @return void
     */
    public function admin_index() {
        $setting = $this->Sitesetting->find('first');
        if ($this->request->is('post')) {
            $this->request->data['Sitesetting']['id'] = 1;
            if ($this->request->data['Sitesetting']['logo']['name'] != '') {
                $logo = rand(0, 9999) . $this->request->data['Sitesetting']['logo']['name'];
                move_uploaded_file($this->request->data['Sitesetting']['logo']['tmp_name'], 'img/' . $logo);
            } else {
                $logo = $setting['Sitesetting']['logo'];
            }
            if ($this->request->data['Sitesetting']['fav_icon']['name'] != '') {
                $fav_icon = rand(0, 9999) . $this->request->data['Sitesetting']['fav_icon']['name'];
                move_uploaded_file($this->request->data['Sitesetting']['fav_icon']['tmp_name'], 'img/' . $fav_icon);
            } else {
                $fav_icon = $setting['Sitesetting']['fav_icon'];
            }
            $this->request->data['Sitesetting']['logo'] = $logo;
            $this->request->data['Sitesetting']['fav_icon'] = $fav_icon;
            $this->request->data['Sitesetting']['modified_date'] = date('Y-m-d H:i:s');
            $this->Sitesetting->save($this->request->data);
            $this->Session->setFlash('Site settings updated successfully!', '', array(''), 'success');
            $this->redirect(array("controller" => "Sitesettings", "action" => "index"));
        }
        $result = $this->Sitesetting->find('first', array('conditions' => array('id' => '1')));
        $this->set('result', $result);
    }

}
