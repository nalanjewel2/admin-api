<?php

App::uses('AppController', 'Controller');

/**
 * Contacts Controller
 *
 * @property Contact $Contact
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ContactsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session', 'Image');
    public $uses = array('Product', 'Contact','Emailcontent','Adminuser');
    public $layout = 'admin';

    /**
     * AdminIndex
     *
     * @return void
     */
    public function admin_index() {
        $this->checkadmin();
        $conditions=array('status'=>'Active','name !='=>"");
        if (isset($_REQUEST['search'])) {
            $conditions['OR'] = array('Contact.name LIKE' => '%' . trim($_REQUEST['s']) . '%', 'Contact.email LIKE' => '%' . trim($_REQUEST['s']) . '%', 'Contact.mobile LIKE' => '%' . trim($_REQUEST['s']) . '%');       
        }
        if(!empty($_REQUEST['datetime'])){
            $conditions=array('DATE(created_date)'=>date('Y-m-d',strtotime($_REQUEST['datetime'])),'status'=>'Active','name !='=>"");
        }
       
        $this->paginate = array('conditions'=>$conditions,'order' => 'contact_id DESC', 'limit' => '30');
        $this->set('results', $this->Paginator->paginate('Contact'));
        }        
    

    public function admin_view($id = null) {
        $this->checkadmin();
        if (!$this->Contact->exists($id)) {
            throw new NotFoundException(__('Contact Not Found'));
        }
        $result = $this->Contact->find('first', array('conditions' => array('contact_id' => $id)));
        $this->set('result', $result);
    }
    
    public function admin_delete($id = null) {
        $this->autorender = false;
        if (!$this->Contact->exists($id)) {
            throw new NotFoundException(__('Contact Not Found'));
        }
        $this->request->data['Contact']['contact_id'] = $id;
        $this->request->data['Contact']['status'] = 'Trash';
        $this->Contact->save($this->request->data['Contact']);
        $this->Session->setFlash('Enquiry  deleted successfully!', '', array(''), 'success');
        $this->redirect(array('action' => 'index'));
    }

}
