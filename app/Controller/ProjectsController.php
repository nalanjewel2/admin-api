<?php

App::uses('AppController', 'Controller');

/**
 * Materials Controller
 *
 * @property Category $Category
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ProjectsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('Emailcontent', 'Sitesetting', 'Project');
    public $layout = 'admin';

    /**
     * AdminIndex
     *
     * @return void
     */
    public function admin_index() {
        $this->Project->recursive = 0;
        $this->checkadmin();
         $conditions = array('status !='=>"Trash"); 
        if (isset($_REQUEST['s'])) {
            $s = $_REQUEST['s'];
            $conditions = array('project_name LIKE' => '%' . trim($s) . '%');
        }
        $this->paginate = array('conditions' => $conditions, 'order' => 'project_id DESC', 'limit' => '20');
        $this->set('projectes', $this->Paginator->paginate('Project'));
    }
  
    
    public function admin_add() {
        $this->checkadmin();
        if ($this->request->is('post')) {
            $checkemail = $this->Project->find('first', array('conditions' => array('project_name' => trim(preg_replace('/\s+/', ' ', $this->request->data['Project']['project_name'])), 'status !=' => 'Trash')));
            if (empty($checkemail)) {
                    $this->request->data['Project']['project_name'] = trim(preg_replace('/\s+/', ' ', $this->request->data['Project']['project_name']));
                    $this->request->data['Project']['created_date'] = date('Y-m-d H:i:s');
                    $this->request->data['Project']['modified_date'] = date('Y-m-d H:i:s');
                    $this->Project->save($this->request->data['Project']);
                    $this->Session->setFlash('Project Saved successfully!.', '', array(''), 'success');
                    $this->redirect(array('action' => 'index'));
            } else {
                    $this->Session->setFlash('The Project already exist. Please, try again.', '', array(''), 'danger');
            }
        }
    }
    

    public function admin_edit($id = null) {
        $this->checkadmin();
        $checkuser = ClassRegistry::init('Project')->find('first', array('conditions' => array('project_id' => $id)));
        if ($this->request->is('post')) {
               $checkmaterial = ClassRegistry::init('Project')->find('first', array('conditions' => array('project_name' => trim(preg_replace('/\s+/', ' ', $this->request->data['Project']['project_name'])),'project_id !='=>$id,'status'=>'Active')));
               if(empty($checkmaterial)) {
                $this->request->data['Project']['project_id'] = $id;
                $this->request->data['Project']['project_name']=trim(preg_replace('/\s+/', ' ', $this->request->data['Project']['project_name']));
                $this->Project->save($this->request->data['Project']);
                $this->Session->setFlash('Project Updated successfully!', '', array(''), 'success');
                $this->redirect(array('action' => 'index'));
               }else{
                  $this->Session->setFlash('Project name already exist!', '', array(''), 'danger'); 
               }
        }
        $options = array('conditions' => array('project_id' => $id));
        $this->set('project', $this->Project->find('first', $options));
    }

    public function admin_delete($id = null) {
        $this->autorender = false;
        if (!$this->Project->exists($id)) {
            throw new NotFoundException(__('Project Not Found'));
        }
        $this->request->data['Project']['project_id'] = $id;
        $this->request->data['Project']['status'] = 'Trash';
        $this->Project->save($this->request->data['Project']);
        $this->Session->setFlash('Project deleted successfully!', '', array(''), 'success');
        $this->redirect(array('action' => 'index'));
    }

}
