<?php

App::uses('AppController', 'Controller');

/**
 * Collection Controller
 *
 * @property Category $Category
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CollectionsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('User', 'Sitesetting', 'Collection');
    public $layout = 'admin';

    /**
     * AdminIndex
     *
     * @return void
     */
    public function admin_index() {
        $this->layout = 'admin';
        $this->Collection->recursive = 0;
        $this->checkadmin();
        $conditions = array('status !=' => 'Trash');
        if (isset($_REQUEST['s'])) {
            $s = trim($_REQUEST['s']);
            $conditions = array('collection_name LIKE' => '%' . $s . '%','status !=' => 'Trash');
        }
        $this->paginate = array('conditions' => $conditions, 'order' => 'collection_id DESC', 'limit' => '20');
        $this->set('collections', $this->Paginator->paginate('Collection'));
    }

    public function admin_add() {
        $this->layout = 'admin';
        $this->checkadmin();
        try {
            if ($this->request->is('post')) {
                $checkcategory = $this->Collection->find('first', array('conditions' => array('collection_name' => trim(preg_replace('/\s+/', ' ', $this->request->data['Collection']['collection_name'])), 'status' => 'Active')));
                if (empty($checkcategory)) {
                    $this->request->data['Collection']['collection_name'] = trim(preg_replace('/\s+/', ' ', $this->request->data['Collection']['collection_name']));
                    $this->request->data['Collection']['created_date'] = date('Y-m-d H:i:s');
                    $this->request->data['Collection']['modified_date'] = date('Y-m-d H:i:s');
                    $this->request->data['Collection']['parent_collection'] = !empty($this->request->data['Collection']['parent_collection']) ? $this->request->data['Collection']['parent_collection'] : 0;
                
                    $this->Collection->save($this->request->data['Collection']);
                    $this->Session->setFlash('Collection Created successfully!', '', array(''), 'success');
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash('Collection name already exists!', '', array(''), 'danger');
                }
            }
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function admin_edit($id = null) {
        $this->checkadmin();
        if (!$this->Collection->exists($id)) {
            throw new NotFoundException(__('Invalid Collection'));
        }
        $category = $this->Collection->find('first', array('conditions' => array('collection_id' => $id)));
        if ($this->request->is(array('post', 'put'))) {
            $check = $this->Collection->find('first', array('conditions' => array('collection_name' => trim(preg_replace('/\s+/', ' ', $this->request->data['Collection']['collection_name'])), 'collection_id !=' => $id, 'status !=' => 'Trash')));
            if (empty($check)) {
                $this->request->data['Collection']['collection_name'] = trim(preg_replace('/\s+/', ' ', $this->request->data['Collection']['collection_name']));
                $this->request->data['Collection']['collection_id'] = $id;
                $this->Collection->save($this->request->data['Collection']);
                $this->Session->setFlash('Collection updated successfully ', '', array(''), 'success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Collection name  already exists', '', array(''), 'danger');
                return $this->redirect(array('action' => 'index'));
            }
        }
        $this->request->data['Collection'] = $category['Collection'];
    }

    public function admin_delete($id = null) {
        $this->autorender = false;
        $this->checkadmin();
        if (!$this->Collection->exists($id)) {
            throw new NotFoundException(__('Collection Not Found'));
        }
        $this->request->data['Collection']['collection_id'] = $id;
        $this->request->data['Collection']['status'] = 'Trash';
        if ($this->Collection->save($this->request->data['Collection'])) {
            $this->Session->setFlash('Collection deleted successfully!', '', array(''), 'success');
        } else {
            $this->Session->setFlash('Collection could not be deleted! Please try again later!', '', array(''), 'danger');
        }
        $this->redirect(array('action' => 'index'));
    }

}
