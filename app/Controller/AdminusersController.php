<?php

App::uses('AppController', 'Controller');

/**
 * Adminusers Controller
 *
 * @property Adminuser $Adminuser
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class AdminusersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('User');
    public $layout = 'admin';
    
    /**
     * Login method
     *
     * @return void
     */
    public function admin_login() {
        $this->layout = 'admin_login';
        $this->admin_logincheck();
        if (!empty($this->request->data)) {
            if (!empty($this->request->data['User']['email'])) {
                $check = $this->User->find('first', array('conditions' => array('email' => $this->request->data['User']['email'], 'status' => 'Active','role_id'=>1)));
                if (!empty($check)) {
                    $pass = $this->str_rand();
                    $password = md5($pass);
                    $emailcontent = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '2')));
                    $link = BASE_URL . "admin/";
                    $name = $check['User']['adminname'];
                    $username = $check['User']['email'];
                    $message = str_replace(array('{name}', '{username}', '{password}', '{link}'), array($name, $username, $pass, $link), $emailcontent['Emailcontent']['emailcontent']);
                    $this->mailsend($emailcontent['Emailcontent']['fromname'], $emailcontent['Emailcontent']['fromemail'], $check['User']['email'], $emailcontent['Emailcontent']['subject'], $message);
                    $check['User']['password'] = $password;
                    $check['User']['modified_date'] = date('Y-m-d H:i:s');
                    $this->User->save($check);
                    $this->Session->setFlash('Your password details sent to your email address.', '', array(''), 'success');
                    $this->redirect(array('controller' => 'adminusers', 'action' => 'login'));
                } else
                    $this->Session->setFlash('Invalid email address.', '', array(''), 'danger');
                $this->set('result', 'forgot');
            }
            else {
                $check = $this->User->find('first', array('conditions' => array('email' => $this->request->data['User']['username'],'role_id'=>1)));
                if (!empty($check)) {
                    if ($check['User']['password'] == md5($this->request->data['User']['password'])) {
                        if ($check['User']['status'] == 'Active') {
                            $this->Session->write($check);
                            $this->redirect(array('controller' => 'dashboard', 'action' => 'admin_index'));
                        } else
                            $this->Session->setFlash('Your account is inactive, please contact admin.', '', array(''), 'danger');
                    } else
                        $this->Session->setFlash('Password mismatch.', '', array(''), 'danger');
                } else
                    $this->Session->setFlash('Email Id not found!', '', array(''), 'danger');
                $this->set('result', '');
            }
        }
    }

    public function admin_forgot() {
        $this->layout = 'admin_login';
        $this->admin_logincheck();
        if (!empty($this->request->data)) {
            $check = $this->User->find('first', array('conditions' => array('email' => $this->request->data['User']['email'], 'status' => 'Active','role_id'=>1)));
            if (!empty($check)) {
                $pass = $this->str_rand();
                $password = md5($pass);
                $emailcontent = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '3')));
                $link = BASE_URL . "admin/login";
                $name = $check['User']['full_name'];
                $useremail = $check['User']['email'];
                $message = str_replace(array('{name}', '{password}', '{email}'), array($name, $pass, $useremail), $emailcontent['Emailcontent']['emailcontent']);
                $this->mailsend($emailcontent['Emailcontent']['fromname'], $emailcontent['Emailcontent']['fromemail'], $check['User']['email'], $emailcontent['Emailcontent']['subject'], $message);
                $check['User']['password'] = $password;
                $check['User']['modified_date'] = date('Y-m-d H:i:s');
                $check['User']['password_text'] = $pass;
                $this->User->save($check);
                $this->Session->setFlash('Your password details sent to your email address.', '', array(''), 'success');
                $this->redirect(array('controller' => 'adminusers', 'action' => 'login'));
            } else {
                $this->Session->setFlash('Invalid email address.', '', array(''), 'danger');
            }
        }
        $this->set('result', 'forgot');
    }

    /**
     * change admin user status
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_profile() {
        $this->layout = 'admin';
        $this->checkadmin();
        $id = $this->Session->read('User.user_id');
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid User'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $checkadmin = $this->User->find('first', array('conditions' => array('user_id' => $id, 'status !=' => 'Trash','role_id'=>1)));

            $checkemail = $this->User->find('first', array('conditions' => array('email' => $this->request->data['User']['email'], 'user_id !=' => $id, 'status !=' => 'Trash','role_id'=>1)));
            if (empty($checkemail)) {
                if (!empty($this->request->data['User']['profile']['name'])) {
                    $banner = rand(0, 99999) . $this->request->data['User']['profile']['name'];
                    move_uploaded_file($this->request->data['User']['profile']['tmp_name'], 'files/profiles/' . $banner);
                    $this->request->data['User']['profile'] = $banner;
                } else {
                    $this->request->data['User']['profile'] = $checkadmin['User']['profile'];
                }
                $this->request->data['User']['full_name']=trim(preg_replace('/\s+/', ' ', $this->request->data['User']['full_name']));
                $this->request->data['User']['user_id'] = $id;
                $this->User->save($this->request->data);
                $this->Session->setFlash('Your profile detail has been updated successfully.', '', array(''), 'success');
                $this->redirect(array('action' => 'profile'));
            } else {
                $this->Session->setFlash('Email already exists.', '', array(''), 'danger');
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }
    }

    /**
     * change password
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_changepassword() {
        $this->checkadmin();
        $id = $this->Session->read('User.user_id');
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid User'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $checkpass = $this->User->find('first', array('conditions' => array('password' => md5($this->request->data['User']['oldpassword']), 'user_id' => $id,'role_id'=>1)));
            if (!empty($checkpass)) {
                if ($this->request->data['User']['password'] == $this->request->data['User']['cpasswords']) {
                    $this->request->data['User']['password'] = md5($this->request->data['User']['password']);
                    $this->request->data['User']['user_id'] = $id;
                    $this->User->save($this->request->data);
                    $this->Session->setFlash('The admin password has been updated successfully.', '', array(''), 'success');
                    $this->redirect(array('action' => 'changepassword'));
                } else {
                    $this->Session->setFlash('New password and confirm password did not match.', '', array(''), 'danger');
                }
            } else {
                $this->Session->setFlash('Old Password is incorrect.', '', array(''), 'danger');
            }
        }
    }
    
}
