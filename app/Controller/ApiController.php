<?php

App::uses('AppController', 'Controller');

/**
 * Sociallinks Controller
 *
 * @property Sociallink $Sociallink
 * @property PaginatorComponent $Paginator
 */
class ApiController extends AppController {
    /*
     * Components
     *
     * @var array
     */

    public $components = array('Paginator', 'Session', 'Image');
    public $uses = array('Applog', 'User', 'User_role', 'Useraddress', 'Product', 'Productenquiry', 'Material', 'Collection', 'Contact', 'Productimage', 'Staticpage', 'Sitesetting', 'Product', 'Slider', 'Productreview', 'Notification', 'Order', 'Orderdetail', 'Cart', 'Emailcontent', 'Sitesettings', 'Shoppingslider', 'Attributevalue', 'Billingaddress', 'Project', 'Shippingaddress');

    /* ----------------------NEW WEBSERVICE--------------------- */

    /* user registeration */

    public function register() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'access-token');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        try {
            $checkemail = $this->User->find('first', array('conditions' => array('email' => $params['email'])));

            if (empty($checkemail)) {
                $token = $this->str_rand(15, 'alphanum');
                $this->request->data['User'] = $params;
                $this->request->data['User']['password'] = md5($params['password']);
                $this->request->data['User']['passwrd_text'] = ($params['password']);
                $this->request->data['User']['role_id'] = '2';
                $this->request->data['User']['access_token'] = $token;
                $this->request->data['User']['created_date'] = date('Y-m-d H:i:s');
                $this->request->data['User']['modified_date'] = date('Y-m-d H:i:s');
                $this->User->save($this->request->data['User']);
                $user_id = $this->User->getLastInsertID();
                $emailcontent = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '7')));
                $message = str_replace(array('{full_name}'), array($this->request->data['User']['full_name']), $emailcontent['Emailcontent']['emailcontent']);
                $this->mailsend($emailcontent['Emailcontent']['fromname'], $emailcontent['Emailcontent']['fromemail'], $this->request->data['User']['email'], $emailcontent['Emailcontent']['subject'], $message);

                $check = $this->User->find('first', array('conditions' => array('role_id' => 1)));
                $emailcontent = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '4')));
                $message = str_replace(array('{full_name}', '{email}', '{mobile}'), array(
                    $this->request->data['User']['full_name'],
                    $this->request->data['User']['email'],
                    $this->request->data['User']['mobile'],
                        ), $emailcontent['Emailcontent']['emailcontent']);
                $this->mailsend($emailcontent['Emailcontent']['fromname'], $emailcontent['Emailcontent']['fromemail'], $check['User']['email'], $emailcontent['Emailcontent']['subject'], $message);
                $result = array("message" => "Registered Successfully!", "code" => 200, 'user_id' => $user_id, "access_token" => $token);
            } else {
                $result = array("message" => "Email Already Exists!", "code" => 0);
            }
            return json_encode($result);
            exit;
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function fb_login() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'user-id');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        try {
            $check = $this->User->find('first', array('conditions' => array('social_token' => $params['facebook_id'], 'status !=' => 'Trash')));
            if (empty($check)) {
                $checkemail = $this->User->find('first', array('conditions' => array('email' => $params['email'])));
                if (!empty($checkemail)) {
                    $checkemail['User']['social_token'] = (!empty($params['facebook_id'])) ? $params['facebook_id'] : "";
                    $checkemail['User']['mobile'] = (!empty($params['mobile'])) ? $params['mobile'] : "";
                    $checkemail['User']['role_id'] = '2';
                    $checkemail['User']['modified_date'] = date('Y-m-d H:i:s');
                    $this->User->save($checkemail['User']);
                    $result = array("message" => "Login Successfully!", "code" => 200, 'user_id' => $checkemail['User']['user_id'], 'access_token' => $checkemail['User']['access_token']);
                    return json_encode($result);
                    exit;
                } else {
                    $this->request->data['User']['email'] = (!empty($params['email'])) ? $params['email'] : "";
                    $this->request->data['User']['social_token'] = (!empty($params['facebook_id'])) ? $params['facebook_id'] : "";
                    $this->request->data['User']['full_name'] = (!empty($params['full_name'])) ? $params['full_name'] : "";
                    $this->request->data['User']['mobile'] = (!empty($params['mobile'])) ? $params['mobile'] : "";
                    $token = $this->str_rand(15, 'alphanum');
                    $this->request->data['User']['access_token'] = $token;
                    $this->request->data['User']['password'] = "";
                    $this->request->data['User']['passwrd_text'] = "";
                    $this->request->data['User']['social_account'] = "FB";
                    $this->request->data['User']['role_id'] = '2';
                    $this->request->data['User']['created_date'] = date('Y-m-d H:i:s');
                    $this->request->data['User']['modified_date'] = date('Y-m-d H:i:s');
                    $this->request->data['User']['profile'] = (!empty($params['profile'])) ? $this->base64_toimage($params['profile'], 'files/users/') : "";
                    $this->User->save($this->request->data['User']);
                    $user_id = $this->User->getLastInsertID();
                    $emailcontent = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '7')));
                    $message = str_replace(array('{full_name}'), array($this->request->data['User']['full_name']), $emailcontent['Emailcontent']['emailcontent']);
                    $this->mailsend($emailcontent['Emailcontent']['fromname'], $emailcontent['Emailcontent']['fromemail'], $this->request->data['User']['email'], $emailcontent['Emailcontent']['subject'], $message);

                    $check = $this->User->find('first', array('conditions' => array('role_id' => 1)));
                    $emailcontent = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '4')));
                    $message = str_replace(array('{full_name}', '{email}', '{mobile}'), array(
                        $this->request->data['User']['full_name'],
                        $this->request->data['User']['email'],
                        $this->request->data['User']['mobile'],
                            ), $emailcontent['Emailcontent']['emailcontent']);
                    $this->mailsend($emailcontent['Emailcontent']['fromname'], $emailcontent['Emailcontent']['fromemail'], $check['User']['email'], $emailcontent['Emailcontent']['subject'], $message);
                    $result = array("message" => "Login Successfully!", "code" => 200, 'user_id' => $user_id, 'access_token' => $token);
                    return json_encode($result);
                    exit;
                }
            } else {
                $result = array("message" => "Login Successfully!", "code" => 200, 'user_id' => $check['User']['user_id'], 'access_token' => $check['User']['access_token']);
                return json_encode($result);
                exit;
            }
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function google_login() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'user-id');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        try {
            $checkemail = $this->User->find('first', array('conditions' => array('email' => $params['email'])));
            if (empty($checkemail)) {
                $this->request->data['User']['email'] = $params['email'];
                $this->request->data['User']['full_name'] = $params['full_name'];
                $this->request->data['User']['mobile'] = (!empty($params['mobile'])) ? $params['mobile'] : "";
                $token = $this->str_rand(15, 'alphanum');
                $this->request->data['User']['access_token'] = $token;
                $this->request->data['User']['social_account'] = "Google";
                $this->request->data['User']['social_token'] = (!empty($params['google_id'])) ? $params['google_id'] : NULL;
                $this->request->data['User']['role_id'] = '2';
                $this->request->data['User']['created_date'] = date('Y-m-d H:i:s');
                $this->request->data['User']['modified_date'] = date('Y-m-d H:i:s');
                $this->request->data['User']['profile'] = (!empty($params['profile'])) ? $this->base64_toimage($params['profile'], 'files/users/') : "";
                $this->User->save($this->request->data['User']);
                $user_id = $this->User->getLastInsertID();
                $emailcontent = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '7')));
                $message = str_replace(array('{full_name}'), array($this->request->data['User']['full_name']), $emailcontent['Emailcontent']['emailcontent']);
                $this->mailsend($emailcontent['Emailcontent']['fromname'], $emailcontent['Emailcontent']['fromemail'], $this->request->data['User']['email'], $emailcontent['Emailcontent']['subject'], $message);

                $check = $this->User->find('first', array('conditions' => array('role_id' => 1)));
                $emailcontent = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '4')));
                $message = str_replace(array('{full_name}', '{email}', '{mobile}'), array(
                    $this->request->data['User']['full_name'],
                    $this->request->data['User']['email'],
                    $this->request->data['User']['mobile'],
                        ), $emailcontent['Emailcontent']['emailcontent']);
                $this->mailsend($emailcontent['Emailcontent']['fromname'], $emailcontent['Emailcontent']['fromemail'], $check['User']['email'], $emailcontent['Emailcontent']['subject'], $message);
                $result = array("message" => "Login Successfully!", "code" => 200, 'user_id' => $user_id, 'access_token' => $token);
            } else {
                $token = $checkemail['User']['access_token'];
                $user_id = $checkemail['User']['user_id'];
                $result = array("message" => "Login Successfully!", "code" => 200, 'user_id' => $user_id, 'access_token' => $token);
            }
            return json_encode($result);
            exit;
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function login() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'user-id');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        $token = $this->request->header('user-id');
        try {
            if (!empty($params['email'])) {
                $checkuser = $this->User->find('first', array('conditions' => array('email' => $params['email'], 'status' => 'Active')));
            }
            if (!empty($checkuser)) {
                if ($checkuser['User']['password'] == md5($params['password'])) {
                    $result = array("message" => "Logged In Successfully!", "access_token" => $checkuser['User']['access_token'], 'user_id' => $checkuser['User']['user_id'], "code" => 200,);
                } else {
                    $result = array("message" => "Password Mismatch", "code" => 0);
                }
            } else {
                $result = array("message" => "Invalid email ID", "code" => 0);
            }

            return json_encode($result);
            exit;
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    /* user login */

    public function user_getProfile() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'access-token');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        $token = $this->request->header('access-token');
        try {
            $user = $this->checkusertoken($token);
            if (!empty($user)) {
                $data = array(
                    'user_id' => $user['User']['user_id'],
                    'full_name' => $user['User']['full_name'],
                    'email' => !empty($user['User']['email']) ? $user['User']['email'] : "",
                    'mobile' => !empty($user['User']['mobile']) ? (int) $user['User']['mobile'] : "",
                    'profile' => (!empty($user['User']['profile']) ? BASE_URL . 'files/users/' . $user['User']['profile'] : ""),
                    'is_visible_notification' => $user['User']['is_visible_notification'],
                );
                $result = array("data" => $data, "code" => 200);
            } else {
                $result = array("message" => "Invalid User", "code" => 0);
            }
            return json_encode($result);
            exit;
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function salesperson_getProfile() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'access-token');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        $token = $this->request->header('access-token');
        try {
            $user = $this->checkusertoken($token);
            if (!empty($user)) {
                $data = array(
                    'user_id' => $user['User']['user_id'],
                    'full_name' => $user['User']['full_name'],
                    'email' => !empty($user['User']['email']) ? $user['User']['email'] : "",
                    'mobile' => !empty($user['User']['mobile']) ? (int) $user['User']['mobile'] : "",
                    'profile' => (!empty($user['User']['profile']) ? BASE_URL . 'files/users/' . $user['User']['profile'] : ""),
                    'is_visible_notification' => $user['User']['is_visible_notification'],
                    'is_visible_commission' => $user['User']['is_visible_commission'],
                );
                $result = array("data" => $data, "code" => 200);
            } else {
                $result = array("message" => "Invalid User", "code" => 0);
            }
            return json_encode($result);
            exit;
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function user_updateProfile() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'access-token');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        $token = $this->request->header('access-token');
        try {
            $user = $this->checkusertoken($token);
            if (!empty($user)) {
                $checkemail = $this->User->find('first', array('conditions' => array('email' => $params['email'], "user_id !=" => $user['User']['user_id'])));
                if (empty($checkemail)) {
                    $this->request->data['User'] = $params;
                    $this->request->data['User']['profile'] = (!empty($params['profile'])) ? $this->base64_toimage($params['profile'], 'files/users/') : $user['User']['profile'];
                    $this->request->data['User']['user_id'] = $user['User']['user_id'];
                    $this->User->save($this->request->data['User']);
                    $result = array("message" => "Profile Updated", "code" => 200);
                } else {
                    $result = array("message" => "Email Already exists!", "code" => 0);
                }
            } else {
                $result = array("message" => "Invalid User", "code" => 0);
            }
            return json_encode($result);
            exit;
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function salesperson_updateProfile() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'access-token');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        $token = $this->request->header('access-token');
        try {
            $user = $this->checkusertoken($token);
            if (!empty($user)) {
                $checkemail = $this->User->find('first', array('conditions' => array('email' => $params['email'], "user_id !=" => $user['User']['user_id'])));
                if (empty($checkemail)) {
                    $this->request->data['User'] = $params;
                    $this->request->data['User']['profile'] = (!empty($params['profile'])) ? $this->base64_toimage($params['profile'], 'files/users/') : $user['User']['profile'];
                    $this->request->data['User']['user_id'] = $user['User']['user_id'];
                    $this->User->save($this->request->data['User']);
                    $result = array("message" => "Profile Updated", "code" => 200);
                } else {
                    $result = array("message" => "Email Already exists!", "code" => 0);
                }
            } else {
                $result = array("message" => "Invalid User", "code" => 0);
            }
            return json_encode($result);
            exit;
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function forgetpassword() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'access-token');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        $token = $this->request->header('access-token');
        try {
            $user = $this->checkusertoken($token);
            if (!empty($user)) {
                $checkuser = $this->User->find('first', array('conditions' => array('email' => $params['email'])));
                if (!empty($checkuser)) {
                    $this->request->data['User']['otp_code'] = $rand = $this->str_rand(4, 'numeric');
                    $this->User->updateAll(array('otp_code' => $rand), array('email' => $params['email']));
                    $user_id = $checkuser['User']['user_id'];
                    $emailcontent = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '2')));
                    $emailmessage = str_replace(array('{full_name}', '{email}', '{otp_code}'), array($checkuser['User']['full_name'], $checkuser['User']['email'], $rand), $emailcontent['Emailcontent']['emailcontent']);
                    $this->mailsend($emailcontent['Emailcontent']['fromname'], $emailcontent['Emailcontent']['fromemail'], $checkuser['User']['email'], $emailcontent['Emailcontent']['subject'], $emailmessage);
                    $result = array("user_id" => $user_id, "access_token" => $checkuser['User']['access_token'], "message" => "Verification Code sent your mail", "code" => 200, "otp_code" => $this->request->data['User']['otp_code']);
                } else {
                    $result = array("code" => 0, "message" => "Email Not Found");
                }
                return json_encode($result);
                exit;
            }
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function resend() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'access-token');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        $token = $this->request->header('access-token');
        try {
            $user = $this->checkusertoken($token);
            if (!empty($user)) {
                $checkuser = $this->User->find('first', array('conditions' => array('email' => $params['email'])));
                if (!empty($checkuser)) {
                    $this->request->data['User']['otp_code'] = $rand = $this->str_rand(4, 'numeric');
                    $this->User->updateAll(array('otp_code' => $rand), array('email' => $params['email']));
                    $user_id = $checkuser['User']['user_id'];
                    $emailcontent = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '2')));
                    $emailmessage = str_replace(array('{full_name}', '{email}', '{otp_code}'), array($checkuser['User']['full_name'], $checkuser['User']['email'], $rand), $emailcontent['Emailcontent']['emailcontent']);
                    $this->mailsend($emailcontent['Emailcontent']['fromname'], $emailcontent['Emailcontent']['fromemail'], $checkuser['User']['email'], $emailcontent['Emailcontent']['subject'], $emailmessage);
                    $result = array("user_id" => $user_id, "access_token" => $checkuser['User']['access_token'], "message" => "Verification Code sent your mail", "code" => 200, "otp_code" => $this->request->data['User']['otp_code']);
                } else {
                    $result = array("code" => 0, "message" => "Email Not Found");
                }
                return json_encode($result);
                exit;
            }
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function verifycode() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'access-token');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        $token = $this->request->header('access-token');
        try {
            $user = $this->checkusertoken($token);
            if (!empty($user)) {
                $checkotp = $this->User->find('first', array('conditions' => array('user_id' => $user['User']['user_id'], 'otp_code' => $params['otp_code'])));
                if (!empty($checkotp)) {
                    $result = array("message" => "Code Verified", "code" => 200, "user_id" => $checkotp['User']['user_id'], "access_token" => $checkotp['User']['access_token']);
                } else {
                    $result = array("message" => "Invalid Code", "code" => 0);
                }
            } else {
                $result = array("message" => "Email not found", "code" => 0);
            }
            return json_encode($result);
            exit;
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function resetpassword() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'access-token');
        $params = json_decode($this->request->input(), true);
        $token = $this->request->header('access-token');
        extract($_REQUEST);
        try {
            $check = $this->checkusertoken($token);
            if (!empty($check)) {
                if (md5($params['password']) == md5($params['confirm_password'])) {
                    $check['User']['password'] = md5($params['password']);
                    $check['User']['passwrd_text'] = ($params['password']);
                    $this->User->save($check['User']);
                    $result = array("message" => "Password Updated!", "code" => 200);
                } else {
                    $result = array("message" => "Password mismatch!", "code" => 0);
                }
            } else {
                $result = array("message" => "User does not Exist!", "code" => 0);
            }
            return json_encode($result);
            exit;
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function changepassword() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'access-token');
        $params = json_decode($this->request->input(), true);
        $token = $this->request->header('access-token');
        extract($_REQUEST);
        try {
            $user = $this->checkusertoken($token);
            if (!empty($user)) {
                if ($user['User']['password'] == md5($params['old_password'])) {
                    if (md5($params['password']) == md5($params['confirm_password'])) {
                        $user['User']['password'] = md5($params['password']);
                        $user['User']['passwrd_text'] = $params['password'];
                        $this->User->save($user['User']);
                        $result = array("message" => "Password Updated Successfully!", "code" => 200);
                    } else {
                        $result = array("message" => "Password Mismatch", "code" => 0);
                    }
                } else {
                    $result = array("message" => "Old Password Mismatch", "code" => 0);
                }
            } else {
                $result = array("message" => "Invalid user", "code" => 0);
            }
            return json_encode($result);
            exit;
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function contact_us() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'access-token');
        $params = json_decode($this->request->input(), true);
        $token = $this->request->header('access-token');
        extract($_REQUEST);
        try {
            $checkuser = $this->checkusertoken($token);

            $this->request->data['Contact']['user_id'] = $checkuser['User']['user_id'];
            $this->request->data['Contact']['name'] = (!empty($params['name'])) ? $params['name'] : "";
            $this->request->data['Contact']['email'] = (!empty($params['email'])) ? $params['email'] : "";
            $this->request->data['Contact']['mobile'] = (!empty($params['mobile'])) ? $params['mobile'] : "";
            $this->request->data['Contact']['subject'] = (!empty($params['subject'])) ? $params['subject'] : "";
            $this->request->data['Contact']['created_date'] = date('Y-m-d H:i:s');
            $this->Contact->save($this->request->data['Contact']);
            $user_id = $this->Contact->getLastInsertID();
            $emailcontent = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '5')));
            $message = str_replace(array('{name}'), array($this->request->data['Contact']['name']), $emailcontent['Emailcontent']['emailcontent']);
            $this->mailsend($emailcontent['Emailcontent']['fromname'], $emailcontent['Emailcontent']['fromemail'], $this->request->data['Contact']['email'], $emailcontent['Emailcontent']['subject'], $message);

            $check = $this->User->find('first', array('conditions' => array('role_id' => 1)));
            $emailcontent = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '6')));
            $message = str_replace(array('{name}', '{email}', '{mobile}', '{subject}'), array(
                $this->request->data['Contact']['name'],
                $this->request->data['Contact']['email'],
                $this->request->data['Contact']['mobile'],
                $this->request->data['Contact']['subject'],
                    ), $emailcontent['Emailcontent']['emailcontent']);
            $this->mailsend($emailcontent['Emailcontent']['fromname'], $emailcontent['Emailcontent']['fromemail'], $check['User']['email'], $emailcontent['Emailcontent']['subject'], $message);
            $result = array("message" => "Enquiry sent to admin!", "code" => 200);
            return json_encode($result);
            exit;
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function updateaddress() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'access-token');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        $token = $this->request->header('access-token');
        try {
            $user = $this->checkusertoken($token);
            $user_address = $this->Useraddress->find('first', array('conditions' => array('user_id' => $user['User']['user_id'])));
            if (!empty($user_address)) {
                $this->request->data['Useraddress']['billing_address1'] = (!empty($params['billing_address1'])) ? $params['billing_address1'] : $user_address['Useraddress']['billing_address1'];
                $this->request->data['Useraddress']['billing_address2'] = (!empty($params['billing_address2'])) ? $params['billing_address2'] : $user_address['Useraddress']['billing_address2'];
                $this->request->data['Useraddress']['billing_zipcode'] = (!empty($params['billing_zipcode'])) ? $params['billing_zipcode'] : $user_address['Useraddress']['billing_zipcode'];
                $this->request->data['Useraddress']['shipping_address1'] = (!empty($params['shipping_address1'])) ? $params['shipping_address1'] : $user_address['Useraddress']['shipping_address1'];
                $this->request->data['Useraddress']['shipping_address2'] = (!empty($params['shipping_address2'])) ? $params['shipping_address2'] : $user_address['Useraddress']['shipping_address2'];
                $this->request->data['Useraddress']['shipping_zipcode'] = (!empty($params['shipping_zipcode'])) ? $params['shipping_zipcode'] : $user_address['Useraddress']['shipping_zipcode'];
                $this->request->data['Useraddress']['user_id'] = $user['User']['user_id'];
                $this->request->data['Useraddress']['modified_date'] = date('Y-m-d H:i:s');
                $this->request->data['Useraddress']['user_address_id'] = $user_address['Useraddress']['user_address_id'];
                $this->Useraddress->save($this->request->data['Useraddress']);
                $result = array("message" => "Updated", "code" => 200);
            } else {
                $this->request->data['Useraddress']['billing_address1'] = (!empty($params['billing_address1'])) ? $params['billing_address1'] : NULL;
                $this->request->data['Useraddress']['billing_address2'] = (!empty($params['billing_address2'])) ? $params['billing_address2'] : NULL;
                $this->request->data['Useraddress']['billing_zipcode'] = (!empty($params['billing_zipcode'])) ? $params['billing_zipcode'] : NULL;
                $this->request->data['Useraddress']['shipping_address1'] = (!empty($params['shipping_address1'])) ? $params['shipping_address1'] : NULL;
                $this->request->data['Useraddress']['shipping_address2'] = (!empty($params['shipping_address2'])) ? $params['shipping_address2'] : NULL;
                $this->request->data['Useraddress']['shipping_zipcode'] = (!empty($params['shipping_zipcode'])) ? $params['shipping_zipcode'] : NULL;
                $this->request->data['Useraddress']['user_id'] = $user['User']['user_id'];
                $this->request->data['Useraddress']['modified_date'] = date('Y-m-d H:i:s');
                $this->request->data['Useraddress']['created_date'] = date('Y-m-d H:i:s');
                $this->Useraddress->save($this->request->data['Useraddress']);
                $result = array("message" => "Address Added Successfully", "code" => 200);
            }
            return json_encode($result);
            exit;
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function getaddress() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'access-token');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        $token = $this->request->header('access-token');
        try {
            $user = $this->checkusertoken($token);
            $user_address = $this->Useraddress->find('first', array('conditions' => array('user_id' => $user['User']['user_id'])));
            if (!empty($user_address)) {
                $data = array(
                    'user_id' => $user['User']['user_id'],
                    'billing_address1' => $user_address['Useraddress']['billing_address1'],
                    'billing_address2' => $user_address['Useraddress']['billing_address2'],
                    'billing_zipcode' => $user_address['Useraddress']['billing_zipcode'],
                    'shipping_address1' => $user_address['Useraddress']['shipping_address1'],
                    'shipping_address2' => $user_address['Useraddress']['shipping_address2'],
                    'shipping_zipcode' => $user_address['Useraddress']['shipping_zipcode'],
                );
                $result = array("data" => $data, "code" => 200);
            } else {
                $result = array("message" => "Invalid User", "code" => 0);
            }
            return json_encode($result);
            exit;
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function add_user() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'access-token');
        $params = json_decode($this->request->input(), true);
        $token = $this->request->header('access-token');
        extract($_REQUEST);
        try {
            $user = $this->checkusertoken($token);
            if (!empty($user)) {
                $check_already = $this->User->find('first', array('conditions' => array('email' => $params['email'])));
                if (empty($check_already)) {
                    $password = $this->str_rand(6, 'alphanum');
                    $token = $this->str_rand(15, 'alphanum');
                    $this->request->data['User'] = $params;
                    $this->request->data['User']['passwrd_text'] = $password;
                    $this->request->data['User']['password'] = md5($password);
                    $this->request->data['User']['role_id'] = '2';
                    $this->request->data['User']['access_token'] = $token;
                    $this->request->data['User']['sales_person_id'] = $user['User']['user_id'];
                    $this->request->data['User']['created_date'] = date('Y-m-d H:i:s');
                    $this->request->data['User']['modified_date'] = date('Y-m-d H:i:s');
                    $this->User->save($this->request->data['User']);
                    $user_id = $this->User->getLastInsertID();
                    $email = $this->request->data['User']['email'];
                    $name = $this->request->data['User']['full_name'];
                    $emaillist1 = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '1')));
                    $message1 = str_replace(array('{name}', '{email}', '{password}'), array($name, $email, $password), $emaillist1['Emailcontent']['emailcontent']);
                    $this->mailsend($emaillist1['Emailcontent']['fromname'], $emaillist1['Emailcontent']['fromemail'], $email, $emaillist1['Emailcontent']['subject'], $message1);

                    $check = $this->User->find('first', array('conditions' => array('role_id' => 1)));
                    $emailcontent = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '4')));
                    $message = str_replace(array('{full_name}', '{email}', '{mobile}'), array(
                        $this->request->data['User']['full_name'],
                        $this->request->data['User']['email'],
                        $this->request->data['User']['mobile'],
                            ), $emailcontent['Emailcontent']['emailcontent']);
                    $this->mailsend($emailcontent['Emailcontent']['fromname'], $emailcontent['Emailcontent']['fromemail'], $check['User']['email'], $emailcontent['Emailcontent']['subject'], $message);
                    $result = array("message" => "User Added Successfully!", "code" => 200, 'user_id' => $user_id, "access_token" => $token);
                } else {
                    $result = array("message" => "Email Already Exists!", "code" => 0);
                }
            } else {
                $result = array("message" => "Invalid User!", "code" => 0);
            }
            return json_encode($result);
            exit;
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function list_user() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'access-token');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        $token = $this->request->header('access-token');
        try {
            $user = $this->checkusertoken($token);
            $conditions = array('sales_person_id' => $user['User']['user_id'], 'status !=' => 'Trash');
            $users_list = $this->User->find('all', array('conditions' => $conditions, 'order' => 'user_id DESC'));
            $count = array();
            $data = array();

            foreach ($users_list as $user_list) {
                $data[] = array(
                    'user_id' => $user_list['User']['user_id'],
                    'full_name' => $user_list['User']['full_name'],
                    'email' => $user_list['User']['email'],
                    'mobile' => $user_list['User']['mobile'],
                    'created_date' => date('d-m-Y', strtotime($user_list['User']['created_date'])),
                );
            }
            $count = count($users_list);
            $result = array("total_user" => $count, "data" => $data, "code" => 200);
            return json_encode($result);
            exit;
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function view_user($id = NULL) {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'access-token');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        $token = $this->request->header('access-token');
        try {
            $user = $this->checkusertoken($token);
            $user_details = $this->User->find('first', array('conditions' => array('user_id' => $id)));
            if (!empty($user)) {
                $data = array(
                    'user_id' => $user_details['User']['user_id'],
                    'full_name' => $user_details['User']['full_name'],
                    'email' => $user_details['User']['email'],
                    'mobile' => $user_details['User']['mobile'],
                    'created_date' => date('d-m-Y', strtotime($user_details['User']['created_date'])),
                );
                $result = array("data" => $data, "code" => 200);
            } else {
                $result = array("message" => "Invalid User", "code" => 0);
            }
            return json_encode($result);
            exit;
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function home() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'access-token');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        $token = $this->request->header('access-token');
        try {
            $user = $this->checkusertoken($token);
            $check = $this->User->find('first', array('conditions' => array('user_id' => $user['User']['user_id'])));
            $sliderdata = array();
            $sliders = $this->Slider->find('all');
            foreach ($sliders as $slider) {
                $sliderdata[] = array(
                    'slider_name' => BASE_URL . 'files/sliders/' . $slider['Slider']['slider_name'],
                );
            }
            $materials = $this->Material->find('all', array('conditions' => array('status' => 'Active'), 'order' => 'material_id DESC'));
            $data_materials = array();
            foreach ($materials as $material) {
                $data_materials[] = array(
                    'material_id' => $material['Material']['material_id'],
                    'material_name' => $material['Material']['material_name'],
                    'material_image' => BASE_URL . 'files/material/' . $material['Material']['material_image']
                );
            }

            $products = $this->Product->find('all', array('conditions' => array('status' => 'Active', 'latest_collection' => '1'), 'order' => 'created_date DESC'));
            $data_product = array();
            foreach ($products as $product) {
                $productimages = $this->Productimage->find('first', array('conditions' => array('product_id' => $product['Product']['product_id'])));
                $img = (!empty($productimages)) ? BASE_URL . 'files/products/' . $productimages['Productimage']['image'] : "";
                $offer_amount = $product['Product']['regular_price'] - $product['Product']['sales_price'];
                if ($product['Product']['commission_type'] == "Percentage") {
                    $commission = $product['Product']['total_price'] * $product['Product']['commission'] / 100;
                } else if ($product['Product']['commission_type'] == "Fixed") {
                    $commission = $product['Product']['commission'];
                }
                if ($check['User']['role_id'] == 3) {
                    $data_product[] = array(
                        'product_id' => $product['Product']['product_id'],
                        'product_image' => $img,
                        'product_name' => $product['Product']['product_name'],
                        'offer_amount' => $offer_amount,
                        'regular_price' => $product['Product']['regular_price'],
                        'sales_price' => $product['Product']['sales_price'],
                        'commission' => round($commission),
                        'favourite' => $this->checkWishlist($product['Product']['product_id'], $check['User']['user_id']),
                        'wishlist_id' => $this->checkWishlistid($product['Product']['product_id'], $check['User']['user_id']),
                    );
                } else if ($check['User']['role_id'] == 2) {
                    $data_product[] = array(
                        'product_id' => $product['Product']['product_id'],
                        'product_image' => $img,
                        'product_name' => $product['Product']['product_name'],
                        'offer_amount' => $offer_amount,
                        'regular_price' => $product['Product']['regular_price'],
                        'sales_price' => $product['Product']['sales_price'],
                        'favourite' => $this->checkWishlist($product['Product']['product_id'], $check['User']['user_id']),
                        'wishlist_id' => $this->checkWishlistid($product['Product']['product_id'], $check['User']['user_id']),
                    );
                }
            }
            $result = array("sliders" => $sliderdata, "materials" => $data_materials, "product" => $data_product, "code" => 200);
            return json_encode($result);
            exit;
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function productlist() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'access-token');
        $params = json_decode($this->request->input(), true);
        $token = $this->request->header('access-token');
        extract($_REQUEST);
        try {
            $check = $this->checkusertoken($token);
            $conditions[] = array('Product.status' => 'Active');
            if (!empty($_REQUEST['s'])) {
                $conditions[] = array('Product.product_name LIKE' => "%" . $_REQUEST['s'] . "%");
            }

            if (!empty($_REQUEST['material_id'])) {
                $conditions[] = array('FIND_IN_SET(\'' . ($_REQUEST['material_id']) . '\',Product.material_id)');
            }
            if (!empty($_REQUEST['collection_id'])) {
                $conditions[] = array('Product.collection_id' => $_REQUEST['collection_id']);
            }
            if (!empty($_REQUEST['subcollection_id'])) {
                $conditions[] = array('FIND_IN_SET(\'' . ($_REQUEST['subcollection_id']) . '\',Product.subcollection_id)');
            }
            if (!empty($_REQUEST['project_id'])) {
                $conditions[] = array('Product.project_id' => $_REQUEST['project_id']);
            }
            if (!empty($_REQUEST['type'])) {
                $check_Product = array();
                $product_conditions[] = array('Productattribute.attributeval_id' => $_REQUEST['type']);
                $check_Product = $this->Productattribute->find('all', array('conditions' => $product_conditions));
                foreach ($check_Product as $check_Products) {
                    $conditions[] = array('Product.product_id' => $check_Products['Productattribute']['product_id']);
                }
            }
            if (!empty($_REQUEST['style'])) {
                $check_Product = array();
                $product_conditions[] = array('Productattribute.attributeval_id' => $_REQUEST['style']);
                $check_Product = $this->Productattribute->find('all', array('conditions' => $product_conditions));
                foreach ($check_Product as $check_Products) {
                    $conditions[] = array('Product.product_id' => $check_Products['Productattribute']['product_id']);
                }
            }
            if (isset($_REQUEST['price_min']) && ($_REQUEST['price_min'] != "") || isset($_REQUEST['price_max']) && ($_REQUEST['price_max'] != "")) {
                $conditions['OR'] = array('Product.sales_price >=' => $_REQUEST['price_min'],'Product.sales_price <=' => $_REQUEST['price_max']);
            }
             if (isset($_REQUEST['price_min']) && ($_REQUEST['price_min'] != "") && isset($_REQUEST['price_max']) && ($_REQUEST['price_max'] != "")) {
                $conditions['AND'] = array('Product.sales_price >=' => $_REQUEST['price_min'],'Product.sales_price <=' => $_REQUEST['price_max']);
            }
            if (isset($_REQUEST['weight_min']) && ($_REQUEST['weight_min'] != "") && isset($_REQUEST['weight_max']) && ($_REQUEST['weight_max'] != "")) {
                $conditions['AND'] = array('Product.gross_weight >=' => $_REQUEST['weight_min'], 'Product.gross_weight <=' => $_REQUEST['weight_max']);
            }
            if (isset($_REQUEST['order']) && $_REQUEST['order'] == 'low-high') {
                $order = array('sales_price ASC');
            } else if (isset($_REQUEST['order']) && $_REQUEST['order'] == 'high-low') {
                $order = array('sales_price DESC');
            } else if (isset($_REQUEST['popular']) && $_REQUEST['popular'] == 'Highest') {
                $order = array('order_count DESC');
            } else {
                $order = 'product_id DESC';
            }
            if (!empty($check)) {
                $products = $this->Product->find('all', array('conditions' => $conditions, 'order' => $order));
                $data_product = array();
                foreach ($products as $product) {
                    $productimages = $this->Productimage->find('first', array('conditions' => array('product_id' => $product['Product']['product_id'])));
                    $img = (!empty($productimages)) ? BASE_URL . 'files/products/' . $productimages['Productimage']['image'] : "";
                    $offer_amount = $product['Product']['regular_price'] - $product['Product']['sales_price'];
                    if ($product['Product']['commission_type'] == "Percentage") {
                        $commission = $product['Product']['total_price'] * $product['Product']['commission'] / 100;
                    } else if ($product['Product']['commission_type'] == "Fixed") {
                        $commission = $product['Product']['commission'];
                    }
                    if ($check['User']['role_id'] == 3) {
                        $data_product[] = array(
                            'product_id' => $product['Product']['product_id'],
                            'product_image' => $img,
                            'product_name' => $product['Product']['product_name'],
                            'offer_amount' => $offer_amount,
                            'regular_price' => $product['Product']['regular_price'],
                            'sales_price' => $product['Product']['sales_price'],
                            'commission' => round($commission),
                            'favourite' => $this->checkWishlist($product['Product']['product_id'], $check['User']['user_id']),
                            'wishlist_id' => $this->checkWishlistid($product['Product']['product_id'], $check['User']['user_id']),
                        );
                    } else if ($check['User']['role_id'] == 2) {
                        $data_product[] = array(
                            'product_id' => $product['Product']['product_id'],
                            'product_image' => $img,
                            'product_name' => $product['Product']['product_name'],
                            'offer_amount' => $offer_amount,
                            'regular_price' => $product['Product']['regular_price'],
                            'sales_price' => $product['Product']['sales_price'],
                            'favourite' => $this->checkWishlist($product['Product']['product_id'], $check['User']['user_id']),
                            'wishlist_id' => $this->checkWishlistid($product['Product']['product_id'], $check['User']['user_id']),
                        );
                    }
                }
                $result = array('code' => '200', 'data' => $data_product);
            } else {
                $result = array('code' => '0', "message" => "Invalid User");
            }
            return json_encode($result);
            exit;
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function materials() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'access-token');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        $token = $this->request->header('access-token');
        try {
            $materials = $this->Material->find('all', array('conditions' => array('status' => 'Active'), 'order' => 'material_id DESC'));
            $data_materials = array();
            foreach ($materials as $material) {
                $data_materials[] = array(
                    'material_id' => $material['Material']['material_id'],
                    'material_name' => $material['Material']['material_name'],
                    'material_image' => BASE_URL . 'files/material/' . $material['Material']['material_image']
                );
            }
            $result = array("data" => $data_materials, "code" => 200);
            return json_encode($result);
            exit;
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function projects() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'access-token');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        $token = $this->request->header('access-token');
        try {
            $projects = $this->Project->find('all', array('conditions' => array('status' => 'Active'), 'order' => 'project_id DESC'));
            $data_project = array();
            foreach ($projects as $project) {
                $data_project[] = array(
                    'project_id' => $project['Project']['project_id'],
                    'project_name' => $project['Project']['project_name'],
                );
            }
            $result = array("data" => $data_project, "code" => 200);
            return json_encode($result);
            exit;
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function collections() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'access-token');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        $token = $this->request->header('access-token');
        try {
            $collections = $this->Collection->find('all', array('conditions' => array('status' => 'Active', 'parent_collection' => '0'), 'order' => 'collection_id DESC'));
            $data_collection = array();
            foreach ($collections as $collection) {
                $data_collection[] = array(
                    'collection_id' => $collection['Collection']['collection_id'],
                    'collection_name' => $collection['Collection']['collection_name'],
                );
            }
            $result = array("data" => $data_collection, "code" => 200);
            return json_encode($result);
            exit;
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function subcollections($id = NULL) {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'access-token');
        $params = json_decode($this->request->input(), true);
        $token = $this->request->header('access-token');
        extract($_REQUEST);
        try {
            $get_collection = $this->Collection->find('all', array('conditions' => array('parent_collection' => $id), 'order' => 'collection_id DESC'));
            $data_collection = array();
            foreach ($get_collection as $collection) {
                $data_collection[] = array(
                    'collection_id' => $collection['Collection']['collection_id'],
                    'collection_name' => $collection['Collection']['collection_name'],
                );
            }
            $result = array("data" => $data_collection, "code" => 200);
            return json_encode($result);
            exit;
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function attri_style() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'access-token');
        $params = json_decode($this->request->input(), true);
        $token = $this->request->header('access-token');
        extract($_REQUEST);
        try {
            $get_styles = $this->Attributevalue->find('all', array('conditions' => array('attribute_id' => 1), 'order' => 'attributeval_id DESC'));
            $data_style = array();
            foreach ($get_styles as $get_style) {
                $data_style[] = array(
                    'id' => $get_style['Attributevalue']['attributeval_id'],
                    'style' => $get_style['Attributevalue']['value'],
                );
            }
            $result = array("data" => $data_style, "code" => 200);
            return json_encode($result);
            exit;
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function attri_type() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Headers', 'access-token');
        $params = json_decode($this->request->input(), true);
        $token = $this->request->header('access-token');
        extract($_REQUEST);
        try {
            $get_styles = $this->Attributevalue->find('all', array('conditions' => array('attribute_id' => 5), 'order' => 'attributeval_id DESC'));
            $data_style = array();
            foreach ($get_styles as $get_style) {
                $data_style[] = array(
                    'id' => $get_style['Attributevalue']['attributeval_id'],
                    'type' => $get_style['Attributevalue']['value'],
                );
            }
            $result = array("data" => $data_style, "code" => 200);
            return json_encode($result);
            exit;
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

}
