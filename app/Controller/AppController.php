<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');
App::import('Controller', 'Yahoo'); // mention at top
App::uses('Adaptive', 'Lib');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $components = array('Cookie', 'Session');
    public $helpers = array('Session');

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array('Adminuser', 'Emaillist', 'Emailcontent', 'User', 'Smssetting', 'Cart', 'Product', 'Productimage', 'Attribute', 'Productattribute', 'Wishlist', 'Notification');
    private $yqlUrl = "http://query.yahooapis.com/v1/public/yql";
    private $options = array("env" => "http://datatables.org/alltables.env", "format" => "json"); // need this env to query yahoo finance);
    private $format;

    private function dateToDBString($date) {
        assert('is_object($date) && get_class($date) == "DateTime"');
        return $date->format('Y-m-d');
    }

    protected function str_rand($length = 8, $output = 'alphanum') {
// Possible seeds
        $outputs['alpha'] = 'abcdefghijklmnopqrstuvwqyz';
        $outputs['numeric'] = '0123456789';
        $outputs['alphanum'] = 'abcdefghijklmnopqrstuvwqyz0123456789';
        $outputs['hexadec'] = '0123456789abcdef';
// Choose seed
        if (isset($outputs[$output])) {
            $output = $outputs[$output];
        }
// Seed generator
        list($usec, $sec) = explode(' ', microtime());
        $seed = (float) $sec + ((float) $usec * 100000);
        mt_srand($seed);
// Generate
        $str = '';
        $output_count = strlen($output);
        for ($i = 0; $length > $i; $i++) {
            $str .= $output{mt_rand(0, $output_count - 1)};
        }
        return $str;
    }

    public function admin_logincheck() {
        if ($this->Session->read('User'))
            $this->redirect(array('controller' => 'adminusers', 'action' => 'profile'));
    }

    public function logout() { 
        $this->Session->delete('Sitelogin');
        $this->Session->delete('User');
        $this->Session->delete('Userlogin');
        $this->redirect(array('controller' => 'home', 'action' => 'index'));
    }

    public function checkadmin() {
        if (!$this->Session->read('User')) {
            $this->redirect(array('controller' => 'adminusers', 'action' => 'login'));
        } else {
            $admindetails = $this->User->find('first', array('conditions' => array('user_id' => $this->Session->read('User.user_id'), 'status' => 'Active','role_id'=>1)));
            if (!empty($admindetails)) {
                $this->set('sessionadmin', $admindetails);
            } else {
                $this->redirect(array('action' => 'logout'));
            }
        }
    }

    public function checkusertoken($token) {
        $check = $this->User->find('first', array('conditions' => array('access_token' => $token)));
        if (empty($check)) {
            return false;
        } else {
            return $check;
        }
    }
    
    public function admin_logout() {
        $admindetails = $this->User->find('first', array('conditions' => array('user_id' => $this->Session->read('User.user_id'), 'status' => 'Active','role_id'=>1)));
        $this->Session->delete('User');
        $this->redirect(array('controller' => 'adminusers', 'action' => 'login'));
    }

    protected function mailsend($fromname, $from, $to, $subject, $message, $cc = NULL, $attachment = 0, $attachmentsfiles = '', $template = 'default', $layout = 'default') {
        App::uses('CakeEmail', 'Network/Email');
        $email = new CakeEmail();
        if ($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == '192.168.1.3') {
            $email->config('smtp');
        }
        $email->emailFormat('html');
        $email->from(array(trim($from) => $fromname));
        if ($attachment == 1) {
            $attach = explode(",", $attachmentsfiles);

            $attachments = array();
            foreach ($attach as $attach) {
                $attachments[] = $attach;
            }

            $email->attachments($attachments);
        }
        $email->template($template, $layout);
        $email->to(trim($to));
//        if ($cc != '' || $cc != NULL) {
//            $email->cc($cc);
//        }
        $email->subject($subject);
        $email->send($message);
        $email->reset();
    }

// Function to get the client ip address
    protected function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if (isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }


    public function sqlformat($date, $time) {
        list($m, $d, $y) = explode('-', $date);
        $times = mktime(0, 0, 0, $m, $d, $y);
        $dates = date('Y-m-d', $times);
        return date("Y-m-d H:i:s", strtotime("$dates $time"));
    }


    public function getFileExtension($str) {
        $i = strrpos($str, ".");
        if (!$i) {
            return "";
        }
        $l = strlen($str) - $i;
        $ext = substr($str, $i + 1, $l);
        return $ext;
    }

    public function upload_file($count, $file, $path, $title) {
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        if ($count == 0) {
            $video = $title . '.' . $ext;
        } else {
            $video = $title . '-' . $count . '.' . $ext;
        }
        return $video;
    }

    public function slugify($string) {
        $string = preg_replace("`\[.*\]`U", "", $string);
        $string = preg_replace('`&(amp;)?#?[a-z0-9]+;`i', '-', $string);
        $string = htmlentities($string, ENT_COMPAT, 'utf-8');
        $string = preg_replace("`&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);`i", "\\1", $string);
        $string = preg_replace(array("`[^a-z0-9]`i", "`[-]+`"), "-", $string);
        return strtolower(trim($string, '-'));
    }

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Flash');
        /* Authentication */
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginRedirect' => [
                'controller' => 'users',
                'action' => 'profile'
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login'
            ]
        ]);
    }


    public function base64_to_file($base64_string, $output_file) {
        $ifp = fopen($output_file, "wb");
        fwrite($ifp, base64_decode($base64_string));
        fclose($ifp);
        return $output_file;
    }

    function guessImageMime($data) {
        if ($data[0] == '/') {
            return "jpeg";
        } else if ($data[0] == 'R') {
            return "gif";
        } else if ($data[0] == 'i') {
            return "png";
        } else {
            return "jpeg";
        }
    }

    function base64_toimage($data, $path) {
        if ($data[0] == '/') {
            $type = "jpeg";
        } else if ($data[0] == 'R') {
            $type = "gif";
        } else if ($data[0] == 'i') {
            $type = "png";
        } else if ($data[0] == '0') {
            $type = "docx";
        } else if ($data[0] == 'J') {
            $type = "pdf";
        } else {
            $type = "jpeg";
        }
        $image = base64_decode($data);
        $imgFile = rand(0, 10000) . time() . "." . $type;
        $ifp = fopen($path . $imgFile, "wb");
        fwrite($ifp, base64_decode($data));
        fclose($ifp);
        return $imgFile;
    }

    function web_to_server($data, $path) {
        $extension = $this->getFileExtension($data['name']);
        $extension = strtolower($extension);
        $m = explode(".", $data['name']);
        $imagename = time() . $m[0] . "." . $extension;
        $destination = $path . $imagename;
        move_uploaded_file($data['tmp_name'], $destination);
        return $imagename;
    }

    public function updatelastaccess($user_id) {
        $this->request->data['User']['user_id'] = $user_id;
        $this->request->data['User']['last_login'] = date('Y-m-d H:i:s');
        $this->User->save($this->request->data['User']);
    }
    
    function user_chatpath($user_id) {
        $filename = "files/chats/user_" . $user_id . "/";
        if (!file_exists($filename)) {
            mkdir($filename, 0755);
        }
        return $filename;
    }

    public function getProductdetail($id = NULL, $user_id = NULL, $type = NULL) {
        if (!empty($this->Session->read('User.user_id'))) {
            $user_id = $this->Session->read('User.user_id');
        }
        $product = $this->Product->find('first', array('conditions' => array('product_id' => $id)));
        if ($type == "shopping") {
            $unit = $this->Unit->find('first', array('conditions' => array('unit_id' => $product['Product']['unit_id'])));
            $short_name = !empty($unit['Unit']['unit_short_name']) ? $unit['Unit']['unit_short_name'] : "";
            $category = $this->Category->find('first', array('conditions' => array('category_id' => $product['Product']['category_id'])));
            $category_id = $category['Category']['category_id'];
        } else if ($type == "deal") {
            $unit = $this->Unit->find('first', array('conditions' => array('unit_id' => $product['Product']['unit_id'])));
            $short_name = !empty($unit['Unit']['unit_short_name']) ? $unit['Unit']['unit_short_name'] : "";
            $category = $this->Dealcategory->find('first', array('conditions' => array('id' => $product['Product']['category_id'])));
            $category_id = $category['Category']['category_id'];
        }

        if (!empty($product['Product']['sales_price'])) {
            $offer = (($product['Product']['regular_price'] - $product['Product']['sales_price']) * 100) / $product['Product']['regular_price'];
            $off = round($offer);
        } else {
            $off = "";
        }
        $attributes = $this->Productattribute->find('all', array('conditions' => array('product_id' => $id, 'isvisible' => 1), 'group' => array('attribute_id')));
        $attri = array();
        foreach ($attributes as $attribute) {
            $attr = $this->Attribute->find('first', array('conditions' => array('attribute_id' => $attribute['Productattribute']['attribute_id'])));


            if (!empty($attr)) {
                $atts = $this->Productattribute->find('all', array('conditions' => array('product_id' => $id, 'attribute_id' => $attr['Attribute']['attribute_id'], 'isvisible' => 1)));
                $values = array();
                foreach ($atts as $att) {
                    $values[] = $att['Productattribute']['value'];
                }
            }
            $attri[] = array(
                'title' => $attr['Attribute']['attribute_name'],
                'values' => (implode(', ', $values))
            );
        }


        $data['product'] = array();
        $data['vendor'] = array();
        $data['product'] = array(
            'category_title' => (!empty($category)) ? $category['Category']['name'] : "",
            'category_id' => $category_id,
            'product_id' => $product['Product']['product_id'],
            'name' => $product['Product']['name'],
            'description' => $product['Product']['description'],
            'regular_price' => $product['Product']['regular_price'],
            'sales_price' => $product['Product']['sales_price'],
            'unit' => $short_name,
            'offer' => $off,
            'vendor_id' => $product['Product']['vendor_id'],
            'favourite' => $this->checkWishlist($product['Product']['product_id'], $user_id),
            'wishlist_id' => $this->checkWishlistid($product['Product']['product_id'], $user_id),
            'stock_status' => $product['Product']['stock_status'],
            'location' => !empty($product['Product']['location']) ? $product['Product']['location'] : "",
            'rating' => $this->product_rating($product['Product']['product_id']),
            'attributes' => $attri,
        );

        $vendor = $this->Vendor->find('first', array('conditions' => array('vendor_id' => $product['Product']['vendor_id'])));
        $vendor_path = $this->vendor_path($vendor['Vendor']['vendor_id']);
        $data['vendor'] = array(
            'vendor_id' => $vendor['Vendor']['vendor_id'],
            'shop_name' => $vendor['Vendor']['shop_name'],
            'name' => $vendor['Vendor']['full_name'],
            'location' => $vendor['Vendor']['location'],
            'logo' => BASE_URL . $vendor_path . $vendor['Vendor']['shop_logo'],
        );
        $productimages = $this->Productimage->find('all', array('conditions' => array('product_id' => $product['Product']['product_id'])));

        if (!empty($productimages)) {
            foreach ($productimages as $productimage) {
                $data['images'][] = BASE_URL . $vendor_path . '/' . $productimage['Productimage']['image'];
            }
        } else {
            $data['images'] = array();
        }

        $data['thumb'] = (!empty($data['images'])) ? $data['images'][0] : "";
        $data['reviews'] = array();
        $data['reviews'] = $this->getProductreviews($id);
        return $data;
    }

    public function checkWishlist($product_id, $user_id) {
        $wishlist = false;
        if (!empty($user_id)) {
            $check = $this->Wishlist->find('first', array('conditions' => array('user_id' => $user_id, 'product_id' => $product_id)));
            if (!empty($check)) {
                $wishlist = true;
            }
        }
        return $wishlist;
    }

    public function checkWishlistid($product_id, $user_id) {
        $wishlist = false;
        if (!empty($user_id)) {
            $check = $this->Wishlist->find('first', array('conditions' => array('user_id' => $user_id, 'product_id' => $product_id)));
            if (!empty($check)) {
                $wishlist = $check['Wishlist']['wish_id'];
            }
        }
        return $wishlist;
    }

    public function send_push_notification($registatoin_ids = null, $message) {

        $url = 'https://fcm.googleapis.com/fcm/send';
        $message['notifydata']['click_action'] = 'OPEN_ACTIVITY_1';
        $msg = array
            (
            'body' => '',
            'title' => $message['notifydata']['message'],
            'notify_from' => $message['notifydata']['notify_from'],
            'icon' => 'myicon',
            'sound' => 'mySound',
            "click_action" => "OPEN_ACTIVITY_1",
            'badge' => '1',
            'notification' => $message['notifydata'],
            'id' => isset($message['notifydata']['id']) ? $message['notifydata']['id'] : ""
        );
        foreach ($registatoin_ids as $registatoin_ids) {
            $fields = array('to' => $registatoin_ids, 'data' => $msg);


            $headers = array('Authorization: key=AAAAiL0ISp8:APA91bGUCI7caJJqg1SLKI7EftZGDYj5TaMeazpcjryol6HHyAM5pBsMqyPqLrQhhdZb9QJrrtgCDs9_iQMpdkdKV-MopyyKgQo9rc_5w6_Nm_3YBVUtdxpAFSAoX2Er6NiPEXJkB-QL', 'Content-Type: application/json');

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);

            $this->request->data['Notification']['msg'] = $message['notifydata']['message'];
            $this->request->data['Notification']['notify_from'] = $message['notifydata']['notify_from'];
            $this->request->data['Notification']['id'] = $message['notifydata']['id'];
            $this->request->data['Notification']['to'] = $message['notifydata']['to'];
            $this->request->data['Notification']['to_id'] = $message['notifydata']['to_id'];
            $this->request->data['Notification']['created_date'] = date('Y-m-d H:i:s');
            $this->Notification->saveAll($this->request->data['Notification']);
            if ($message['notifydata']['to'] == 'user') {
                $this->User->updateAll(
                        array('User.notification_count' => "notification_count+1"), array('User.user_id' => $message['notifydata']['to_id'])
                );
            } else {
                $this->Vendor->updateAll(
                        array('Vendor.notification_count' => "notification_count+1"), array('Vendor.vendor_id' => $message['notifydata']['to_id'])
                );
            }

            if ($result === FALSE) {

                return ('Curl failed: ' . curl_error($ch));
            }

            curl_close($ch);
        }
        return $result;
    }

    public function lequillaOrderId($id) {
        return 'LeQ' . sprintf("%03d", $id);
    }


}
