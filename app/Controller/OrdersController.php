<?php

App::uses('AppController', 'Controller');

/**
 * Products Controller
 *
 * @property Product $Product
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class OrdersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session', 'Image');
    public $uses = array('Adminuser', 'Product', 'Productimage', 'Order', 'Orderdetail', 'Billingaddress', 'Shippingaddress', 'Couponcode');

    /**
     * AdminIndex
     *
     * @return void
     */
    public function admin_index() {
        $this->layout = 'admin';
        $this->Order->recursive = 0;
        $this->checkadmin();   
        if (isset($_REQUEST['search'])) {
            if (!empty($_REQUEST['s'])) {
                $conditions['order_id'] = array('order_id' => $_REQUEST['s']);
            }if (!empty($_REQUEST['status'])) {
                $conditions['order_status'] = $_REQUEST['status'];
            }
        }
        $this->paginate = array('conditions' => $conditions, 'order' => 'order_id DESC', 'limit' => '30');
        $this->set('orders', $this->Paginator->paginate('Order'));
    }

    public function admin_orderdetail($id = NULL) {
        $this->layout = 'admin';
        $this->Order->recursive = 0;
        $this->checkadmin();

        if ($this->request->is('post')) {
            $this->Order->save($this->request->data['Order']);
            $this->Session->setFlash('Updated successfully', '', array(''), 'success');
            $this->redirect($this->referer());
        }
        $order = $this->Order->find('first', array('conditions' => array('order_id' => $id)));
        $user = $this->User->find('first', array('conditions' => array('user_id' => $order['Order']['user_id'])));
        /* User mail */
        $bookingdetail = $this->get_orderdetails($id);
        $status = "<p>Your Order status <strong>" . $id . "</strong>  updated to <strong>" . $order['Order']['status'] . "</strong></p>";
        $emailcontent = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '8')));
        $emailmessage = str_replace(array('{name}', '{status}', '{content}'), array($user['User']['full_name'], $status, $bookingdetail), $emailcontent['Emailcontent']['emailcontent']);
        $this->mailsend($emailcontent['Emailcontent']['fromname'], $emailcontent['Emailcontent']['fromemail'], $user['User']['email'], $emailcontent['Emailcontent']['subject'], $emailmessage);

        $orders = ClassRegistry::init('Order')->find('first', array('conditions' => array('order_id' => $id)));
        $data = $orders['Order'];

        $orderdetails = $this->Orderdetail->find('all', array('conditions' => array('order_id' => $id, 'orderdetail_status' => 'Active')));
        foreach ($orderdetails as $orderdetail) {
            $product = $this->getProductdetail($orderdetail['Orderdetail']['product_id']);
            $products[] = array_merge($product, $orderdetail['Orderdetail']);
        }
        $this->set('order', $data);
        $this->set('products', $products);
    }
 

    public function get_orderdetails($orderid = null) {
        //    $this->autoRender = false;
        $result = $this->Order->find('first', array('conditions' => array('order_id' => $orderid)));
        $discount = !empty($result['Order']['discount_amount']) ? $result['Order']['discount_amount'] : "";
        $delivery = !empty($result['Order']['delivery_charge']) ? $result['Order']['delivery_charge'] : "";
        $delivery_date = !empty($result['Order']['delivery_date']) ? date('d-m-y', strtotime($result['Order']['delivery_date'])) : "";

        $book_detail = ' <table class="mailtable" style="border-collapse: collapse;" border="1">';
        $book_detail .= ' <thead>';
        $book_detail .= ' <tr>';
        $book_detail .= ' <th align="left" style="padding:8px;">S.No</th>';
        $book_detail .= ' <th align="left" style="padding:8px;">Product</th>';
        $book_detail .= ' <th align="left" style="padding:8px;">Order Qty</th>';
        $book_detail .= ' <th align="left" style="padding:8px;">Mrp</th>';
        $book_detail .= ' <th align="left" style="padding:8px;">Total</th>';
        $book_detail .= ' </tr>';
        $book_detail .= ' </thead>';
        $book_detail .= ' <tbody>';
        $details = $this->Orderdetail->find('all', array('conditions' => array('order_id' => $orderid)));
        $i = 1;
        foreach ($details as $detail) {
            $product = $this->Product->find('first', array('conditions' => array('product_id' => $detail['Orderdetail']['product_id'])));
            $book_detail .= ' <tr>';
            $book_detail .= ' <td style="padding:8px;">' . $i . '</td>';
            $book_detail .= ' <td  style="padding:8px;">' . $product['Product']['name'] . '</td>';
            $book_detail .= ' <td style="padding:8px;">' . $detail['Orderdetail']['qty'] . '</td>';
            $book_detail .= ' <td style="padding:8px;">Rs ' . $product['Product']['sales_price'] . '</td>';
            $book_detail .= ' <td style="padding:8px;">Rs ' . round($detail['Orderdetail']['total_amount']) . '</td>';
            $book_detail .= ' </tr>';
            $i++;
        }
        $book_detail .= ' <tr>';
        $book_detail .= ' <td  colspan="4" style="padding:8px;">Cart Total</td>';

        $book_detail .= ' <td style="padding:8px;"> Rs. ' . round($result['Order']['total_product_price']) . '</td>';
        $book_detail .= ' </tr>';
        if (!empty($discount)) {
            $book_detail .= ' <tr>';
            $book_detail .= ' <td  colspan="4" style="padding:8px;">Discount Amount</td>';
            $book_detail .= ' <td  style="padding:8px;"> Rs. ' . $discount . '</td>';
            $book_detail .= ' </tr>';
        }
        if (!empty($delivery)) {
            $book_detail .= ' <tr>';
            $book_detail .= ' <td  colspan="4" style="padding:8px;">Delivery Amount</td>';
            $book_detail .= ' <td style="padding:8px;"> Rs. ' . $delivery . '</td>';
            $book_detail .= ' </tr>';
        }

        $book_detail .= ' <tr>';
        $book_detail .= ' <td  colspan="4" style="padding:8px;">Total Amount</td>';

        $book_detail .= ' <td style="padding:8px;"> Rs. ' . round($result['Order']['total_amount']) . '</td>';
        $book_detail .= ' </tr>';
        $book_detail .= ' </tr>';
        $book_detail .= ' </tbody>';
        $book_detail .= ' </table>';
        $book_detail .= '<br></br>';
        $billingaddress = $this->Billingaddress->find('first', array('conditions' => array('billing_address_id' => $result['Order']['billing_address_id'])));
        if (!empty($billingaddress)) {
            $book_detail .= '
                  <h5 style="margin:0px;font-size:18px;">Billing Address</h5>
                  <br>
                  <p style="padding:8px;"><b>Location : </b>' . $billingaddress['Billingaddress']['location'] . '</p>';
            if (!empty($billingaddress['Billingaddress']['flat_no'])) {
                $book_detail .= '<p style="padding:8px;margin:0px;"><b>Flat No : </b>' . $billingaddress['Billingaddress']['flat_no'] . '</p>';
            }
            if (!empty($billingaddress['Billingaddress']['building'])) {
                $book_detail .= '<p style="padding:8px;margin:0px;"><b>Building :</b>' . $billingaddress['Billingaddress']['building'] . '</p>';
            }
            if (!empty($billingaddress['Billingaddress']['landmark'])) {
                $book_detail .= ' <p style="padding:8px;margin:0px;"><b>Landmark :</b>' . $billingaddress['Billingaddress']['landmark'] . '</p>';
            }
            if (!empty($billingaddress['Billingaddress']['instruction'])) {
                $book_detail .= '<p style="padding:8px;margin:0px;"><b>Instruction : </b>' . $billingaddress['Billingaddress']['instruction'] . '</p>';
            }
            if (!empty($billingaddress['Billingaddress']['pincode'])) {
                $book_detail .= '<p style="padding:8px;margin:0px;"><b>Pincode : </b>' . $billingaddress['Billingaddress']['pincode'] . '</p>';
            }
            if (!empty($billingaddress['Billingaddress']['contact_person'])) {
                $book_detail .= '  <p style="padding:8px;margin:0px;"><b>Contact Person : </b>' . $billingaddress['Billingaddress']['contact_person'] . '</p>';
            }
            if (!empty($billingaddress['Billingaddress']['save'])) {
                $book_detail .= '<p style="padding:8px;margin:0px;"><b>Save :</b>' . $billingaddress['Billingaddress']['save'] . '</p>';
            }
            $book_detail .= '<br>';
            $shippingaddress = $this->Shippingaddress->find('first', array('conditions' => array('shippingaddress_id' => $result['Order']['shipping_address_id'])));
            if (!empty($shippingaddress)) {
                $book_detail .= '
                  <h5 style="margin:0px;font-size:18px;">Shipping Address</h5>
                  <br>
                  <p style="padding:8px;margin:0px;"><b>Location : </b>' . $shippingaddress['Shippingaddress']['location'] . '</p>';
                if (!empty($shippingaddress['Shippingaddress']['flat_no'])) {
                    $book_detail .= '<p style="padding:8px;margin:0px;"><b>Flat No : </b>' . $shippingaddress['Shippingaddress']['flat_no'] . '</p>';
                }
                if (!empty($shippingaddress['Shippingaddress']['building'])) {
                    $book_detail .= '<p style="padding:8px;margin:0px;"><b>Building :</b>' . $shippingaddress['Shippingaddress']['building'] . '</p>';
                }
                if (!empty($shippingaddress['Shippingaddress']['landmark'])) {
                    $book_detail .= ' <p style="padding:8px;margin:0px;"><b>Landmark :</b>' . $shippingaddress['Shippingaddress']['landmark'] . '</p>';
                }
                if (!empty($shippingaddress['Shippingaddress']['instruction'])) {
                    $book_detail .= '<p style="padding:8px;margin:0px;"><b>Instruction : </b>' . $shippingaddress['Shippingaddress']['instruction'] . '</p>';
                }
                if (!empty($shippingaddress['Shippingaddress']['pincode'])) {
                    $book_detail .= '<p style="padding:8px;margin:0px;"><b>Pincode : </b>' . $shippingaddress['Shippingaddress']['pincode'] . '</p>';
                }
                if (!empty($shippingaddress['Shippingaddress']['contact_person'])) {
                    $book_detail .= '  <p style="padding:8px;margin:0px;"><b>Contact Person : </b>' . $shippingaddress['Shippingaddress']['contact_person'] . '</p>';
                }
                if (!empty($shippingaddress['Shippingaddress']['save'])) {
                    $book_detail .= '<p style="padding:8px;margin:0px;"><b>Save :</b>' . $shippingaddress['Shippingaddress']['save'] . '</p>';
                }
            }
            if (!empty($delivery_date)) {
                $book_detail .= '<p> <mark>Delivery Date :<b>' . $delivery_date . '</b></mark></p>';
            }

            return $book_detail;
        }
    }

}
