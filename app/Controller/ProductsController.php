<?php

App::uses('AppController', 'Controller');

/**
 * Products Controller
 *
 * @property Product $Product
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ProductsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session', 'Image');
    public $uses = array('User', 'Product', 'Productimage', 'Emailcontent','Sitesetting','Attributevalue', 'Attribute', 'Productattribute','Collection');
    /**
     * AdminIndex
     *
     * @return void
     */
    public function admin_index() {
        $this->layout = 'admin';
        $this->Product->recursive = 0;
        $this->checkadmin();
        $conditions['Product.status !='] = "Trash";
        if (isset($_REQUEST['search'])) {
            $s = $_REQUEST['s'];
            $conditions['Product.product_name'] = trim($s);
            if (!empty($_REQUEST['c'])) {
                $conditions['Product.collection_id'] = $_REQUEST['c'];
            }
        }
        $this->paginate = array('fields' => array('Product.*'), 'conditions' => $conditions, 'order' => 'product_id DESC', 'limit' => '30');
        $this->set('products', $this->Paginator->paginate('Product'));
    }

    public function admin_view($id = NULL) {
        $this->layout = 'admin';
        $this->Product->recursive = 0;
        $this->checkadmin();
        $reviews = $this->Product->find('first', array('conditions' => array('product_id' => $id)));
        $this->set('product', $reviews);
    }

    public function admin_add() {
        $this->layout = 'admin';
        $vendor = $this->checkadmin();
        try {
            if ($this->request->is('post')) {
              

                $this->request->data['Product']['product_name'] = trim(preg_replace('/\s+/', ' ', $this->request->data['Product']['product_name']));
                $this->request->data['Product']['description'] = $this->request->data['Product']['description'];
                $this->request->data['Product']['collection_id'] = $this->request->data['Product']['collection_id'];
                $this->request->data['Product']['material_id'] = implode(',',$this->request->data['Product']['material_id']);
                $this->request->data['Product']['project_id'] = $this->request->data['Product']['project_id'];
                if (!empty($this->request->data['Product']['subcollection_id'])) {
                    $this->request->data['Product']['subcollection_id'] = implode(',',$this->request->data['Product']['subcollection_id']);
                }
                $this->request->data['Product']['created_date'] = date('Y-m-d H:i:s');
                $this->request->data['Product']['modifed_date'] = date('Y-m-d H:i:s');                 
                $this->Product->save($this->request->data['Product']);             
                $product_id = $this->Product->getLastInsertID();
                $p_shop_photos = $this->request->data['Product']['productimages'];  


                if (!empty($this->request->data['Product']['image'][0]['name'])) {
                	$i=0;
                foreach ($this->request->data['Product']['image'] as $scan) {                                             
                    if (in_array($scan['name'],$p_shop_photos)) {                    	   
                        $scn = uniqid() . '.' . $scan['name'];
                        move_uploaded_file($scan['tmp_name'], 'files/products/' . $scn);
                        $this->request->data['Productimage']['image'] = $scn;
                        $this->request->data['Productimage']['product_id'] = $product_id;
                        $this->Productimage->saveAll($this->request->data['Productimage']);
                    }
                    $i++;
                }
            }
            
                // foreach ($p_shop_photos as $photo) {
                //         $str1 = ltrim($photo, 'data:image/jpeg;base64,');
                //         if(strpos($str1, "base64") !== false){
                //           $str2= ltrim($str1, 'ng;base64,');
                //           $string=$str2;
                //         }else{
                //           $string=("/".$str1);
                //         }
                //         $base=$this->base64_toimage($string, 'files/products/');
                //         $this->request->data['Productimage']['image'] = $base;
                //         $this->request->data['Productimage']['product_id'] = $product_id;
                //         $this->Productimage->saveAll($this->request->data['Productimage']);
                // }
                
                if (!empty($this->request->data['Productattribute']['attributename'])) {
                    $this->request->data['Attribute']['attribute_name'] = $this->request->data['Productattribute']['attributename'];
                    $this->request->data['Attribute']['type'] = "Text";
                    $this->request->data['Attribute']['category_id'] = $this->request->data['Product']['category_id'];
                    $this->request->data['Attribute']['created_date'] = date('Y-m-d H:i:s');
                    $this->request->data['Attribute']['modified_date'] = date('Y-m-d H:i:s');
                    $values = explode(',', $this->request->data['Productattribute']['value']);
                    if ($this->Attribute->save($this->request->data['Attribute'])) {
                        $id = $this->Attribute->getLastInsertID();
                        foreach ($values as $value) {
                            $this->request->data['Attributevalue']['value'] = trim($value);
                            $this->request->data['Attributevalue']['attribute_id'] = $id;
                            $this->Attributevalue->saveAll($this->request->data['Attributevalue']);
                        }
                        $attributevalue=$this->Attributevalue->find('first', array('conditions' => array('attribute_id'=>$id)));
                        $this->request->data['Productattribute']['product_id'] = $product_id;
                        $this->request->data['Productattribute']['attributeval_id'] = !empty($attributevalue['Attributevalue']['attributeval_id']) ? $attributevalue['Attributevalue']['attributeval_id'] : "";
                        $this->request->data['Productattribute']['attribute_id'] = $id;
                        $this->request->data['Productattribute']['value'] = $this->request->data['Productattribute']['value'];
                        $this->request->data['Productattribute']['is_visible'] = 1;
                        $this->Productattribute->saveAll($this->request->data['Productattribute']);
                    }
                } else {
                    if (isset($this->request->data['Productattribute']) && !empty($this->request->data['Productattribute'])) {
                        $Productattribute = $this->request->data['Productattribute'];
                        $attributes = $Productattribute['attribute_id'];
                        foreach ($attributes as $key => $attribute) {
                            $values = $Productattribute['value'][$attribute];
                            $attributevalue=$this->Attributevalue->find('first', array('conditions' => array('attribute_id'=>$attribute)));
                         

                            if (is_array($values) && !empty($values)) {
                                foreach ($values as $valu) {
                                    $this->request->data['Productattribute']['product_id'] = $product_id;
                                    $this->request->data['Productattribute']['attribute_id'] = $attribute;
                                    $this->request->data['Productattribute']['attributeval_id'] = $attributevalue['Attributevalue']['attributeval_id'];
                                    $this->request->data['Productattribute']['value'] = $valu;
                                    $this->request->data['Productattribute']['is_visible'] = 1;
                                    $this->Productattribute->saveAll($this->request->data['Productattribute']);
                                }
                            } else {
                                $this->request->data['Productattribute']['product_id'] = $product_id;
                                $this->request->data['Productattribute']['attribute_id'] = $attribute;
                                $this->request->data['Productattribute']['attributeval_id'] = !empty($attributevalue['Attributevalue']['attributeval_id']) ? $attributevalue['Attributevalue']['attributeval_id'] : "";
                                $this->request->data['Productattribute']['value'] = $values;
                                $this->request->data['Productattribute']['is_visible'] = 1;
                                $this->Productattribute->saveAll($this->request->data['Productattribute']);
                            }
                        }
                    }
                }
            
                $this->Session->setFlash('Product Added successfully!', '', array(''), 'success');
                $this->redirect(array('action' => 'index'));
            }
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function admin_edit($id = null) {
        $this->layout = 'admin';
        $this->checkadmin();
        if (!$this->Product->exists($id)) {
            throw new NotFoundException(__('Invalid Product'));
        }
        $products = $this->Product->find('first', array('conditions' => array('product_id' => $id)));
        $proimages = $this->Productimage->find('first', array('conditions' => array('product_id' => $id)));
        if ($this->request->is(array('post', 'put'))) {
        	
            $check = $this->Product->find('first', array('conditions' => array('product_name' => $this->request->data['Product']['product_name'], 'product_id !=' => $id, 'status !=' => 'Trash')));           

                $this->request->data['Product']['product_id'] = $id;
                $this->request->data['Product']['latest_collection']=!empty($this->request->data['Product']['latest_collection']) ? 1 : 0;
                $this->request->data['Product']['material_id']=implode(',',$this->request->data['Product']['material_id']);
                $this->request->data['Product']['subcollection_id']= !empty($this->request->data['Product']['subcollection_id']) ? implode(',',$this->request->data['Product']['subcollection_id']) : "";
                if ($this->Product->save($this->request->data['Product'])) {
                    if (!empty($this->request->data['Productattribute'])) {
                        if (isset($this->request->data['Productattribute']) && !empty($this->request->data['Productattribute'])) {
                            $this->Productattribute->deleteAll(array('product_id' => $id), false, false);
                            $Productattribute = $this->request->data['Productattribute'];

                            if (!empty($Productattribute['value'])) {
                                $attributes = $Productattribute['attribute_id'];
                                foreach ($attributes as $key => $attribute) {

                                    $values = $Productattribute['value'][$attribute];

                                    if (is_array($values) && !empty($values)) {
                                        foreach ($values as $valu) {
                                        $attributevalue=$this->Attributevalue->find('first', array('conditions' => array('attribute_id'=>$attribute)));

                                            $this->request->data['Productattribute']['product_id'] = $id;
                                            $this->request->data['Productattribute']['attributeval_id']=!empty($attributevalue['Attributevalue']['attributeval_id']) ? $attributevalue['Attributevalue']['attributeval_id'] : "";
                                            $this->request->data['Productattribute']['attribute_id'] = $attribute;
                                            $this->request->data['Productattribute']['value'] = $valu;
                                            $this->request->data['Productattribute']['is_visible'] = 1;
                                            $this->Productattribute->saveAll($this->request->data['Productattribute']);
                                        }
                                    } else {                                      
                                        $attributevalue=$this->Attributevalue->find('first', array('conditions' => array('attribute_id'=>$attribute)));
                                        $this->request->data['Productattribute']['product_id'] = $id;
                                        $this->request->data['Productattribute']['attributeval_id'] = !empty($attributevalue['Attributevalue']['attributeval_id']) ? $attributevalue['Attributevalue']['attributeval_id'] :"";
                                        $this->request->data['Productattribute']['attribute_id'] = $attribute;
                                        $this->request->data['Productattribute']['value'] = $values;
                                        $this->request->data['Productattribute']['is_visible'] = 1;
                                        $this->Productattribute->saveAll($this->request->data['Productattribute']);
                                    }
                                }
                            }
                        }
                    }
                }
                
              
                if (!empty($this->request->data['Product']['old_photos'])) {
                    
                    $this->Productimage->deleteAll(array("Productimage.product_id" => $id, "NOT" => array("Productimage.product_imageid" => $this->request->data['Product']['old_photos'])), false);
                } else {
                    
                    $this->Productimage->deleteAll(array("Productimage.product_id" => $id), false);
                }

                if(!empty($this->request->data['Product']['productimages'])){
                	  $p_shop_photos=$this->request->data['Product']['productimages'];

                     if (!empty($this->request->data['Product']['image'][0]['name'])) {
                	$i=0;
                foreach ($this->request->data['Product']['image'] as $scan) {                                     
                    if (in_array($scan['name'],$p_shop_photos)) {      
                      	   
                        $scn = uniqid() . '.' . $scan['name'];
                        move_uploaded_file($scan['tmp_name'], 'files/products/' . $scn);
                        $this->request->data['Productimage']['image'] = $scn;
                        $this->request->data['Productimage']['product_id'] = $id;
                        $this->Productimage->saveAll($this->request->data['Productimage']);
                    }
                    $i++;
                }
          
            }
        }                      

                $this->Session->setFlash('Product Details Updated successfully!', '', array(''), 'success');
                return $this->redirect(array('action' => 'index'));
            
        }
        $this->request->data['Product'] = $products['Product'];
    }



    public function admin_delete($id = null) {
        $this->autorender = false;
        if (!$this->Product->exists($id)) {
            throw new NotFoundException(__('Product Not Found'));
        }
        $this->request->data['Product']['product_id'] = $id;
        $this->request->data['Product']['status'] = 'Trash';
        $this->Product->save($this->request->data['Product']);
        $this->Session->setFlash('Product deleted successfully!', '', array(''), 'success');
        $this->redirect($this->referer());
    }


    public function get_subcollections() {
        $this->layout = '';
        $this->render(false);
        $subcollections = $this->Collection->find('all', array('conditions' => array('status' => 'Active', 'parent_collection' => $_REQUEST['category_id']),'order'=>'collection_name ASC'));
        if (!empty($subcollections)) {
            foreach ($subcollections as $subcollection) {
                echo '<option value="' . $subcollection['Collection']['collection_id'] . '">' . $subcollection['Collection']['collection_name'] . '</option>';
            }
        } else {
            echo '';
        }
    }

    public function admin_getattributes() {
        $this->autoRender = false;
        if (isset($_REQUEST['attribute_id']) && !empty($_REQUEST['attribute_id'])) {
            $attributevalues = $this->Attributevalue->find('all', array('conditions' => array('attribute_id' => $_REQUEST['attribute_id'])));
            $attribute = $this->Attribute->find('first', array('conditions' => array('attribute_id' => $_REQUEST['attribute_id'])));
            if(!empty($attribute)) {
                $tr = '<tr class="additional" id="'.$attribute['Attribute']['attribute_id'] . '">';
            $tr .= '<td style="color: #000;">' . $attribute['Attribute']['attribute_name'] . '</td>';
            if ($attribute['Attribute']['type'] == 'Select') {
                $attributevalues = $this->Attributevalue->find('all', array('conditions' => array('attribute_id' => $_REQUEST['attribute_id'])));
                $options = '';
                foreach ($attributevalues as $attributevalue) {
                    $options .= '<option data-number="'.$attribute['Attribute']['attribute_id'].'" value="' . $attributevalue['Attributevalue']['value'] . '">' . $attributevalue['Attributevalue']['value'] . '</option>';
                }
                $tr .= '<td><select class="form-control chosen validate[required]" name="data[Productattribute][value][' . $attribute['Attribute']['attribute_id'] . '][]">' . $options;
                $tr .= '</select>';
                $tr .= '</td>';
            } else {
                $tr .= '<td><textarea class="form-control validate[required]" data-number="'.$attribute['Attribute']['attribute_id'].'" name="data[Productattribute][value][' . $attribute['Attribute']['attribute_id'] . ']"></textarea></td>';
            }
            $tr .= '<td class="hide text-center"><input type="hidden" name="data[Productattribute][attribute_id][]" value="' . $attribute['Attribute']['attribute_id'] . '"/>';           
            $tr .= '<i class="fa fa-times remove_attribute" aria-hidden="true"></i>';
            $tr .= '</td>';
            $tr .= '</tr>';
            echo $tr;
            exit;
            }
            
        }
    }

}
