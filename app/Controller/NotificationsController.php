<?php

App::uses('AppController', 'Controller');

/**
 * Categorys Controller
 *
 * @property Category $Category
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class NotificationsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('Adminuser', 'Sitesetting', 'Notification');
    public $layout = 'admin';

    /**
     * AdminIndex
     *
     * @return void
     */
    public function admin_add() {
        $this->layout = 'vendor';
        $this->checkvendor();
        try {
            if ($this->request->is('post')) {

                if ($this->request->data['Notification']['smscategory'] == "all") {
                    $users = $this->User->find('all', array('conditions' => array('status !=' => 'Trash', 'vendor_id' => $this->Session->read('Vendor.vendor_id'), 'phone_number !=' => '')));
                    foreach ($users as $user) {
                        $fcmid = $user['User']['fcmid'];
                        $message = array("notifydata" => array('to' => 'User', 'to_id' => $user['User']['user_id'], 'message' => $this->request->data['Notification']['msg'], 'notify_from' => 'Vendor sent notification', 'id' => $this->Session->read('Vendor.vendor_id')));
                        $this->send_vendor_push_notification($fcmid, $message);
                    }
                } else {
                    $users = $this->request->data['Notification']['to_id'];
                    foreach ($users as $user) {
                        $customer = $this->User->find('first', array('conditions' => array('user_id' => $user)));
                        $fcmid = $customer['User']['fcmid'];
                        $message = array("notifydata" => array('to' => 'User', 'to_id' => $customer['User']['user_id'], 'message' => $this->request->data['Notification']['msg'], 'notify_from' => 'Vendor sent notification', 'id' => $this->Session->read('Vendor.vendor_id')));
                        $this->send_vendor_push_notification($fcmid, $message);
                    }
                }

                $this->Session->setFlash('Notification Sent Successfully!', '', array(''), 'success');
                $this->redirect($this->referer());
            }
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function admin_send() {
        $this->layout = 'admin';
        $this->checkadmin();
        try {
            if ($this->request->is('post')) {

                if ($this->request->data['Notification']['smscategory'] == "all") {
                    $users = $this->User->find('all', array('conditions' => array('status !=' => 'Trash', 'phone_number !=' => '')));
                    foreach ($users as $user) {
                        $fcmid = $user['User']['fcmid'];
                        $message = array("notifydata" => array('to' => 'User', 'to_id' => $user['User']['user_id'], 'message' => $this->request->data['Notification']['msg'], 'notify_from' => 'Admin sent notification', 'id' => "Admin"));
                        $this->send_vendor_push_notification($fcmid, $message);
                    }
                } else {
                    $users = $this->request->data['Notification']['to_id'];
                    foreach ($users as $user) {
                        $customer = $this->User->find('first', array('conditions' => array('user_id' => $user)));
                        $fcmid = $customer['User']['fcmid'];
                        $message = array("notifydata" => array('to' => 'User', 'to_id' => $customer['User']['user_id'], 'message' => $this->request->data['Notification']['msg'], 'notify_from' => 'Admin sent notification', 'id' => "Admin"));
                        $this->send_vendor_push_notification($fcmid, $message);
                    }
                }

                $this->Session->setFlash('Notification Sent Successfully!', '', array(''), 'success');
                $this->redirect($this->referer());
            }
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

}
